/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ParsedResult.h
 * Author: wadhaw13
 * ECE297 2020
 * Created on February 29, 2020, 2:53 PM
 */

#ifndef PARSEDRESULT_H
#define PARSEDRESULT_H
#include "LatLon.h"

struct ParsedResult{

    std::string automotive;
    std::string cyclist;
    std::string pedestrian;
    LatLon position;
    
};

#endif /* PARSEDRESULT_H */

