#include "AStar_New.h"
#include "Node.h"
#include "streets_draw.h"
#include "feature_find.h"
#include "StreetsDatabaseAPI.h"
#include "ezgl/application.hpp"
#include "RouteBuilder.h"
#include "m3.h"

extern void load_directions(std::vector <std::string>& directions);

extern ezgl::application* appication;

volatile bool calculating = false;

extern std::vector <Node*> nodes;

RouteBuilder r;

void find_directions(int start, int stop) {

    if (!calculating) calculating = true;

    else return;

    segments_onscreen.clear();

    segments_onscreen = find_path_between_intersections(start, stop, 0);

    if (segments_onscreen.size() != 0) draw_path(segments_onscreen, appication->get_renderer());

    std::reverse(segments_onscreen.begin(), segments_onscreen.end());

    auto list = r.getRouteDirections(segments_onscreen);

    //for (int i = 0 ; i < list.size(); i++) std::cout << list[i] << "\n";        

    load_directions(list);

    appication->flush_drawing();

    auto spane = (GtkWidget *) appication->get_object("spinner");

    gtk_spinner_stop((GtkSpinner*) spane);

    appication->update_message("");

    calculating = false;

}

void find_walking_directions(int start, int stop, double speed, double limit) {

    segments_onscreen.clear();

    segments_onscreen_walking.clear();

    std::vector <std::string > directions;

    std::cout << start << " " << stop << " " << speed << " " << limit << "\n";

    std::pair<std::vector<StreetSegmentIndex>, std::vector < StreetSegmentIndex>> result = find_path_with_walk_to_pick_up(start, stop, 0, speed, limit);

    segments_onscreen = result.second;

    segments_onscreen_walking = result.first;

    if (segments_onscreen.size() != 0) {
        draw_path(segments_onscreen, appication->get_renderer());
    } else {
        appication->update_message("An error occurred that we are unaware of.");
    }

    if (segments_onscreen_walking.size() != 0) {
        draw_walking_path(segments_onscreen_walking, appication->get_renderer());
    } else {
        appication->update_message("An error occurred that we are unaware of.");
    }

    std::vector <std::string> vec1, vec2;

    if (segments_onscreen_walking.size() != 0) {

        directions.push_back("Walking Directions.");

        vec1 = r.getRouteDirections(segments_onscreen_walking);

    } else {

        appication->update_message("No Walking Directions found.");

    }

    if (segments_onscreen.size() != 0) {

        vec2 = r.getRouteDirections(segments_onscreen);

    } else {

        appication->update_message("No Driving Directions found.");

    }

    if (vec1.size() != 0) {

        directions.insert(directions.end(), vec1.begin(), vec1.end());

    }

    directions.push_back("Driving Directions.");

    if (vec2.size() != 0) {

        directions.insert(directions.end(), vec2.begin(), vec2.end());
        
    }

    load_directions(directions);    

    appication->flush_drawing();

    auto spane = (GtkWidget *) appication->get_object("spinner");

    gtk_spinner_stop((GtkSpinner*) spane);

    appication->update_message("");

}

void display_path(ezgl::renderer * g) {

    if (segments_onscreen.size() != 0) draw_path(segments_onscreen, g);

    if (segments_onscreen_walking.size() != 0) draw_walking_path(segments_onscreen_walking, g);

}

void clear_route_info() {

    segments_onscreen.clear();

    segments_onscreen_walking.clear();

}

void draw_path(std::vector<StreetSegmentIndex>& intersections, ezgl::renderer * g) {

    draw_street(appication->get_renderer(), intersections, ezgl::RED, ezgl::line_cap::round, ezgl::line_dash::none);

    g->set_line_dash(ezgl::line_dash::none);

    appication->flush_drawing();

}

void draw_walking_path(std::vector<StreetSegmentIndex>& intersections, ezgl::renderer * g) {

    draw_street(appication->get_renderer(), intersections, ezgl::GREEN, ezgl::line_cap::round, ezgl::line_dash::none);

    g->set_line_dash(ezgl::line_dash::none);

    appication->flush_drawing();

}
