/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RouteBuilder.h
 * Author: wadhaw13
 *
 * Created on March 23, 2020, 9:43 PM
 */

#ifndef ROUTEBUILDER_H
#define ROUTEBUILDER_H

#include "LatLon.h"
#include "DirectionClassifier.h"
#include "ChangeStreetStruct.h"
#include "StreetsDatabaseAPI.h"
#include "Data_Structures.h"

extern std::vector<POI> pois_selected;
extern int internal_1, internal_2, mouse1, mouse2;

class RouteBuilder {
public:

    RouteBuilder();

    virtual ~RouteBuilder() {
    }

    std::vector <std::string> getRouteDirections(std::vector <StreetSegmentIndex>&);

private:

    void filterStreetChanges(std::vector <StreetSegmentIndex>&);

    void buildDirections(std::vector<StreetSegmentIndex>&);

    double getDistance(std::vector<StreetSegmentIndex>&);

    std::string getDirectionFromHeading(Direction vec1, Direction vec2);

    std::vector <ChangeStreetStruct> changingStreets;

    std::vector <std::string> final_list;

    std::vector <StreetIndex> streets_encountered;

    std::vector <StreetSegmentIndex> streetSegmentsHot;

};

#endif /* ROUTEBUILDER_H */

