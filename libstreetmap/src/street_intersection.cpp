#include "m1.h"
#include "StreetsDatabaseAPI.h"
#include "Data_Structures.h"

#define LARGEDIST 99999999
#include "adjacent_headers.h"

std::vector<int> find_intersections_of_two_streets(std::pair<int, int> ss_id) {

    std::vector <int> common_intersections;

    //get all the intersections for each street 
    std::vector <int> vector_a = data.intersections_per_street[ss_id.first];

    std::vector <int> vector_b = data.intersections_per_street[ss_id.second];

    int size_a = vector_a.size(), size_b = vector_b.size();

    //Loop over only the smallest vector of vector_a and vector_b
    int min = (size_a >= size_b) ? size_b : size_a;

    for (int i = 0; i < min; i++) {

        //add all the intersections common to both the streets into vector. 
        //Use std::find to check if the vector does not already contain an intersection

        if ((size_a >= size_b)) {

            if (std::find(vector_a.begin(), vector_a.end(), vector_b[i]) != vector_a.end()) common_intersections.push_back(vector_b[i]); 

        } else {

            if (std::find(vector_b.begin(), vector_b.end(), vector_a[i]) != vector_b.end()) common_intersections.push_back(vector_a[i]);

        }

    }

    return common_intersections;

}

std::vector<std::string> find_street_names_of_intersection(int id) {

    std::vector <std::string> string_vector;

    //get the number of street segments for this particular intersection
    int result = getIntersectionStreetSegmentCount(id);

    for (int i = 0; i < result; i++) {

        int as_id = getIntersectionStreetSegment(id, i);

        string_vector.push_back(getStreetName(getInfoStreetSegment(as_id).streetID));

    }

    return string_vector;

}

int find_closest_intersection(LatLon m_p) {

    //Initialise to an invalid number
    IntersectionIndex idx = -1;
    
    double dist_min = LARGEDIST, dist_tmp = -1;

    LatLon new_position, current_shortest_position;

    for (int i = 0; i < getNumIntersections(); i++) {

        //get new intersection's pos'n
        new_position = getIntersectionPosition(i);

        //Pair the LatLons
        auto pair = std::make_pair(m_p, new_position);

        //if the new calculated distance is smaller then store the current index.
        if ((dist_tmp = (find_distance_between_two_points(pair))) < dist_min) {

            dist_min = dist_tmp;

            idx = i;

            current_shortest_position = new_position;

        }


    }

    return idx;
}

std::vector<int> find_intersections_of_street(int street_id) {

    return data.intersections_per_street.at(street_id);

}

std::vector<int> find_adjacent_intersections(int intersection_id) {

    std::vector<int> myvector = data.adjacent_intersections.at(intersection_id);
       
    sort(myvector.begin(), myvector.end());

    //remove duplicates, return iterator to the end of the unique vector
    auto it = std::unique(myvector.begin(), myvector.end());
    
    //resize to exclude unnecessary entries.
    myvector.resize(std::distance(myvector.begin(), it)); 

    return myvector;

}

std::vector<int> find_adjacent_inter(int intersection_id, std::vector<int> &myvector) {

    myvector = data.adjacent_intersections.at(intersection_id);
       
    sort(myvector.begin(), myvector.end());

    //remove duplicates, return iterator to the end of the unique vector
    auto it = std::unique(myvector.begin(), myvector.end());
    
    //resize to exclude unnecessary entries.
    myvector.resize(std::distance(myvector.begin(), it)); 

    return myvector;

}

std::vector<int> find_adjacent_intersections_nocheck(int intersection_id) {

    std::vector<int> myvector = data.adjacent_intersections_nocheck.at(intersection_id);
       
    sort(myvector.begin(), myvector.end());

    //remove duplicates, return iterator to the end of the unique vector
    auto it = std::unique(myvector.begin(), myvector.end());
    
    //resize to exclude unnecessary entries.
    myvector.resize(std::distance(myvector.begin(), it)); 

    return myvector;

}



