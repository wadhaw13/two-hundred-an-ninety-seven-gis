#ifndef S_PATH
#define S_PATH

#include <queue>
#include "AStar_New.h"
#include "Node.h"
#include "m4.h"
#include <m3.h>
#include <unordered_map>
#include "Node_m4.h"
#include "paths.h"

//void delivery_paths(Node* start, std::vector<Node*> dest, int turn_penalty, std::vector<std::vector<int>> & paths);
//void depot_paths(std::vector<Node*> depots, std::vector<Node*> dest, int turn_penalty, std::vector<std::vector<int>> & depot_paths);


#pragma once

typedef struct paths {
    double travel_time;
    int start;
    int end;
    std::vector<int> path_segs;
}paths;

class comparator{
public:
    bool operator()(Node * const & id1, Node * const & id2) {
        return id1->localGoal > id2->localGoal;
    }
};

class travel_time_comparator {
public:
    bool operator()(paths const & lhs, paths const & rhs) {
        return lhs.travel_time > rhs.travel_time;
    }
};

std::priority_queue<paths, std::vector<paths>, travel_time_comparator> delivery_paths(Node* start, std::vector<Node*> dest, int turn_penalty);
void depot_paths(std::vector<Node*> depots, std::vector<Node*> dest, int turn_penalty, std::unordered_map<int, std::vector<int>> &depot_paths);
void store_deliveries(const std::vector<DeliveryInfo>& deliveries, std::unordered_map<int, std::vector<int>> &delivery_map);
std::priority_queue<paths, std::vector<paths>, travel_time_comparator>  delivery_paths_new(int start_int, std::vector<int> dest_int, int turn_penalty);

#endif

