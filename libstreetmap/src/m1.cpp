#include "m1_headers.h"

bool load_map(std::string map) {

    bool load_successful = loadStreetsDatabaseBIN(map); //Indicates whether the map has loaded successfully

    if (!load_successful) return false;

    std::string path = map.substr(0, 26);

    std::string name = map.substr(26, map.find(".") - 26);

    std::string osm_map = "/cad2/ece297s/public/maps/" + name + ".osm.bin";

    if (!loadOSMDatabaseBIN(osm_map)) {
        
        
        return false; // If invalid map, return false}
    }

    //Load database
    store_intersection_segments();
    store_adjacent_intersections();
    store_segments_per_street();
    store_segment_lengths();
    store_nodepointers();
    store_waypointers();
    store_street_names();
    store_intersections_per_street();
    store_street_lengths();
    store_adjacent_intersections_nocheck();    
    
    if (load_points_of_interest() != 0) std::cout << "Could not load points of interest.\n";
    std::cout << "Done loading map\n";
    
    nodes.resize(getNumIntersections());
    for (int i = 0; i < getNumIntersections(); ++i) {  
        nodes[i] = new Node(getIntersectionPosition(i), i);
    }
    
    return true;
}

void close_map() {
    
    for (int i = 0; i < nodes.size(); ++i) {
        delete nodes[i]; //nodes[i] is a pointer to Node and should deallocate all nodes                                                                     
    }
    nodes.clear();

    closeStreetDatabase();
    data.adjacent_intersections.clear();
    closeOSMDatabase();
    data.segment_lengths.clear();
    data.segment_travel_time.clear();
    data.segments_per_intersection.clear();
    data.segments_per_street.clear();
    data.nodepointer_from_nodeid.clear();
    data.waypointer_from_wayid.clear();
    data.intersections_per_street.clear();
    data.street_names_ordered.clear();
    data.adjacent_intersections.clear();
    data.big_features_area_id.clear();
    data.small_features_pos_id.clear();
    data.points_of_interest.clear();
    data.segments_x_y_id.clear();
    data.segements_big.clear();
    data.pois_selected.clear();
    data.pep_collisions.clear(); 
    data.car_collisions.clear(); 
    data.cyc_collisions.clear(); 
    data.safety_vector.clear();
    data.adjacent_intersections_nocheck.clear();
}


// Creates a nested vector that stores the segments that belong to an intersection
// in a vector. Loops over all intersections and number of segments per intersection.

void store_intersection_segments() {

    // Get total number of intersections
    int total_intersections = getNumIntersections();

    //i holds the current intersection index
    for (int i = 0; i < total_intersections; ++i) {
        int total_segments_per_intersection = getIntersectionStreetSegmentCount(i);

        // Create a vector that holds street_segments
        std::vector<int> street_segments;

        //j holds the current index of the street segment connected to this intersection
        for (int j = 0; j < total_segments_per_intersection; ++j) {
            // Add street segments of particular intersection to vector
            int intersection_segment = getIntersectionStreetSegment(i, j);
            street_segments.push_back(intersection_segment);
        }
        // Add the vector to a vector.
        data.segments_per_intersection.push_back(street_segments);

    }
}

// Create a vector of vectors that store all the street segments that belong to
// a particular street. Loop over all street segments, and use the streetID to index
// which vector the segment should be stored to

void store_segments_per_street() {
    data.segments_per_street.resize(getNumStreets());
    int total_street_segments = getNumStreetSegments();

    for (int i = 0; i < total_street_segments; ++i) {
        int streetid = getInfoStreetSegment(i).streetID;
        data.segments_per_street.at(streetid).push_back(i);
    }
}

// Create a vector that holds all the street segment lengths and travel time.
// Uses distance between two points, as well as constant conversion factors.

void store_segment_lengths() {
    int total_street_segments = getNumStreetSegments();

    data.segment_lengths.resize(total_street_segments);
    data.segment_travel_time.resize(total_street_segments);

    for (int segment = 0; segment < total_street_segments; ++segment) {
        /*Also store the draw_weight, speed is slow if this data is not stored beforehand*/
        // Get the intersection points the street is connected to
        LatLon from_intersection = getIntersectionPosition(getInfoStreetSegment(segment).from);
        LatLon to_intersection = getIntersectionPosition(getInfoStreetSegment(segment).to);

        // Get number of curve points
        int num_curve_points = getInfoStreetSegment(segment).curvePointCount;


        // Straight segment (No curve points)
        if (num_curve_points == 0) {
            std::pair<LatLon, LatLon> p(from_intersection, to_intersection);
            data.segment_lengths.at(segment) = find_distance_between_two_points(p);
        }// has curve points
        else {
            double sum = 0;
            LatLon point1, point2;
            // Loop through smaller segments and sum distance
            for (int i = 0; i < num_curve_points; ++i) {
                // Cases
                if (i == 0) {
                    point1 = from_intersection;
                    point2 = getStreetSegmentCurvePoint(i, segment);
                } else {
                    point1 = getStreetSegmentCurvePoint((i - 1), segment);
                    point2 = getStreetSegmentCurvePoint(i, segment);
                }
                std::pair<LatLon, LatLon> p1(point1, point2);
                sum = sum + find_distance_between_two_points(p1);
            }
            // Sum previous total distance with distance from last curve point
            // to intersection point
            point1 = getStreetSegmentCurvePoint((num_curve_points - 1), segment);
            point2 = to_intersection;
            std::pair<LatLon, LatLon> p1(point1, point2);
            sum = sum + find_distance_between_two_points(p1);

            // Push the sum to a vector
            data.segment_lengths.at(segment) = sum;
        }
        // Get the length of the segment
        double length = find_street_segment_length(segment);

        // Get speedlimit (km/h)
        float speed_limit = getInfoStreetSegment(segment).speedLimit;

        // Push the travel time to vector. Travel time will be in m/s
        // check that speed limit is not 0.
        if (speed_limit == 0){
            //std::cout << "Speed limit of segment " << segment << " is 0. Causes division by 0." << std::endl;
        }
        else{   
            data.segment_travel_time.at(segment) = ((length / KM_TO_M) / speed_limit) * H_TO_S;
        }
    }
}

//hash map now holds corresponding node pointer with an OSMID

void store_nodepointers() {
    int k = getNumberOfNodes();
    for (int i = 0; i < k; i++) {
        const OSMNode* nodei;

        OSMID key;
        nodei = getNodeByIndex(i);
        key = nodei->id();
        data.nodepointer_from_nodeid.insert(std::make_pair(key, nodei));
    }
}

//hash map now holds corresponding way pointer with an OSMID

void store_waypointers() {
    int k = getNumberOfWays();
    for (int i = 0; i < k; i++) {
        const OSMWay* wayi;

        OSMID key;
        wayi = getWayByIndex(i);
        key = wayi->id();
        data.waypointer_from_wayid.insert(std::make_pair(key, wayi));
    }

}

//multimap loads all street names and correspoinding ID (sorted)
//names are made lowercase and spaceless

void store_street_names() {
    int k = getNumStreets();
    for (int i = 0; i < k; i++) {
        std::string name = getStreetName(i);
        boost::algorithm::to_lower(name);
        name.std::string::erase(std::remove(name.begin(), name.end(), ' '), name.end());
        data.street_names_ordered.insert(std::make_pair(name, i));

    }
}

// Creates a vector of vectors that holds all the intersections that can travelled
// to by the current intersections. Accounts for street segments that are only one way.
void store_adjacent_intersections_nocheck() {
    data.adjacent_intersections_nocheck.resize(getNumIntersections());

    // Loop over all intersections, and the segments in that intersection
    for (int i = 0; i < getNumIntersections(); ++i) {
        for (int j = 0; j < getIntersectionStreetSegmentCount(i); ++j) {
            int segment = getIntersectionStreetSegment(i, j);

            if (getInfoStreetSegment(segment).to != i) {
                data.adjacent_intersections_nocheck.at(i).push_back(getInfoStreetSegment(segment).to);
            } else {
                data.adjacent_intersections_nocheck.at(i).push_back(getInfoStreetSegment(segment).from);
            }
            
        }
    }

}

void store_adjacent_intersections() {
/*THIS FUNCTION NEEDS TO BE USING DIRECTLY CONNECTED!!!! WHY REDO THE WORK*/
    data.adjacent_intersections.resize(getNumIntersections());

    // Loop over all intersections, and the segments in that intersection
    for (int i = 0; i < getNumIntersections(); ++i) {
        for (int j = 0; j < getIntersectionStreetSegmentCount(i); ++j) {
            int segment = getIntersectionStreetSegment(i, j);

            // Checks if the intersection is one-way, and only pushes if the travel
            // direction is correct.
            if (getInfoStreetSegment(segment).oneWay) {
                if (getInfoStreetSegment(segment).to != i) {
                    data.adjacent_intersections.at(i).push_back(getInfoStreetSegment(segment).to);
                }
            } else if (getInfoStreetSegment(segment).to != i) {
                data.adjacent_intersections.at(i).push_back(getInfoStreetSegment(segment).to);
            } else {
                data.adjacent_intersections.at(i).push_back(getInfoStreetSegment(segment).from);
            }
        }
    }

}

//Helper function to store all the intersections of a street. Called in loadMap().

void store_intersections_per_street() {

    for (int j = 0; j < getNumStreets(); j++) {

        std::vector <int> intersections;

        //store all the intersections in a hashtable to account for duplicates
        std::unordered_map <int, int> already_have;

        for (int i = 0; i < (data.segments_per_street[j]).size(); i++) {

            auto info = getInfoStreetSegment((data.segments_per_street[j])[i]);

            try {

                //Will throw an exception if the key info.from does not exist. 
                //The exception is handled so that we can account for the new intersection and add it to the list.
                already_have.at(info.from);
            } catch (std::exception e) {

                //add the intersection to the hashtable
                already_have[info.from] = 1;
                intersections.push_back(info.from);
            }


            //same process is repeated for the info.to intersection
            try {

                already_have.at(info.to);
            } catch (std::exception e) {
                already_have[info.to] = 1;
                intersections.push_back(info.to);
            }
        }

        //Add the list of intersections into the vector.
        data.intersections_per_street.push_back(intersections);
    }
}

void store_street_lengths(){
    int streets = getNumStreets();
    data.segment_street_length.resize(streets);
    for (int i = 0; i < getNumStreetSegments(); ++i){
        data.segment_street_length[getInfoStreetSegment(i).streetID] += data.segment_lengths.at(i);
    }
}
