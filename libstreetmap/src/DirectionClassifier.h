/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DirectionClassifier.h
 * Author: wadhaw13
 *
 * Created on March 23, 2020, 5:14 PM
 */

#ifndef DIRECTIONCLASSIFIER_H
#define DIRECTIONCLASSIFIER_H

#include "LatLon.h"

enum class Direction {
    NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST, NORTH, SOUTH, EAST, WEST, DEFAULT //added this because need to sreturn something compile rwarning
};

class DirectionClassifier {

public:

    static Direction classify(LatLon origin, LatLon destination);        

private:

    DirectionClassifier() = delete;

};

std::ostream& operator<<(std::ostream&, Direction&);

std::string operator<<(std::string& lhs, Direction& d);

#endif /* DIRECTIONCLASSIFIER_H */

