/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataService.cpp
 * Author: wadhaw13
 * 
 * Created on March 9, 2020, 10:25 PM
 */

#include "DataService.h"

//using boost::property_tree::read_json;

DataService::DataService() {
}

DataService::~DataService() {
}

std::vector <PlaceFromText> DataService::InvokeRequest(std::string request) {

    this->m_text_list.clear();
    
    auto& m_inst = TransmissionInvoker::getInstance();

    m_token = m_inst.getToken();

    m_inst.setCustomReceiver(json_data);

    auto irq = m_builder.TextSearch(request);

    auto reply = m_inst.invokeTransmission(irq);    

    std::istringstream stream(reply._data.response);

    try {

        read_json(stream, m_tree);

    } catch (const std::exception &exc) {

        std::cout << exc.what() << " problem \n";

    };

    cleanData(stream);  

    return this->m_text_list;

}

void DataService::cleanData(std::istringstream&) {

    BOOST_FOREACH(ptree::value_type &featVal, m_tree.get_child("candidates")) {

        PlaceFromText place;        

        try {

            place.name = featVal.second.get<std::string>("name");

        } catch (...) {
        }
        try {

            place.Lat = featVal.second.get<double>("geometry.location.lat");

        } catch (...) {

        }
        try {

            place.Lon = featVal.second.get<double>("geometry.location.lng");

        } catch (...) {

        }

        try {

            BOOST_FOREACH(ptree::value_type &new_b, featVal.second.get_child("photos")) {

                auto x = new_b.second.get<std::string>("photo_reference");

                place.ref.push_back(x);


            }

        } catch (...) {
        }        

        m_text_list.push_back(place);

    }

}

std::string DataService::InvokePhotoRequest(std::string& ) {


    return {"123"};
    
}
