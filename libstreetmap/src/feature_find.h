#ifndef FEATURE_FIND_H
#define FEATURE_FIND_H

#include "ezgl/application.hpp"
#include "ezgl/callback.hpp"
#include "ezgl/camera.hpp"
#include "ezgl/canvas.hpp"
#include "ezgl/color.hpp"
#include "ezgl/control.hpp"
#include "ezgl/graphics.hpp"
#include "ezgl/point.hpp"
#include "ezgl/rectangle.hpp"
#include "/cad2/ece297s/public/include/milestones/m2.h"
#include <chrono>
#include <thread>
#include <iostream>
#include <algorithm>
#include <boost/algorithm/string.hpp> 
#include "m1.h"
#include "StreetsDatabaseAPI.h"
#include "OSMDatabaseAPI.h"
#include "Data_Structures.h"
void feature_draw(ezgl::renderer *g);
void find(ezgl::application*);
std::pair<double, double> xy_from_point(LatLon point);

double zoom_ratio(ezgl::renderer* g);
std::vector< std::pair<std::string, int>> hit_detect(LatLon pos, double hit_radius);
bool is_worthy(double zoom, double area);
int find_weight(std::string);
bool node_out_of_bounds(std::pair<double, double>, std::pair<double, double>, std::pair<double, double>);
void draw_layer_1(GtkWidget *widget, ezgl::application*);
void draw_layer_2(GtkWidget *widget, ezgl::application*);
void draw_layer_3(GtkWidget *widget, ezgl::application*);
void draw_collisions(ezgl::renderer* g);
extern double LAT_TOP;
extern double LAT_BOT;
extern double init_area;
extern bool vis;
extern ezgl::rectangle draw_this;
void show_popup(std::string);
void determine_factors(ezgl::renderer *g, std::string type);


struct closest_items {
    StreetIndex street_id = -1;

    POIIndex point_of_interest_id = -1;

    FeatureIndex feature_id = -1;

    IntersectionIndex intersection_id = -1;

};


#define X_1 0.05 //area > 50000 if bigger than this . if less than this then area > 25000
#define X_2 0.01 //area > 2500
#define X_3 0.001//area > 1500
#define X_4 0.0001//all
#endif


