/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TToken.h
 * Author: wadhaw13
 * ECE297 2020
 * Created on March 9, 2020, 10:16 PM
 */

#ifndef TTOKEN_H
#define TTOKEN_H

#include <curl/curl.h>

typedef int TransmissionID;

struct TransmissionToken {
    friend class TransmissionInvoker;

public:

    TransmissionID public_id;

private:

    CURL* m_handle;

};

struct TransmissionStruct {
    char *url = nullptr;

    unsigned int size = 0;

    char *response = nullptr;

};

typedef struct Response {
    CURLcode response_code;

    TransmissionStruct _data;

} Response;


size_t curl_default(void *buffer, size_t size, size_t nmemb, void *userp);

size_t json_data(void *buffer, size_t size, size_t nmemb, void *userp);

#endif /* TTOKEN_H */

