/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Courier.h
 * Author: wadhaw13
 *
 * Created on March 28, 2020, 10:34 AM
 */

#ifndef COURIER_H
#define COURIER_H
#pragma once
#include "m4.h"
#include "m4_headers.h"
#include "Data_Structures.h"
#include "streets_draw.h"
#include <algorithm>
#include <ctime>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include "Timer.h"
#include <unordered_map>
#include <queue>
#include "paths.h"
#include "shortest_paths.h"

#define FLOAT_EPSILON 3e-2

extern std::unordered_map<int, std::priority_queue<paths, std::vector<paths>, travel_time_comparator>> pre_computed;

extern std::unordered_map<int, std::vector<int>> delivery_map_global;

struct SolutionGenerated {
    std::vector<CourierSubpath> data;

    double cost;

};

bool comparator_soln(SolutionGenerated& i, SolutionGenerated& j);

extern std::unordered_map<int, std::vector<int>> delivery_map_global;

class Solution {
public:

    Solution();

    std::vector <CourierSubpath> multiStart(const std::vector<DeliveryInfo>& deliveries,
            const std::vector<int>& depots,
            const float turn_penalty,
            const float truck_capacity
            );

    std::vector <CourierSubpath> two_opt(std::vector<DeliveryInfo>& deliveries,
            const std::vector<int>& depots,
            const float turn_penalty,
            const float truck_capacity
            );


    ~Solution();

private:

    //solution verifier unmodified from courier_veriy.cpp
    bool isLegal(const std::vector<DeliveryInfo>& deliveries,
            const std::vector<IntersectionIndex>& depots,
            const std::vector<CourierSubpath>& path,
            const float truck_capacity);

    //returns a random number between [start, stop)
    int getRandom(int start, int stop);


    //Check for paths that do overlap
    std::vector <std::pair <IntersectionIndex, IntersectionIndex>> findOverlappingPaths();

    //Finds the travel time in seconds for a vector of Street_Segments
    double findTravelTime(std::vector <StreetSegmentIndex> vec);

    //Checks for the order fo the delivery
    bool checkLegality(std::vector <CourierSubpath>, std::vector <DeliveryInfo>&);

    void visualise(std::vector <CourierSubpath> soln);

    std::vector<DeliveryInfo> deliveryRandomizer(const std::vector<DeliveryInfo>& info_old);

    //Calculate the total travel time of a solution
    double calculateMetric(std::vector <CourierSubpath> solution);

    //random number class
    boost::random::mt19937 rng;

    float m_cap = -1;

};

//random necessity
int myrandom(int i);

//from the courier_verify.h file
bool pick_up_at_intersection_capacity(const std::vector<DeliveryInfo>& deliveries,
        const std::multimap<IntersectionIndex, size_t>& intersections_to_pick_up,
        const std::vector<unsigned> & pickup_indices,
        const IntersectionIndex curr_intersection,
        std::vector<bool>& deliveries_picked_up,
        float & current_total_weight);

bool drop_off_at_intersection_capacity(const std::vector<DeliveryInfo>& deliveries,
        const std::multimap<IntersectionIndex, size_t>& intersections_to_drop_off,
        const std::vector<bool>& deliveries_picked_up,
        const IntersectionIndex curr_intersection,
        std::vector<bool>& deliveries_dropped_off,
        float & current_total_weight);

bool delivered_all_packages(const std::vector<DeliveryInfo>& deliveries,
        const std::vector<bool>& deliveries_picked_up,
        const std::vector<bool>& deliveries_dropped_off);

bool traverse_segment(const std::vector<StreetSegmentIndex>& path,
        const unsigned path_idx,
        const IntersectionIndex curr_intersection,
        IntersectionIndex& next_intersection);


#endif /* COURIER_H */

