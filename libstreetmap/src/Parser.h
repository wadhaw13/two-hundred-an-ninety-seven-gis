/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Parser.h
 * Author: wadhaw13
 *
 * Created on February 29, 2020, 10:22 AM
 */

#ifndef PARSER_H
#define PARSER_H
#include <iostream>
#include <string.h>
#include <curl/curl.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include "LatLon.h"
#include <string>
#include <sstream>
#include "ParsedResult.h"

using namespace std;
using boost::property_tree::ptree;
using boost::property_tree::read_json;

class Parser {
public:

    void ParsePath(std::string file_path) {
        
        
        m_filestream.open(file_path);

        if (!m_filestream.is_open()) {

            //std::cout << "error.\n";

            return;

        }

        try {
            read_json(m_filestream, m_tree);
            // std::cout << "Json loaded" << &m_tree << "\n";
        } catch (const std::exception &exc) {
            // std::cout << exc.what() << "\n";
        }

        m_filestream.close();

        std::string autos, ped, cycl;

        BOOST_FOREACH(ptree::value_type &featVal, m_tree.get_child("features")) {
            // "features" maps to a JSON array, so each child should have no name
            if (!featVal.first.empty())
                throw "\"features\" child node has a name";

            autos = featVal.second.get<std::string>("properties.AUTOMOBILE");

            ped = featVal.second.get<std::string>("properties.PEDESTRIAN");

            cycl = featVal.second.get<std::string>("properties.CYCLIST");

            // Get GPS coordinates (stored as JSON array of 2 values)
            // Sanity checks: Only 2 values
            ptree coordinates = featVal.second.get_child("geometry.coordinates");
            if (coordinates.size() != 2)
                throw "Coordinates node does not contain 2 items";

            double lon = coordinates.front().second.get_value<double>();
            double lat = coordinates.back().second.get_value<double>();

            LatLon new_pos{(float) lat, (float) lon};

            m_internal.push_back(ParsedResult{autos, cycl, ped, new_pos});

        }

    }

    std::vector <int> ParseStream(std::string file_path) {

         m_filestream.open(file_path);

        if (!m_filestream.is_open()) {

            //std::cout << "error.\n";

            return {123};

        }                
        
         try {
            read_json(m_filestream, m_tree);
             std::cout << "Json loaded" << &m_tree << "\n";
        } catch (const std::exception &exc) {
             std::cout << exc.what() << " problem \n";
        };        
        
        BOOST_FOREACH(ptree::value_type &featVal, m_tree.get_child("results")){
        
            std::cout << featVal.second.get<string>("name") << "\n";
        
        }

    }

    std::vector<struct ParsedResult>& data() {

        return m_internal;

    }

    std::vector<struct ParsedResult>& getData() {

        return m_internal;

    }

    virtual ~Parser() {

        if (m_filestream.is_open()) m_filestream.close();

    }

private:

    std::ifstream m_filestream;

    ptree m_tree;

    std::vector <struct ParsedResult> m_internal;

};

#endif /* PARSER_H */

