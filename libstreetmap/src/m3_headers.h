/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   m3_headers.h
 * Author: wadhaw13
 * ECE297 2020
 * Created on March 11, 2020, 7:08 PM
 */

#ifndef M3_HEADERS_H
#define M3_HEADERS_H

#include "feature_find.h"
#include <vector>
#include <algorithm>
#include <utility>

static std::vector <int> segments_onscreen;

static std::vector <int> segments_onscreen_walking;

class Node;

void find_directions(int start, int stop);

void find_walking_directions(int start, int stop, double, double);

void display_path(ezgl::renderer* g);

void draw_path(std::vector<IntersectionIndex>& intersections, ezgl::renderer* g);

void draw_walking_path(std::vector<IntersectionIndex>& intersections, ezgl::renderer* g);

void clear_route_info();

std::vector<int> find_adjacent_intersections_nocheck(int intersection_id);

#endif /* M3_HEADERS_H */

