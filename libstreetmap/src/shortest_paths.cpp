#include "m4_headers.h"

//pass in any delivery point as node start for loop 
//dest remaining points
//Will have N priority queue for N start nodes paths to each other dest sorted by travel time

std::priority_queue<paths, std::vector<paths>, travel_time_comparator> delivery_paths(Node* start, std::vector<Node*> dest, int turn_penalty) {

    std::priority_queue<paths, std::vector<paths>, travel_time_comparator> path;
    // Check to see if start or end array is empty
    if (dest.empty()) {
        return path;
    }

    std::vector<Node*> dest_copy = dest;
    std::vector<Node*> modified;

    Node* current = start;
    std::priority_queue<Node*, std::vector<Node*>, comparator> notTested;
    current->localGoal = 0;

    notTested.push(current);
    modified.push_back(current);

    while (!notTested.empty() && !dest_copy.empty()) {
        // remove visited nodes
        while (!notTested.empty() && notTested.top()->visited) {
            notTested.pop();
        }

        if (notTested.empty()) {
            break;
        }

        current = notTested.top();
        current->storeNeighbours();
        current->visited = true;
        modified.push_back(current);

        // check the neighbours
        for (auto it2 = current->neighbours.begin(); it2 != current->neighbours.end(); it2++) {
            double maybeLower = DBL_MAX;
            int seg = -1;

            std::vector<int> evaluating_segment;
            getSegment_vec(current->id, (*it2)->id, evaluating_segment);

            if (evaluating_segment.size() == 0) maybeLower = DBL_MAX;
            else {
                for (int i = 0; i < evaluating_segment.size(); i++) {
                    if (oneWayStreet(evaluating_segment.at(i), current->id)) continue;
                    else if (current == start) {
                        double temp = current->localGoal + data.segment_travel_time.at(evaluating_segment.at(i));
                        if (temp < maybeLower) {
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    } else {
                        double temp = current->localGoal + data.segment_travel_time.at(evaluating_segment.at(i));
                        if (apply_turn_penalty(current->cameFrom, evaluating_segment.at(i))) {
                            temp += turn_penalty;
                        }
                        if (temp < maybeLower) {
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }

                }

            }
            if (maybeLower < (*it2)->localGoal) {
                modified.push_back(*it2);
                (*it2)->parent = current;
                (*it2)->localGoal = maybeLower;
                (*it2)->cameFrom = seg;
            }

            if (!(*it2)->visited) {
                auto it = std::find(dest_copy.begin(), dest_copy.end(), *it2);
                if (it != dest_copy.end()) {
                    *it = std::move(dest_copy.back());
                    dest_copy.pop_back();
                }
                notTested.push(*it2);
            }
        }
    }
    //auto map = delivery_map_global;
    // Reconstruct all the paths.
    for (int i = 0; i < dest.size(); ++i) {
        paths p;
        current = dest.at(i);
        p.end = current->id;
        
        if (current->id == start->id){
            continue;
        }
     //   else std::cout << " delivery map global doesn't have the current " << std::endl;
        while (current->parent != nullptr){
            (p.path_segs).push_back(current->cameFrom);
            current = current->parent;
        }
        p.start = current->id;
        p.travel_time = compute_path_travel_time(p.path_segs, 0);
        path.push(p);
    }
    // Reset all modified nodes
    for (int i = 0; i < modified.size(); ++i) {
        modified.at(i)->resetNode();
    }

    return path;
}

void store_deliveries(const std::vector<DeliveryInfo>& deliveries, std::unordered_map<int, std::vector<int>> &delivery_map) {
    for (int i = 0; i < deliveries.size(); ++i) {
        delivery_map.emplace(deliveries.at(i).pickUp, std::vector<int>());
        delivery_map.emplace(deliveries.at(i).dropOff, std::vector<int>());
        delivery_map[deliveries.at(i).pickUp].push_back(i);
        delivery_map[deliveries.at(i).dropOff].push_back(i);
    }
}

//vector of all the depots 
//takes vector that has all the delivery points
//key = delivery point id
//value = vector from closest depot to delivery point (after reversing)

void depot_paths(std::vector<Node*> depots, std::vector<Node*> dest, int turn_penalty, std::unordered_map<int, std::vector<int>> &depot_paths) {
    std::vector<Node*> dest_copy = dest;
    std::vector<Node*> modified;

    if (depots.empty() || dest.empty()) {
        return;
    }
    Node* current = depots.at(0);

    std::priority_queue<Node*, std::vector<Node*>, comparator> notTested;

    // Initialize all start nodes with local goal of 0
    for (int i = 0; i < depots.size(); ++i) {
        Node* start = depots.at(i);
        start->localGoal = 0;

        notTested.push(start);
        modified.push_back(start);
    }
    // Terminate condition will be when all destinations have been reached.
    while (!notTested.empty() && !dest_copy.empty()) {
        while (!notTested.empty() && notTested.top()->visited) {
            notTested.pop();
        }
        if (notTested.empty()) {
            break;
        }

        current = notTested.top();
        current->storeNeighbours_multiple_start();
        current->visited = true;

        // check the neighbours
        for (auto it2 = current->neighbours.begin(); it2 != current->neighbours.end(); it2++) {

            double maybeLower = DBL_MAX;
            int seg = -1;
            std::vector<int> evaluating_segment;
            getSegment_vec(current->id, (*it2)->id, evaluating_segment);

            if (evaluating_segment.size() == 0) maybeLower = DBL_MAX;
            else {
                for (int i = 0; i < evaluating_segment.size(); i++) {
                    if (oneWayStreet(evaluating_segment.at(i), current->id)) continue;
                    else if (std::find(depots.begin(), depots.end(), current) != depots.end()) {
                        double temp = current->localGoal + data.segment_travel_time.at(evaluating_segment.at(i));
                        if (temp < maybeLower) {
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    } else {
                        double temp = current->localGoal + data.segment_travel_time.at(evaluating_segment.at(i));
                        if (apply_turn_penalty(current->cameFrom, evaluating_segment.at(i))) {
                            temp += turn_penalty;
                        }
                        if (temp < maybeLower) {
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }

                }

            }

            if (maybeLower < (*it2)->localGoal) {
                modified.push_back(*it2);
                (*it2)->parent = current;
                (*it2)->localGoal = maybeLower;
                (*it2)->cameFrom = seg;
            }

            if (!(*it2)->visited) {
                // check to see if node is in dest_copy. If it is, remove it.
                auto it = std::find(dest_copy.begin(), dest_copy.end(), *it2);
                if (it != dest_copy.end()) {
                    *it = std::move(dest_copy.back());
                    dest_copy.pop_back();
                }
                notTested.push(*it2);
            }
        }
    }

    // Reconstruct all the paths.
    for (int i = 0; i < dest.size(); ++i) {
        current = dest.at(i);
        std::vector<int> segs;
        while (current->parent != nullptr) {
            segs.push_back(current->cameFrom);
            current = current->parent;
        }
        depot_paths[dest.at(i)->id] = segs;
    }

    // Reset all modified nodes
    for (int i = 0; i < modified.size(); ++i) {
        modified.at(i)->resetNode();
    }
}

std::priority_queue<paths, std::vector<paths>, travel_time_comparator> delivery_paths_new(int start_int, std::vector<int> dest_int, int turn_penalty){
    
    std::unordered_map<int, int> parent;
    std::vector<bool> visited_nodes;
    std::vector<int> current_value;
    std::vector<int> came_from;
    //std::unordered_map<int, std::vector<int>> neighbours;
    
    came_from.resize(getNumIntersections());
    current_value.resize(getNumIntersections());
    visited_nodes.resize(getNumIntersections());
    
    for (int i = 0; i < getNumIntersections(); ++i){
        current_value[i] = DBL_MAX;
    }
    
    std::priority_queue<paths, std::vector<paths>, travel_time_comparator> path;
    // Check to see if start or end array is empty
    if (dest_int.empty()) {
        return path;
    }
    
    std::vector<int> dest_copy = dest_int;
    
    int current = start_int;
    
    auto my_comp = [&current_value](const int& rhs, const int& lhs){
        return (current_value[rhs] > current_value[lhs]);
    };
    std::priority_queue<int, std::vector<int>, decltype(my_comp)> notTested(my_comp);
    current_value[current] = 0;
  
    notTested.push(current);

    while (!notTested.empty() && !dest_copy.empty()) {
        // remove visited nodes
        while (!notTested.empty() && visited_nodes[notTested.top()]) {
            notTested.pop();
        }

        if (notTested.empty()) {
            break;
        }

        current = notTested.top();
        std::vector<int> neighbours;
        find_adjacent_inter(current, neighbours);    
        visited_nodes[current] = true;

        // check the neighbours
        for (auto it2 = neighbours.begin(); it2 != neighbours.end(); it2++) {        
            double maybeLower = DBL_MAX;
            int seg = -1;

            std::vector<int> evaluating_segment;
            getSegment_vec(current, (*it2), evaluating_segment);
            
            if(evaluating_segment.size() == 0) maybeLower = DBL_MAX;
            else{
                for(int i = 0; i < evaluating_segment.size(); i++){
                    if (oneWayStreet(evaluating_segment.at(i), current)) continue;
                    else if(current == start_int){
                        double temp = current_value[current] + data.segment_travel_time.at(evaluating_segment.at(i));
                        if (temp < maybeLower){
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }
                    else{
                        double temp = current_value[current] + data.segment_travel_time.at(evaluating_segment.at(i));
                        if(apply_turn_penalty(came_from[current], evaluating_segment.at(i))){
                                temp+=turn_penalty;
                            }
                        if (temp < maybeLower){
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }

                }

            }
            if (maybeLower < current_value[*it2]){
                parent.emplace(*it2, current);
                parent[*it2] = current;
                current_value[*it2] = maybeLower;
                came_from[*it2] = seg;                        
            }
            
            if (!visited_nodes[*it2]) {
                auto it = std::find(dest_copy.begin(), dest_copy.end(), *it2);
                if (it != dest_copy.end()) {
                    *it = std::move(dest_copy.back());
                    dest_copy.pop_back();
                }
                notTested.push(*it2);
            }
        }
    }
    //auto map = delivery_map_global;
    // Reconstruct all the paths.
    for (int i = 0; i < dest_int.size(); ++i){
        paths p;
        current = dest_int.at(i);
        p.end = current;
        if (current == start_int){
            continue;
        }

     //   else std::cout << " delivery map global doesn't have the current " << std::endl;
        while (parent.find(current) != parent.end()){
            (p.path_segs).push_back(came_from[current]);
            current = parent[current];
        }
        p.start = current;
        p.travel_time = compute_path_travel_time(p.path_segs, 0);
        path.push(p);
    }    
//    
//    for (int i = 0; i < getNumIntersections(); ++i){
//        delete nodes_m4.at(i);
//    }
    
    return path;
}


