/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   poi.h
 * Author: wadhaw13
 * ECE297 2020
 * Created on February 22, 2020, 5:44 PM
 */

#ifndef POI_H
#define POI_H

#include "feature_find.h"
#include "StreetsDatabaseAPI.h"
#include "Data_Structures.h"
#include "ezgl/graphics.hpp"
#include "ezgl/Popup.hpp"
#include "ezgl/control.hpp"
#include <string>
#include "Parser.h"
#include <time.h>

extern const std::string GLOBAL_PATH;

LatLon point_from_xy(double x, double y);

int load_points_of_interest();

extern double LAT_BOT, LAT_TOP;

class POI {
public:

    int idx;
    
    LatLon position;

    std::pair <double, double> location;        

    bool operator==(POI& rhs) {

        return this->idx == rhs.idx;

    }

};


#endif /* POI_H */

