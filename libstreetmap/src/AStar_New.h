#pragma once

#include "m3_headers.h"
#include "Node.h"
#include <chrono>
#include <queue>
#include <map> 

extern ezgl::application* appication;

// Get the segment between two intersections
int getSegment(int id1, int id2);
std::vector<int> getSegment_vec(int id1, int id2);
void getSegment_vec(int id1, int id2, std::vector<int>& segs);

// Create a custom method to sort nodes for priority
bool sortMethod(Node* & lhs, Node* & rhs);

// Determine whether turn penalty needs to be applied
bool apply_turn_penalty(int prev_seg, int next_seg);

// Algorithm code
std::vector<int> algorithm_a_star(Node* start, Node* end, double turn_penalty, std::vector<int>& path);
std::vector<int> algorithm_a_star(std::vector<int>& start, Node* end, double turn_penalty, std::vector<int>& path);

// Get the path based on alloted time
std::vector<int> get_time_limited_path(std::vector<int> & path, double walking_time_limit, double turn_penalty, double walking_speed);

// Algorithm for walking cases
void algorithm_a_star_walking(Node* start, Node* end, double turn_penalty, double walking_speed, double walking_time_limit, std::map<int, std::vector<int>>& walkable_paths, std::vector<int>& qualified_intersections);

// Determine whether the street is one way.
bool oneWayStreet(int seg_id, int inter_id);

// Custom comparator for priority queue ordering.
class myComparator{
public:
    bool operator() (Node* const & id1, Node* const & id2){
        return id1->globalGoal > id2->globalGoal;
    }
};
