#include "m4.h"
#include "m3.h"
#include "m4_headers.h"
#include "Node.h"

int find_depot_intersection(int segment_id, const std::vector<int> & depots) {
    int from = getInfoStreetSegment(segment_id).from;
    int to = getInfoStreetSegment(segment_id).to;
    for (int i = 0; i < depots.size(); i++) {
        if (to == depots[i]) {
            return to;
        } else if (from == depots[i]) {
            return from;
        }
    }
    std::cout << "couldm't get depot intersection" << std::endl;
    return -1;
}

void pre_compute_paths(const float turn_penalty, const std::vector<DeliveryInfo>& deliveries) {
    std::vector<int> delivery_intersections;
    for (int i = 0; i < deliveries.size(); ++i) {
        delivery_intersections.push_back(deliveries.at(i).pickUp);
        delivery_intersections.push_back(deliveries.at(i).dropOff);
    }

    sort(delivery_intersections.begin(), delivery_intersections.end());
    auto it = std::unique(delivery_intersections.begin(), delivery_intersections.end());
    delivery_intersections.resize(std::distance(delivery_intersections.begin(), it));

#pragma omp parallel for
    for (int i = 0; i < delivery_intersections.size(); ++i) {
     //   std::priority_queue<paths, std::vector<paths>, travel_time_comparator> path;
        
        pre_computed.emplace(delivery_intersections.at(i), delivery_paths_new(delivery_intersections.at(i), delivery_intersections, turn_penalty));

    }
}

void algo_solution(
        const std::vector<DeliveryInfo>& deliveries,
        const std::vector<int>& depots,
        const float turn_penalty,
        const float truck_capacity,
        std::vector<CourierSubpath> & solution, 
        int start_index) {

    //GO FROM ANY DEPOT TO A PICKUP THAT IS SHORTEST
    //It would be best to put to travel times in a priority queue so that this operation is fast
    //priority queue fro depot -> pickup travel times
    //lets say in a vector for now, travel times are stored along with the associated start and end intersections
    //for now assume some sort of struct datastructor

    /*struct paths{ //travel_time a vector of paths sorted from lowest travel time to highest
     * double travel_time (sorted on travel time)
     * int start
     * int end
     * bool start_is_depot
     * std::vector<int> deliveries_index; //keep track of any deliveries that have intersections that are the start index of this path,
     * bool end_is_depot
     * std::vector<int> path_segments;
     */
    
    double current_weight = 0;
    std::vector<int> deliveries_on_truck;
    std::vector < std::pair<bool, bool>> delivery_pickup_done_dropoff_done; //keep track of which delivery pick ups or drop offs have been done
    delivery_pickup_done_dropoff_done.resize(deliveries.size());
    std::vector<Node*> depots_Nodes; //all depot nodes
    std::vector<Node*> dest; //all destination nodes (not including other depots, deliveries only)
    std::unordered_map<int, std::vector<int>> depot_to_deliveries_reverse; //key is delivivery intersection, value is path from closests depot to it (need to reverse)

    for (int i = 0; i < depots.size(); i++) {
        depots_Nodes.push_back(nodes[depots[i]]);
    }
    for (int i = 0; i < deliveries.size(); i++) {
        dest.push_back(nodes[deliveries[i].pickUp]); //because only every gona use this for pickups
    }

    depot_paths(depots_Nodes, dest, turn_penalty, depot_to_deliveries_reverse); //got the hasmap now key = deliveries intersection index, value is vector of segments
    //    for (int i = 0; i < depot_to_deliveries_reverse.at(147022).size(); ++i){
    //        std::cout << getStreetName(getInfoStreetSegment(depot_to_deliveries_reverse.at(147022).at(i)).streetID) << std::endl;
    //    }

    /***push from depot to some deliverie that is closest*/
    int start_intersection_id = deliveries[start_index].pickUp; //this can be random but for now take first one
    auto search_path_segments = depot_to_deliveries_reverse.find(start_intersection_id);
    if (search_path_segments == depot_to_deliveries_reverse.end()) std::cout << "ERROR: Not found in hasmap" << std::endl;
    std::vector<int> path_segments = search_path_segments->second;

    int pick_up_depot_id = find_depot_intersection(path_segments.back(), depots); //because it is reverse /************** DOes work because giveing it a segment id, need to also store the depot intersecton***/
    std::reverse(path_segments.begin(), path_segments.end());
    std::vector<unsigned> pick_up_empty; //empty because no pickup done at start intersection

    CourierSubpath depot_pick1 = {pick_up_depot_id, start_intersection_id, path_segments, pick_up_empty};
    solution.push_back(depot_pick1);

    int deliveries_count = deliveries.size();
    while (deliveries_count != 0) {
        Node* start = nodes[solution.back().end_intersection]; //the start intersection of the next path is the end intersection of the previous

       //resize to exclude unnecessary entries.
     //   remaining_dests.resize(std::distance(remaining_dests.begin(), it));

        std::priority_queue<paths, std::vector<paths>, travel_time_comparator> paths_to_other_points_sorted_by_shortest_travel_time_first = pre_computed.at(start->id);
        //delivery_paths(start, remaining_dests, turn_penalty, paths_to_other_points_sorted_by_shortest_travel_time_first);
        //std::cout << "priority queue size " << paths_to_other_points_sorted_by_shortest_travel_time_first.size() << std::endl;

        auto path = paths_to_other_points_sorted_by_shortest_travel_time_first.top();
        std::vector<unsigned> pick_ups;

        //check if at this start node a legal pickup can be made or if a drop of in done, need to keep track of both these things
        std::vector<int> deliveries_index = delivery_map_global.at(solution.back().end_intersection);
        //std::cout << "deliveries index size " << deliveries_index.size() << std::endl;
        for (int j = 0; j < deliveries_index.size(); j++) { //could have multiple things happening at an intersection
            //std::cout << "index " << deliveries_index[j] << " deliveries size " << deliveries.size() << std::endl;
            DeliveryInfo deliveryj = deliveries[deliveries_index[j]];
            bool is_pickup = (deliveryj.pickUp == path.start);
            // std::cout << "is pickup " << is_pickup << " truck capacity " << truck_capacity << " item weight " << deliveryj.itemWeight << " current weight " << current_weight << std::endl;
            if (is_pickup && (current_weight + deliveryj.itemWeight) <= truck_capacity && delivery_pickup_done_dropoff_done[deliveries_index[j]].first == false) {
                //this is a good pickup
                current_weight += deliveryj.itemWeight;
                pick_ups.push_back(deliveries_index[j]); //push back the delivery index
                deliveries_on_truck.push_back(deliveries_index[j]); //keep track that this pick up is on the truck
                // std::cout << "it is worth to pick this bitch up " << std::endl;
                delivery_pickup_done_dropoff_done[deliveries_index[j]].first = true;

            } else if (!is_pickup) { //this is a drop off keep track of if a drop off in made
                //std::cout << "evaluation drop off, deliveries on truck index " << std::endl;
                //for(int d = 0; d < deliveries_on_truck.size(); d++){
                //    std::cout << deliveries_on_truck[d] << std::endl;
                // }
                auto pointer_to_elem = std::find(deliveries_on_truck.begin(), deliveries_on_truck.end(), deliveries_index[j]);
                if (pointer_to_elem != deliveries_on_truck.end()) {
                    //item is on the truck and therefore drop off occurs
                    current_weight -= deliveryj.itemWeight;
                    //a delivery was made so now i can decrement
                    deliveries_count--;
                    //need to erase from deliveries on truck
                    deliveries_on_truck.erase(pointer_to_elem);
                    // std::cout << "it is worth to drop this bitch off " << std::endl;
                    delivery_pickup_done_dropoff_done[deliveries_index[j]].second = true;
                }
            } else {
                //not enough space on the truck for pickup and not a drop off
                //std::cout << "not enough space on truck for pickup and not a drop off" << std::endl;
            }
        }
        if (deliveries_count == 0) {
            //std::cout << " made the last drop off, current weight = " << current_weight << std::endl;
            goto find_end_depot;
        }
        while (paths_to_other_points_sorted_by_shortest_travel_time_first.size() != 0) { //determine nearest legal pickup or drop off
            path = paths_to_other_points_sorted_by_shortest_travel_time_first.top();
            int path_end_intersection = path.end;
            auto deliveries_at_end = delivery_map_global.at(path_end_intersection);
            //things that can be done at end intersection (a legal dropp off or pickup)
            for (int k = 0; k < deliveries_at_end.size(); k++) {
                //                if(current_weight / truck_capacity >= 0.5){ //check  nearest drop off first
                //                    if (path_end_intersection == deliveries[deliveries_at_end[k]].dropOff && delivery_pickup_done_dropoff_done[deliveries_at_end[k]].second != true) {
                //                        auto pointer_to_elem = std::find(deliveries_on_truck.begin(), deliveries_on_truck.end(), deliveries_at_end[k]); //use k because delivers_on_truck uses index
                //                        if (pointer_to_elem != deliveries_on_truck.end()) {
                //                            //this is a good drop off
                //                            goto found_path;
                //                        }
                //                    }
                //                    
                //                }
                if (0) {
                } else {
                    if (path_end_intersection == deliveries[deliveries_at_end[k]].pickUp && delivery_pickup_done_dropoff_done[deliveries_at_end[k]].first != true) { //pick up can be made
                        if ((current_weight + deliveries[deliveries_at_end[k]].itemWeight) <= truck_capacity) {
                            //this is a legal pickup
                            //so this is a good path
                            goto found_path;
                        } else {
                            //std::cout << "mans cant find a path because there aint enough space on the stuck" << std::endl;
                        }
                    }
                    if (path_end_intersection == deliveries[deliveries_at_end[k]].dropOff && delivery_pickup_done_dropoff_done[deliveries_at_end[k]].second != true) {
                        auto pointer_to_elem = std::find(deliveries_on_truck.begin(), deliveries_on_truck.end(), deliveries_at_end[k]); //use k because delivers_on_truck uses index
                        if (pointer_to_elem != deliveries_on_truck.end()) {
                            //this is a good drop off
                            goto found_path;
                        }
                    }
                }

            }
            paths_to_other_points_sorted_by_shortest_travel_time_first.pop(); //end of this path aint a good place to go try the next

        }

        if (paths_to_other_points_sorted_by_shortest_travel_time_first.size() == 0) std::cout << "went through whole queue" << std::endl;
found_path: //breakout of both the for and while loop
        std::reverse(path.path_segs.begin(), path.path_segs.end());
        CourierSubpath pick_upi = {path.start, path.end, path.path_segs, pick_ups};
        solution.push_back(pick_upi);
    }

    //now done all derliveries go to nearest depot
find_end_depot:

    if (current_weight != 0) {
        //std::cout << " going to end depot but truck weight is not 0" << std::endl;
    }
    Node* start = nodes[solution.back().end_intersection]; //the start intersection of the next path is the end intersection of the previous
    std::vector<Node*> remaining_dests; //these will now be the drop offs because at end
    for (int i = 0; i < depots.size(); i++) {
        remaining_dests.push_back(nodes[depots[i]]);
    }
    std::priority_queue<paths, std::vector<paths>, travel_time_comparator> paths_to_other_points_sorted_by_shortest_travel_time_first;
    paths_to_other_points_sorted_by_shortest_travel_time_first = delivery_paths(start, remaining_dests, turn_penalty);

    auto path = paths_to_other_points_sorted_by_shortest_travel_time_first.top();
    //empty because no pickup done before going to depot always will be a drop off
    std::reverse(path.path_segs.begin(), path.path_segs.end()); //reverse th path segs because astar gives reversed segments
    CourierSubpath drop_depot = {path.start, path.end, path.path_segs, pick_up_empty};
    solution.push_back(drop_depot);
    
    
    
}


