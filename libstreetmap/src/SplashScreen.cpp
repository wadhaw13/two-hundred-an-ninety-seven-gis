/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SplashScreen.cpp
 * Author: wadhaw13
 * 
 * Created on March 28, 2020, 12:47 AM
 */

#include "SplashScreen.h"

SplashScreen::SplashScreen(std::string res) {

    int x = 0;

    gtk_init(&x, NULL);

    // Create a builder object that will load the file.
    builder = gtk_builder_new();

    if (builder == nullptr) {

        std::cout << "Builder failed to be allocated.\n";
        return;

    }

    GError *error = nullptr;
    // Load the XML from a file.
    gtk_builder_add_from_file(builder, res.c_str(), &error);

    //Get the object called 'main_window' from the file and show it.
    window = gtk_builder_get_object(builder, "main");

    // Connect the signal handlers defined in the glade file.
    // (Note: if you're looking for the c++ way to do this, there's no
    // support for binding C++ signal handlers. You must use 'extern
    // "C"' functions as handlers.)
    int my_user_data = 0xDEADBEEF;
    gtk_builder_connect_signals(builder, &my_user_data);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    g_timeout_add_seconds(m_out, close_screen, window);


}

void SplashScreen::setTimeout(int x) {

    this->m_out = x;

}

void SplashScreen::showAndAutoHide() {
    gtk_widget_show(GTK_WIDGET(window));
    std::thread thread_obj(gtk_main);
    thread_obj.detach();

}

SplashScreen::~SplashScreen() {


}

gboolean close_screen(gpointer data) {
    gtk_widget_destroy(GTK_WIDGET(data));
    gtk_main_quit();
    return false;
}

