#include "AStar_New.h"
#include "Node.h"

void algorithm_a_star_walking(Node* start, Node* , double turn_penalty, double walking_speed, double walking_time_limit, std::map<int, std::vector<int>>& walkable_paths, std::vector<int>& ) {    
    Node* current = start;
    start->localGoal = 0;
    start->setGlobalGoal_walking(); //heuritis = 0

    std::priority_queue<Node*, std::vector<Node*> , myComparator > notTested;
    notTested.push(start);

    while (!notTested.empty() && current->localGoal <= walking_time_limit) { //changed to break on walking time limit for now 
        
        while (!notTested.empty() && notTested.top()->visited) {
            notTested.pop();
        }

        if (notTested.empty()) {
            break;
        }
        current = notTested.top();
        
        current->storeNeighbours_walking(); //need to store all, no one way qualification for walk path
        
        current->visited = true;

        // check the neighbours
        for (auto it2 = current->neighbours.begin(); it2 != current->neighbours.end(); it2++) {
            
            double maybeLower = DBL_MAX;
            int seg = -1;
            std::vector<int> evaluating_segment;
            getSegment_vec(current->id, (*it2)->id, evaluating_segment);
                    
            if(evaluating_segment.size() == 0) maybeLower = DBL_MAX;
            else{
                for(int i = 0; i < evaluating_segment.size(); i++){
                    if(current == start){
                        double temp = current->localGoal + data.segment_lengths.at(evaluating_segment.at(i)) / walking_speed;     
                        if (temp < maybeLower){
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }
                    else{
                        double temp = current->localGoal + data.segment_lengths.at(evaluating_segment.at(i))/walking_speed;                       
                        if(apply_turn_penalty(current->cameFrom, evaluating_segment.at(i))){
                                temp+=turn_penalty;
                            }
                        if (temp < maybeLower){
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }
                    
                }
                
            }
            if (maybeLower < (*it2)->localGoal){
                (*it2)->parent = current;
                (*it2)->localGoal = maybeLower;
                (*it2)->setGlobalGoal_walking(); //no heuristics because want to keep djikstra
                (*it2)->cameFrom = seg;                        
            }
            
           if (!(*it2)->visited) {
                notTested.push(*it2);
            }
            
        }
    }
    
    for(auto it = nodes.begin(); it != nodes.end(); it++){ //could push nodes visited to a vector 
        if((*it)->visited == true){ //filters all nodes that havent been visied, so includes start
            std::vector<int> segs;
            //std::cout << "parent not null"  << std::endl;
            Node* key = *it;
            Node* iterator = *it;
            while( iterator != start){
                //std::cout << "not equal to start"  << std::endl;
                segs.push_back(iterator->cameFrom);
                iterator = iterator->parent; 
            }
            //std::cout << " going to insert now " << (key)->id << " " << segs.size() <<std::endl;
            walkable_paths.insert(std::make_pair(key->id, segs));
        }
    } //
}
