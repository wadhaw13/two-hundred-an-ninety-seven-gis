/* 
 * File:   Timer.cpp
 * Author: wadhaw13
 * 
 * Created on April 6, 2020, 11:42 AM
 */

#include "Timer.h"

Timer::Timer() {

    previous = std::chrono::high_resolution_clock::now();

}

void Timer::reset() {

    previous = std::chrono::high_resolution_clock::now();

}

double Timer::ellapsed() {

    std::chrono::duration<double> diff = std::chrono::high_resolution_clock::now() - this->previous;

    auto tl = diff.count();

    return tl;

}

void Timer::operator() (){

    std::chrono::duration<double> diff = std::chrono::high_resolution_clock::now() - this->previous;

    auto tl = diff.count();
    
    std::cout << tl << ".\n";

}



