#include "m3.h"
#include "m4.h"
#include "m4_headers.h"

std::unordered_map<int, std::priority_queue<paths, std::vector<paths>, travel_time_comparator>> pre_computed;
std::unordered_map<int, std::vector<int>> delivery_map_global;

std::vector<CourierSubpath> traveling_courier(
        const std::vector<DeliveryInfo>& deliveries,
        const std::vector<int>& depots,
        const float turn_penalty,
        const float truck_capacity) {

    auto start = std::chrono::high_resolution_clock::now();
    pre_compute_paths(turn_penalty, deliveries);
    store_deliveries(deliveries, delivery_map_global); //store this for use later
   
    std::vector<CourierSubpath> solution;
    
    double cost_for_solution = 0;
    int r = 0;
    
    auto end_precompute = std::chrono::high_resolution_clock::now();
    auto duration_in_mili = std::chrono::duration<double, std::milli> (end_precompute - start);
    double time_remaining_after_precompute = 50 -  (duration_in_mili.count() / 1000) ;
    
    algo_solution(deliveries, depots, turn_penalty, truck_capacity, solution, r);
            
    cost_for_solution =  find_travel_time_courier(solution);
    
    duration_in_mili = std::chrono::duration<double, std::milli> (std::chrono::high_resolution_clock::now() - end_precompute);
    double algo_time = (duration_in_mili.count() / 1000);
    
    double remaining_time = time_remaining_after_precompute - algo_time;
    int limit;
    if(algo_time >= .34){
        limit = (int) (remaining_time / algo_time) - 2; //-1 more just for safe measure
    }
    else{
        
        limit = (int) (remaining_time / algo_time) - 1; //-1 just for safe measure
    }
    
    //std::cout << " algo time (s) " << algo_time << " precompute time " << time_remaining_after_precompute << " limit " << limit << std::endl;
    for(int i = 0; i < limit; i++){ //run it 2 times with random dliveries. estimate number because of size of deliveries vector
        
        std::vector<CourierSubpath> temp_solution;
        double temp_cost; 
        r++; //next
        if(r >= deliveries.size()){
            
            pre_computed.clear();
            delivery_map_global.clear();
            return solution;
        }
        algo_solution(deliveries, depots, turn_penalty, truck_capacity, temp_solution, r);
        temp_cost =  find_travel_time_courier(temp_solution);
            
        if(temp_cost < cost_for_solution){
            cost_for_solution = temp_cost;
            
            if((i+1) == limit){
                //no copy needed last one
                
                pre_computed.clear();
                delivery_map_global.clear();
                return temp_solution;
            }
            solution = temp_solution;
        }
    }
    
       
    pre_computed.clear();
    delivery_map_global.clear();
    
    return solution;
}


double find_travel_time_courier(std::vector<CourierSubpath> & solution ){
    double cost = 0;
    
    for(int i = 0; i < solution.size(); i++){
        cost+= findTravelTime(solution[i].subpath);
    }
    
    return cost;
}

double findTravelTime(std::vector <StreetSegmentIndex> vec) {

    double travelTime = 0;

    for (int i = 0; i < vec.size(); i++)

        travelTime += data.segment_travel_time[vec[i]];

    return travelTime;

}

