#include "RouteBuilder.h"

RouteBuilder::RouteBuilder() {

    //nothing

}

std::vector<std::string> RouteBuilder::getRouteDirections(std::vector <StreetSegmentIndex>& indeces) {

    this->streets_encountered.clear();

    this->changingStreets.clear();

    this->streetSegmentsHot.clear();

    this->filterStreetChanges(indeces);

    this->buildDirections(indeces);

    return this->final_list;

}

void RouteBuilder::filterStreetChanges(std::vector <StreetSegmentIndex>& m_indeces) {

    if (m_indeces.size() == 0) {

        std::cout << ("No route found") << "\n";

        return;

    }

    StreetSegmentIndex currentIndex = getInfoStreetSegment(m_indeces[0]).streetID;

    POI p, c, d;

    for (int i = 0; i < m_indeces.size(); i++) {

        InfoStreetSegment response = getInfoStreetSegment(m_indeces[i]);

        if ((response.streetID) != currentIndex) {

            currentIndex = response.streetID;

            this->streets_encountered.push_back(currentIndex);

            this->streetSegmentsHot.push_back(m_indeces[i]);

            if (response.from == getInfoStreetSegment(m_indeces[i - 1]).from) {

                p.position = getIntersectionPosition(getInfoStreetSegment(m_indeces[i - 1]).to);
                p.location = xy_from_point(p.position);


                c.position = getIntersectionPosition(response.from);
                c.location = xy_from_point(c.position);

                d.position = getIntersectionPosition(response.to);
                d.location = xy_from_point(d.position);

                p.idx = getInfoStreetSegment(m_indeces[i - 1]).to;
                c.idx = response.from;
                d.idx = response.to;

                ChangeStreetStruct s{p, c, d};
                changingStreets.push_back(s);

                pois_selected.push_back(c);
                //pois_selected.push_back(p);
                //pois_selected.push_back(d);

            }
            else if (response.from == getInfoStreetSegment(m_indeces[i - 1]).to) {

                p.position = getIntersectionPosition(getInfoStreetSegment(m_indeces[i - 1]).from);
                p.location = xy_from_point(p.position);

                c.position = getIntersectionPosition(response.from);
                c.location = xy_from_point(c.position);

                d.position = getIntersectionPosition(response.to);
                d.location = xy_from_point(d.position);


                p.idx = getInfoStreetSegment(m_indeces[i - 1]).from;
                c.idx = response.from;
                d.idx = response.to;

                ChangeStreetStruct s{p, c, d};
                changingStreets.push_back(s);

                //pois_selected.push_back(p);
                pois_selected.push_back(c);
                //pois_selected.push_back(d);

                //std::cout << p.position.lat() << " " << p.position.lon() << "\n";
                //std::cout << c.position.lat() << " " << c.position.lon() << "\n";

            }
            else if (response.to == getInfoStreetSegment(m_indeces[i - 1]).from) {

                p.position = getIntersectionPosition(getInfoStreetSegment(m_indeces[i - 1]).to);
                p.location = xy_from_point(p.position);

                c.position = getIntersectionPosition((response.to));
                c.location = xy_from_point(c.position);

                d.position = getIntersectionPosition(response.from);
                d.location = xy_from_point(d.position);

                p.idx = getInfoStreetSegment(m_indeces[i - 1]).to;
                c.idx = response.to;
                d.idx = response.from;

                ChangeStreetStruct s{p, c, d};
                changingStreets.push_back(s);

                //pois_selected.push_back(p);
                pois_selected.push_back(c);
                //pois_selected.push_back(d);

                //std::cout << p.position.lat() << " " << p.position.lon() << "\n";
                //std::cout << c.position.lat() << " " << c.position.lon() << "\n";


            }
            else if (response.to == getInfoStreetSegment(m_indeces[i - 1]).to) {

                p.position = getIntersectionPosition(getInfoStreetSegment(m_indeces[i - 1]).from);
                p.location = xy_from_point(p.position);

                c.position = getIntersectionPosition(response.to);
                c.location = xy_from_point(c.position);

                d.position = getIntersectionPosition(response.from);
                d.location = xy_from_point(d.position);

                p.idx = getInfoStreetSegment(m_indeces[i - 1]).from;
                c.idx = response.to;
                d.idx = response.from;

                //pois_selected.push_back(p);
                pois_selected.push_back(c);
                //pois_selected.push_back(d);

                ChangeStreetStruct s{p, c, d};
                changingStreets.push_back(s);

                //std::cout << p.position.lat() << " " << p.position.lon() << "\n";
                //std::cout << c.position.lat() << " " << c.position.lon() << "\n";

            }

        }


    }

}

void RouteBuilder::buildDirections(std::vector <StreetSegmentIndex>& indeces) {

    if (indeces.size() == 0) return;

    this->final_list.clear();

    double total = getDistance(indeces);

    this->final_list.push_back("Total distance: " + std::to_string(total) + " metres.");

    //if no streets were changed, remain on the same street and 
    if (this->changingStreets.size() == 0) {

        if (indeces.size() == 1) {

            auto org = (mouse1 == -1 && mouse2 == -1) ? getIntersectionPosition(internal_1) : getIntersectionPosition(mouse1);

            auto dest = (mouse1 == -1 && mouse2 == -1) ? getIntersectionPosition(internal_2) : getIntersectionPosition(mouse2);

            auto direction = DirectionClassifier::classify(org, dest);

            std::string x, name;

            x << direction;

            if (internal_1 == -1 && internal_2 == -1) {

                name = getIntersectionName(mouse2);

            } else {

                name = getIntersectionName(internal_2);

            }

            final_list.push_back("Head " + x + " towards " + name + " for " + std::to_string(total) + " metres.");

            return;

        }

        auto org = getInfoStreetSegment(indeces[0]);

        auto dest = getInfoStreetSegment(indeces[0 + 1]);

        IntersectionIndex uncommon_intersection = -1; //need to decalre or used without inialize

        if (org.from == dest.from) uncommon_intersection = org.to;

        else if (org.to == dest.from) uncommon_intersection = org.from;

        else if (org.from == dest.to) uncommon_intersection = org.to;

        else if (org.to == dest.to) uncommon_intersection = org.from;

        auto p = DirectionClassifier::classify(getIntersectionPosition(uncommon_intersection), getIntersectionPosition(getInfoStreetSegment(indeces[indeces.size() - 1]).to));

        std::string x, name;

        x << p;

        if (internal_1 == -1 && internal_2 == -1) {

            name = getIntersectionName(mouse2);

        } else {

            name = getIntersectionName(internal_2);

        }

        final_list.push_back("Head " + x + " towards " + name + " for " + std::to_string(total) + " metres.");

        return;

    } else {

        auto org = getInfoStreetSegment(indeces[0]);

        auto dest = getInfoStreetSegment(indeces[0 + 1]);

        IntersectionIndex uncommon_intersection, common;
        uncommon_intersection = -1;
        common = -1;

        if (org.from == dest.from) {
            uncommon_intersection = org.to;
            common = org.from;
        } else if (org.to == dest.from) {
            uncommon_intersection = org.from;
            common = org.to;
        } else if (org.from == dest.to) {
            uncommon_intersection = org.to;
            common = org.from;
        } else if (org.to == dest.to) {
            uncommon_intersection = org.from;
            common = org.to;
        }

        auto p = DirectionClassifier::classify(getIntersectionPosition(uncommon_intersection), getIntersectionPosition(common));

        std::string x;

        x << p;

        auto first = std::find(indeces.begin(), indeces.end(), indeces[0]);

        auto second = std::find(indeces.begin(), indeces.end(), this->streetSegmentsHot[0]);

        std::vector <int> xx(first, second);

        total = getDistance(xx);

        auto name = getStreetName((getInfoStreetSegment(indeces[0]).streetID));

        this->final_list.push_back("Head " + x + " on " + name + " for " + std::to_string(total) + " metres.");

        //process the intersections that are at the brink of a street change and then find somehow whether we are turning left or right.
        for (int i = 0; i < changingStreets.size(); i++) {

            std::string turn;

            auto heading_original = DirectionClassifier::classify(changingStreets[i].from.position, changingStreets[i].middle.position);

            auto heading_new = DirectionClassifier::classify(changingStreets[i].middle.position, changingStreets[i].to.position);

            turn = getDirectionFromHeading(heading_original, heading_new);

            if (i == changingStreets.size() - 1) {

                auto first_2 = std::find(indeces.begin(), indeces.end(), this->streetSegmentsHot[i]);

                std::vector <int> xx_2(first_2, indeces.end());

                total = getDistance(xx_2);

                this->final_list.push_back("Turn " + turn + " onto " + getStreetName(streets_encountered[streets_encountered.size() - 1]) + " and continue for " + std::to_string(total) + " metres.");

                return;


            }

            auto first_1 = std::find(indeces.begin(), indeces.end(), this->streetSegmentsHot[i]);

            auto second_1 = std::find(indeces.begin(), indeces.end(), this->streetSegmentsHot[i + 1]);

            std::vector <int> xx_1(first_1, second_1);

            total = getDistance(xx_1);

            auto next_intersection = changingStreets[i + 1].middle;

            auto name_inter = getIntersectionName(next_intersection.idx);

            auto streetName = getStreetName(this->streets_encountered[i]);

            this->final_list.push_back("Turn " + turn + " onto " + streetName + " and continue for " + std::to_string(total) + " metres.");


        }

    }


}

std::string RouteBuilder::getDirectionFromHeading(Direction vec1, Direction vec2) {

    std::string turn = "";

    if (vec1 == Direction::NORTH_EAST) {


        if (vec2 == Direction::EAST || vec2 == Direction::SOUTH_EAST || vec2 == Direction::SOUTH) turn = "right";

        else if (vec2 == Direction::SOUTH_WEST) turn == "uturn";

        else if (vec2 == Direction::NORTH_EAST) turn = "straight";

        else turn = "left";


    } else if (vec1 == Direction::NORTH_WEST) {

        if (vec2 == Direction::EAST || vec2 == Direction::NORTH_EAST || vec2 == Direction::NORTH) turn = "right";

        else if (vec2 == Direction::SOUTH_EAST) turn == "uturn";

        else if (vec2 == Direction::NORTH_WEST) turn = "straight";

        else turn = "left";


    } else if (vec1 == Direction::SOUTH_EAST) {

        if (vec2 == Direction::EAST || vec2 == Direction::NORTH_EAST || vec2 == Direction::NORTH) turn = "left";

        else if (vec2 == Direction::SOUTH_EAST) turn == "straight";

        else if (vec2 == Direction::NORTH_WEST) turn = "uturn";

        else turn = "right";

    } else if (vec1 == Direction::SOUTH_WEST) {

        if (vec2 == Direction::EAST || vec2 == Direction::SOUTH_EAST || vec2 == Direction::SOUTH) turn = "left";

        else if (vec2 == Direction::SOUTH_WEST) turn == "straight";

        else if (vec2 == Direction::NORTH_EAST) turn = "uturn";

        else turn = "right";


    }

    return turn;


}

double RouteBuilder::getDistance(std::vector<StreetSegmentIndex>& f) {

    double sum = 0;

    for (int i = 0; i < f.size(); i++) {

        sum += data.segment_lengths[f[i]];

    }

    return sum;

}
