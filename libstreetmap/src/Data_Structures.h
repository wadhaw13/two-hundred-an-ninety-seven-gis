#pragma once

#include <vector>
#include "OSMDatabaseAPI.h"
#include <map>
#include "poi.h"
#include "Parser.h"

struct POI;

class Data_Structures {
    
    public:
        
        std::vector<double>segment_lengths;
        std::vector<double>segment_travel_time;
        std::vector<std::vector<int>>segments_per_intersection;
        std::vector<std::vector<int>>segments_per_street;
        std::unordered_map<OSMID, const OSMNode*> nodepointer_from_nodeid;
        std::unordered_map<OSMID, const OSMWay*> waypointer_from_wayid;
        std::vector<std::vector <int>> intersections_per_street;
        std::multimap<std::string,int> street_names_ordered;
        std::vector<std::vector <int>> adjacent_intersections;
        std::vector<std::pair<double, int>> big_features_area_id;
        std::vector<std::pair<double, int>> small_features_pos_id;
        std::vector<double>segment_street_length;
        std::multimap <std::string, POI> points_of_interest;
        std::vector<std::pair<std::string, int>> close_items;
        std::vector<std::vector<double>> segments_x_y_id;
        std::vector<int> segements_big; //ids of the big street segments //always drawn
        std::vector <POI> pois_selected;
        std::vector<LatLon> pep_collisions; 
        std::vector<LatLon> car_collisions; 
        std::vector<LatLon> cyc_collisions; 
        std::vector <struct ParsedResult> safety_vector;       
        std::vector <int> segments;
        std::vector<std::vector<int>> adjacent_intersections_nocheck;
        // std::vector<std::pair<double, int>> segments_y_id;
};

extern Data_Structures data;
