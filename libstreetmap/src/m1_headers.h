#include "m1.h"
#include "StreetsDatabaseAPI.h"
#include <cmath>
#include <cctype>
#include "OSMDatabaseAPI.h"
#include <vector>
#include "Data_Structures.h"
#include <algorithm>
#include <boost/algorithm/string.hpp> 
#include "poi.h"
#include <chrono>
#include <Node.h>
// Define Constants
#define KM_TO_M 1000
#define H_TO_S 3600

// Create a global instance of data structures
Data_Structures data;
// Helper Function definitions
void store_intersection_segments();
void store_adjacent_intersections();
void store_segments_per_street();
void store_segment_lengths();
void store_nodepointers();
void store_waypointers();
void store_intersections_per_street();
void store_street_names();
void find_adjacent_intersections_helper(int i_id);
void find_intersections_of_street_helper(InfoStreetSegment & info);
void store_big_features_by_area_small_by_pos();
void store_street_lengths();
void store_adjacent_intersections_nocheck();