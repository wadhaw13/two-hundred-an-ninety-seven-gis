#include "Node.h"
#include "adjacent_headers.h"

#define HEURISTIC_FACTOR 27

Node::Node(LatLon pos, int _id) {
    position = pos;
    parent = nullptr;
    localGoal = DBL_MAX;
    globalGoal = DBL_MAX;
    id = _id;
}

Node::Node(LatLon pos, Node* _parent, int _id) {
    position = pos;
    parent = _parent;
    localGoal = DBL_MAX;
    globalGoal = DBL_MAX;
    id = _id;

}

Node::~Node() {

}

void Node::setGlobalGoal(LatLon pos) {
    globalGoal = (find_distance_between_two_points(std::make_pair(position, pos)))/HEURISTIC_FACTOR + localGoal;
    //globalGoal = 0 + localGoal;
}
void Node::setGlobalGoal_walking(){
    globalGoal = localGoal;
}
void Node::setParent(Node* & _parent) {
    parent = _parent;
}

void Node::setLocalGoal(double travel_time) {
    localGoal = travel_time + parent->localGoal;
}

void Node::storeNeighbours() {
    // Need to deallocate

    std::vector<int> adj;
    find_adjacent_inter(id, adj);
    for (auto it = adj.begin(); it != adj.end(); it++) {
        neighbours.push_back(nodes.at(*it));
    }
}
void Node::storeNeighbours_multiple_start(){
    std::vector<int> adj;
    find_adjacent_inter(id, adj);
    for (auto it = adj.begin(); it != adj.end(); it++) {
        Node* node = nodes.at(*it);
        if(node->localGoal == 0) {
        }//do nothing
        else neighbours.push_back(node); //if local goal is 0 then its also a start node so no need to travel to it in any case
        //else 
    }
}
void Node::storeNeighbours_walking() { //in this function oneway does not matter
        // Need to deallocate

        std::vector<int> adj = find_adjacent_intersections_nocheck(id);
        for (auto it = adj.begin(); it != adj.end(); it++) {
            neighbours.push_back(nodes.at(*it));
        }
    }

bool Node::operator == (const Node* rhs) {
    return id == rhs->id;
}

void Node::resetNode(){
    parent = nullptr;
    localGoal = DBL_MAX;
    globalGoal = DBL_MAX;
    neighbours.clear();
    visited = false;
}