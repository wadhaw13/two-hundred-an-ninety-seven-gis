#include "feature_find.h"
std::vector<ezgl::point2d> draw_points;
std::pair<double, double> xy_from_point(LatLon point){
    double cos_lat_avg_rad = cos(DEGREE_TO_RADIAN * (LAT_TOP + LAT_BOT) / 2);
    return std::make_pair(DEGREE_TO_RADIAN * point.lon() * cos_lat_avg_rad, DEGREE_TO_RADIAN * point.lat());
}
void feature_draw(ezgl::renderer *g){   
    double zoom_level = zoom_ratio(g);
    g->set_line_width(3);
    ezgl::rectangle draw_area = g->get_visible_world();
    bool is_closed;
    double factor = 1.00;
    std::pair<double, double> bot_xy = std::make_pair(draw_area.m_first.x*factor,draw_area.m_first.y/factor);
    std::pair<double, double> top_xy = std::make_pair(draw_area.m_second.x/factor,draw_area.m_second.y *factor);
    
    auto& big_features = data.big_features_area_id;
    for(int k = 0; k < big_features.size(); k++){
        int& i = big_features[k].second;
         
        double feature_area = big_features[k].first;
        bool draw = is_worthy(zoom_level, feature_area);
        if(!draw) continue; //dont do anything if too to small
        
        draw_points.clear();
        int pc = getFeaturePointCount(i);
       
        if(big_features[k].first == 0) is_closed = false;
        else is_closed = true;
        
          FeatureType type = getFeatureType(i);
           switch(type){
               case Unknown: g->set_color(225,225,225, 100);
               break;
               case Park: g->set_color(172,224,161, 100);
               break; 
               case Beach: g->set_color(238,240,213, 100);
               break; 
               case Lake: g->set_color(170,211,223, 100);
               break; 
               case River: g->set_color(180,208,209, 100);
               break; 
               case Island: g->set_color(182,182,144, 100);
               break; 
               case Building:g->set_color(199,199,180, 100);
               break; 
               case Greenspace: g->set_color(172,210,156, 100);
               break; 
               case Golfcourse: g->set_color(205,235,176, 100);
               break; 
               case Stream: g->set_color(180,208,190, 100);
               break;
               default: g->set_color(ezgl::BLACK);
           }
        for(int j = 1; j < pc; j++){
           std::pair<LatLon, LatLon> ll = std::make_pair(getFeaturePoint(j-1, i), getFeaturePoint(j, i));
           std::pair <double, double> xy_1 = xy_from_point(ll.first); 
           std::pair <double, double> xy_2 = xy_from_point(ll.second); 

         
           
           ezgl::point2d p1(xy_1.first * EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS);
           ezgl::point2d p2(xy_2.first * EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS);
           if(is_closed){
               if(j-1 == 0) { //i.e first point then push both
                draw_points.push_back(p1);
                draw_points.push_back(p2);
               }
               else
                   draw_points.push_back(p2);
           }
           else
           g->draw_line(p1, p2);
        }
        if(is_closed && pc != 1 && draw_points.size() > 1){ 
            g->fill_poly(draw_points);
        }
    }
    //small features sorted by position_weight
    if(zoom_level < X_4){      
        auto& small_features = data.small_features_pos_id;

        int last_ind = -1;
        int first_ind = -1;
        
       for(int k = 0; k < small_features.size(); k++){
         double pos  =  small_features[k].first;
          if(pos*EARTH_RADIUS_METERS  <= (bot_xy.first)){
          last_ind = k;
          break;
        }
     }
        
    for(int k = 0; k < small_features.size(); k++){ //sorted from largest to smallest
         double pos  =  small_features[k].first;
          if(pos*EARTH_RADIUS_METERS  <= (top_xy.first)){
          first_ind = k;
          break;
    }
   }
           
    for(int k = first_ind; k < last_ind; k++){
        int& i = small_features[k].second;
         
//        double pos = small_features[k].first;
        
        draw_points.clear();
        int pc = getFeaturePointCount(i);
       
        if(small_features[k].first == 0) is_closed = false;
        else is_closed = true;
        
          FeatureType type = getFeatureType(i);
           switch(type){
               case Unknown: g->set_color(225,225,225, 100);
               break;
               case Park: g->set_color(172,224,161, 100);
               break; 
               case Beach: g->set_color(238,240,213, 100);
               break; 
               case Lake: g->set_color(170,211,223, 100);
               break; 
               case River: g->set_color(180,208,209, 100);
               break; 
               case Island: g->set_color(182,182,144, 100);
               break; 
               case Building:g->set_color(199,199,180, 100);
               break; 
               case Greenspace: g->set_color(172,210,156, 100);
               break; 
               case Golfcourse: g->set_color(205,235,176, 100);
               break; 
               case Stream: g->set_color(180,208,190, 100);
               break;
               default: g->set_color(ezgl::BLACK);
           }
        for(int j = 1; j < pc; j++){
           std::pair<LatLon, LatLon> ll = std::make_pair(getFeaturePoint(j-1, i), getFeaturePoint(j, i));
           std::pair <double, double> xy_1 = xy_from_point(ll.first); 
           std::pair <double, double> xy_2 = xy_from_point(ll.second); 
               
           ezgl::point2d p1(xy_1.first * EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS);
           ezgl::point2d p2(xy_2.first * EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS);
           if(is_closed){
               if(j-1 == 0) { //i.e first point then push both
                draw_points.push_back(p1);
                draw_points.push_back(p2);
               }
               else
                   draw_points.push_back(p2);
           }
           else
           g->draw_line(p1, p2);
        }
        if(is_closed && pc != 1 && draw_points.size() > 1){ //is there any point of drawing something if its a singular point?? I dont think so , currently a feature needs atleadt 2 points to work
            g->fill_poly(draw_points);
        }
    }
    }
}
//
//based on zoom levels, determine the hit radius (i.e offset form absolute pos)

std::vector<std::pair <std::string, int>> hit_detect(LatLon pos, double hit_radius) {

    //Given a latlon finds if the user has pressed anything of use

    std::string FEATURE = "feature", POI = "poi", STREETS = "str", INTERSECTION = "int";
    
    std::vector <std::pair <std::string, int>> close_items;
    
    int street_seg = getNumStreetSegments();
    int intersections = getNumIntersections();
    int poi = getNumPointsOfInterest();
    int feature = getNumFeatures();

    for (int i = 0; i < street_seg; i++) {
        OSMID way_id = getInfoStreetSegment(i).wayOSMID;
        int street_id = getInfoStreetSegment(i).streetID;
        const OSMWay* way;
        auto search_way = data.waypointer_from_wayid.find(way_id);
        if (search_way == data.waypointer_from_wayid.end()) break; //not found return
        way = search_way->second;

        std::vector<OSMID> nodes = getWayMembers(way); //nodes in way

        int size = nodes.size();
        for (int j = 0; j < size; j++) {

            auto node2 = data.nodepointer_from_nodeid.find(nodes.at(j));
            if (node2 == data.nodepointer_from_nodeid.end()) break;

            LatLon item_pos = getNodeCoords(node2->second);
            if (find_distance_between_two_points(std::make_pair(pos, item_pos)) < hit_radius) {
                close_items.push_back(std::make_pair(STREETS, street_id));
                break;
            }
        }
    }

    for (int i = 0; i < intersections; i++) {
        LatLon item_pos = getIntersectionPosition(i);
        if (find_distance_between_two_points(std::make_pair(pos, item_pos)) < hit_radius) {
            close_items.push_back(std::make_pair(INTERSECTION, i));
            break; //can return  here to improve perforamce
            //but it might be usefull to go over all, up to the implementation
        }
    }

    for (int i = 0; i < poi; i++) {
        LatLon item_pos = getPointOfInterestPosition(i);
        if (find_distance_between_two_points(std::make_pair(pos, item_pos)) < hit_radius) {
            close_items.push_back(std::make_pair(POI, i));
            break; //can return here to improve perforamce
            //but it might be usefull to go over all, up to the implementation
        }
    }

    for (int i = 0; i < feature; i++) {
        int fea_point = getFeaturePointCount(i);
        for (int j = 0; j < fea_point; j++) {
            LatLon item_pos = getFeaturePoint(j, i);
            if (find_distance_between_two_points(std::make_pair(pos, item_pos)) < hit_radius) {
                close_items.push_back(std::make_pair(FEATURE, i));
                break; //can return here to improve perforamce
                //but it might be usefull to go over all, up to the implementation
            }
        }
    }
    return close_items;
}

double zoom_ratio(ezgl::renderer* g) {
    ezgl::rectangle draw_area = g->get_visible_world();

    return draw_area.area() / init_area;
}

bool is_worthy(double zoom, double area) {
    if (zoom < X_4) {
        return true;
    }
    if (zoom < X_3) {
        if (area > 1500) {
            return true;
        } else return false;
    }
    if (zoom < X_2) {
        if (area > 2500) {
            return true;
        } else return false;
    }
    if (zoom < X_1) {
        if (area > 25000) {
            return true;
        } else return false;
    }

    if (zoom > X_1) {
        if (area > 50000) {
            return true;
        } else return false;
    }
    return false;
}

bool node_out_of_bounds(std::pair<double, double> bot_xy, std::pair<double, double> top_xy, std::pair<double, double> xy_1){
    if(xy_1.first*EARTH_RADIUS_METERS  >= (top_xy.first) || xy_1.second*EARTH_RADIUS_METERS >= (top_xy.second)){
        return true;
           }
    if(xy_1.first*EARTH_RADIUS_METERS <= (bot_xy.first) || xy_1.second*EARTH_RADIUS_METERS <= (bot_xy.second)){
        return true;
    }
    return false;
}
