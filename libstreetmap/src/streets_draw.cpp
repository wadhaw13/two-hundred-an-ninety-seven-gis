#include "streets_draw.h"
#include "StreetsDatabaseAPI.h"
#include "Data_Structures.h"
#include "feature_find.h"
#include "poi.h"
#define OFFSET 1

int interval;
bool print_name = false;
int text_x;
double adder = 0;
std::vector<int> colours;

int find_weight(std::string type) { //SHRENIK ADD more drawing info here (willl need to draw these diff)
    if (type == "cycleway" || type == "path" || type == "steps" || type == "bridleway" || type == "footway") {
        return 1;
    }
    if (type == "bus_guideway" || type == "track" || type == "pedestrian" || type == "service" || type == "living_street") {
        return 1;
    }
    if (type == "road" || type == "raceway" || type == "construction") {
        return 1;
    }
    if (type == "unclassified" || type == "tertiary_link" || type == "residential") {
        return 2;
    }
    if (type == "secondary" || type == "secondary_link") {
        return 3;
    }
    if (type == "primary" || type == "primary_link") {
        return 4;
    }
    if (type == "trunk" || type == "trunk_link") {
        return 5;
    }
    if (type == "motorway" || type == "motorway_link") {
        return 6;
    } else {
        return 0; //dont draw
    }
}

// This function is used to determine the drawing factors of each segment, depending
// on the tags that it has

void determine_factors(ezgl::renderer *g, std::string type) {
    colours.resize(4);

    if (type == "motorway" || type == "motorway_link") {
        g->set_color(233, 144, 160, 255);
        print_name = true;
        g->set_line_width(10);
        g->set_font_size(11);
        adder = 0.002;
        colours[0] = 233;
        colours[1] = 144;
        colours[2] = 160;
        colours[3] = 255;
    } else if (type == "trunk" || type == "trunk_link") {
        g->set_color(251, 178, 154, 255);
        g->set_line_width(10);
        print_name = false;
        adder = 0.0002;
        colours[0] = 251;
        colours[1] = 178;
        colours[2] = 154;
        colours[3] = 255;
    } else if (type == "primary" || type == "primary_link") {
        g->set_color(253, 215, 161, 200);
        g->set_line_width(5);
        adder = 0.0002;
        print_name = false;
        colours[0] = 253;
        colours[1] = 215;
        colours[2] = 161;
        colours[3] = 200;
    } else if (type == "secondary" || type == "secondary_link") {
        g->set_color(246, 250, 187, 200);
        g->set_line_width(5);
        adder = 0.0002;
        print_name = true;
        colours[0] = 246;
        colours[1] = 250;
        colours[2] = 187;
        colours[3] = 200;
    } else if (type == "tertiary" || type == "tertiary_link") {
        g->set_color(254, 254, 254, 255);
        g->set_line_width(5);
        adder = 0;
        print_name = true;
        colours[0] = 254;
        colours[1] = 254;
        colours[2] = 254;
        colours[3] = 255;
    } else if (type == "unclassified") {
        g->set_color(254, 254, 254, 255);
        g->set_line_width(3);
        print_name = true;
        adder = 0;
        colours[0] = 254;
        colours[1] = 254;
        colours[2] = 254;
        colours[3] = 255;
    } else if (type == "residential" || type == "service") {
        g->set_color(254, 254, 254, 255);
        g->set_line_width(3);
        print_name = true;
        colours[0] = 254;
        colours[1] = 254;
        colours[2] = 254;
        colours[3] = 255;
        adder = 0;
    } else if (type == "living_street") {
        g->set_color(237, 237, 237, 255);
        g->set_line_width(3);
        print_name = true;
        colours[0] = 237;
        colours[1] = 237;
        colours[2] = 237;
        colours[3] = 255;
        adder = 0;
    } else if (type == "pedestrian") {
        g->set_color(221, 221, 233, 255);
        g->set_line_width(3);
        print_name = true;
        colours[0] = 221;
        colours[1] = 221;
        colours[2] = 233;
        colours[3] = 255;
        adder = 0;
    } else if (type == "raceway") {
        g->set_color(255, 192, 202, 255);
        g->set_line_width(3);
        print_name = true;
        colours[0] = 255;
        colours[1] = 192;
        colours[2] = 202;
        colours[3] = 255;
        adder = 0;
    } else if (type == "road") {
        g->set_color(194, 194, 193, 255);
        g->set_line_width(3);
        print_name = true;
        colours[0] = 194;
        colours[1] = 194;
        colours[2] = 193;
        colours[3] = 255;
        adder = 0;
    } else if (type == "cycleway") {
        g->set_line_dash(ezgl::line_dash::asymmetric_5_3);
        g->set_color(168, 167, 245, 255);
        g->set_line_width(3);
        adder = 0;
        colours[0] = 168;
        colours[1] = 167;
        colours[2] = 245;
        colours[3] = 255;
    } else {
        print_name = true;
        g->set_color(254, 254, 254, 255);
        adder = 0;
        colours[0] = 254;
        colours[1] = 254;
        colours[2] = 254;
        colours[3] = 255;
    }
}

// This function is the optimization and drawing logic for the streets. It first 
// qualifies streets to draw depending on various street factors and then draws them
// accordingly

void streets_draw(ezgl::renderer *g) {
    std::vector<int> street_names_count;
    street_names_count.resize(getNumStreets());
    double zoom_level = zoom_ratio(g);

    ezgl::rectangle draw_area = g->get_visible_world();

    double factor = 1.00;
    double streets_offset = 1.0001;
    std::pair<double, double> bot_xy = std::make_pair(draw_area.m_first.x*factor, draw_area.m_first.y / factor);
    std::pair<double, double> top_xy = std::make_pair(draw_area.m_second.x / factor, draw_area.m_second.y * factor);

    if (zoom_level < X_1) {
        auto& street_seg_x_y = data.segments_x_y_id;
        int last_ind_x = 0;
        int first_ind_x = 0;

        if (bot_xy.first < 0) {
            bot_xy.first = bot_xy.first * streets_offset;
        } else {
            bot_xy.first = bot_xy.first / streets_offset;
        }
        if (top_xy.first > 0) {
            top_xy.first = top_xy.first * streets_offset;
        } else {
            top_xy.first = top_xy.first / streets_offset;
        }


        if (bot_xy.second < 0) {
            bot_xy.second = bot_xy.second * streets_offset;
        } else {
            bot_xy.second = bot_xy.second / streets_offset;
        }
        if (top_xy.second > 0) {
            top_xy.second = top_xy.second * streets_offset;
        } else {
            top_xy.second = top_xy.second / streets_offset;
        }

        for (int k = 0; k < street_seg_x_y.size(); k++) {
            double pos = street_seg_x_y[k][0];
            if (pos * EARTH_RADIUS_METERS <= (bot_xy.first)) {
                last_ind_x = k;
                break;
            }
        }

        for (int k = 0; k < street_seg_x_y.size(); k++) {
            double pos = street_seg_x_y[k][0];
            if (pos * EARTH_RADIUS_METERS <= (top_xy.first)) {
                first_ind_x = k;
                break;
            }
        }
        std::vector<int> more_qualified_ids;
        for (int j = first_ind_x; j < last_ind_x; ++j) {
            adder = 0;
            int segIdx = (int) street_seg_x_y[j][2];

            if (street_seg_x_y[j][1] * EARTH_RADIUS_METERS < top_xy.second && street_seg_x_y[j][1] * EARTH_RADIUS_METERS > bot_xy.second) {
                more_qualified_ids.push_back(segIdx);
            }
        }


        if (last_ind_x == 0) last_ind_x = street_seg_x_y.size();
        for (int j = 0; j < more_qualified_ids.size(); ++j) {

            int segIdx = more_qualified_ids[j];

            OSMID way_id = getInfoStreetSegment(segIdx).wayOSMID;
            auto search_way = data.waypointer_from_wayid.find(way_id);
            const OSMWay* way = search_way->second;
            std::string street_name = getStreetName(getInfoStreetSegment(segIdx).streetID);

            int draw_weight = 0;
            for (int i = 0; i < getTagCount(way); ++i) {
                std::string key, value;
                std::tie(key, value) = getTagPair(way, i);
                if (key == "highway") {
                    draw_weight = find_weight(value);
                    determine_factors(g, value);
                }
            }
            bool draw = false;
            if (zoom_level < X_4) {
                if (draw_weight >= 0) {
                    text_x = 100;
                    interval = 1;
                    draw = true;
                    adder = 0;
                }
            } else if (zoom_level < X_3) {
                if (draw_weight >= 0) {
                    text_x = 200;
                    interval = 3;
                    draw = true;
                }
            } else if (zoom_level < X_2) {
                if (draw_weight > 1) {
                    text_x = 300;
                    interval = 20;
                    draw = true;
                }
            } else if (zoom_level < X_1) {
                if (draw_weight > 1) {
                    text_x = 500;
                    interval = 40;
                    draw = true;
                }
            } else if (zoom_level > X_1) {
                if (draw_weight > 2) {
                    text_x = 1000;
                    interval = 100;
                    draw = true;
                }
            }
            if (draw) {

                if (getInfoStreetSegment(segIdx).curvePointCount == 0) {
                    LatLon fromInt = getIntersectionPosition(getInfoStreetSegment(segIdx).from);
                    LatLon toInt = getIntersectionPosition(getInfoStreetSegment(segIdx).to);

                    std::pair<double, double> xy_1 = xy_from_point(fromInt);
                    std::pair<double, double> xy_2 = xy_from_point(toInt);

                    if (xy_1.first * EARTH_RADIUS_METERS < top_xy.first && xy_1.first * EARTH_RADIUS_METERS > bot_xy.first && xy_2.first * EARTH_RADIUS_METERS < top_xy.first && xy_2.first * EARTH_RADIUS_METERS > bot_xy.first) {
                        if (xy_1.second * EARTH_RADIUS_METERS < top_xy.second && xy_1.second * EARTH_RADIUS_METERS > bot_xy.second && xy_2.second * EARTH_RADIUS_METERS < top_xy.second && xy_2.second * EARTH_RADIUS_METERS > bot_xy.second) {
                            g->draw_line({xy_1.first*EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS},
                            {
                                xy_2.first*EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS
                            });
                            street_names_count.at(getInfoStreetSegment(segIdx).streetID)++;

                            double deltaX = (xy_2.first - xy_1.first);
                            double deltaY = (xy_2.second - xy_1.second);

                            double length = sqrt(pow(deltaX, 2) + pow(deltaY, 2));


                            double angle = std::atan2(deltaY, deltaX) / DEGREE_TO_RADIAN;

                            if (angle > 90 || angle < -90) {
                                g->set_color(ezgl::BLACK);
                                g->set_text_rotation((angle - 180));
                                if (print_name && (street_names_count.at(getInfoStreetSegment(segIdx).streetID) % interval == 0 || street_names_count.at(getInfoStreetSegment(segIdx).streetID) == 0)) {
                                    g->draw_text({(xy_1.first + xy_2.first) / 2 * EARTH_RADIUS_METERS, (xy_1.second + xy_2.second) / 2 * EARTH_RADIUS_METERS}, "← " + street_name, (length + adder) * EARTH_RADIUS_METERS, DBL_MAX);
                                }
                            } else {
                                g->set_color(ezgl::BLACK);
                                g->set_text_rotation(angle);
                                if (print_name && (street_names_count.at(getInfoStreetSegment(segIdx).streetID) % interval == 0 || street_names_count.at(getInfoStreetSegment(segIdx).streetID) == 0)) {
                                    g->draw_text({(xy_1.first + xy_2.first) / 2 * EARTH_RADIUS_METERS, (xy_1.second + xy_2.second) / 2 * EARTH_RADIUS_METERS}, street_name + " →", (length + adder) * EARTH_RADIUS_METERS, DBL_MAX);

                                }
                            }


                        }

                    }

                } else {
                    LatLon fromPoint;
                    LatLon toPoint;

                    for (int k = 0; k < getInfoStreetSegment(segIdx).curvePointCount; ++k) {
                        if (k == 0) {
                            fromPoint = getIntersectionPosition(getInfoStreetSegment(segIdx).from);
                            toPoint = getStreetSegmentCurvePoint(k, segIdx);
                        } else {
                            fromPoint = getStreetSegmentCurvePoint(k - 1, segIdx);
                            toPoint = getStreetSegmentCurvePoint(k, segIdx);
                        }
                        std::pair<double, double> xy_1 = xy_from_point(fromPoint);
                        std::pair<double, double> xy_2 = xy_from_point(toPoint);

                        if (xy_1.first * EARTH_RADIUS_METERS < top_xy.first && xy_1.first * EARTH_RADIUS_METERS > bot_xy.first && xy_2.first * EARTH_RADIUS_METERS < top_xy.first && xy_2.first * EARTH_RADIUS_METERS > bot_xy.first) {
                            if (xy_1.second * EARTH_RADIUS_METERS < top_xy.second && xy_1.second * EARTH_RADIUS_METERS > bot_xy.second && xy_2.second * EARTH_RADIUS_METERS < top_xy.second && xy_2.second * EARTH_RADIUS_METERS > bot_xy.second) {
                                g->draw_line({xy_1.first*EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS},
                                {
                                    xy_2.first*EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS
                                });

                                if (k == getInfoStreetSegment(segIdx).curvePointCount / 2) {
                                    street_names_count.at(getInfoStreetSegment(segIdx).streetID)++;

                                    double deltaX = (xy_2.first - xy_1.first);
                                    double deltaY = (xy_2.second - xy_1.second);

                                    double length = sqrt(pow(deltaX, 2) + pow(deltaY, 2));


                                    double angle = std::atan2(deltaY, deltaX) / DEGREE_TO_RADIAN;

                                    if (angle > 90 || angle < -90) {
                                        g->set_color(ezgl::BLACK);
                                        g->set_text_rotation((angle - 180));
                                        if (street_name != "<unknown>" && (street_names_count.at(getInfoStreetSegment(segIdx).streetID) % interval == 0 || street_names_count.at(getInfoStreetSegment(segIdx).streetID) == 0)) {
                                            g->draw_text({(xy_1.first + xy_2.first) / 2 * EARTH_RADIUS_METERS, (xy_1.second + xy_2.second) / 2 * EARTH_RADIUS_METERS}, "← " + street_name, (length) * EARTH_RADIUS_METERS, DBL_MAX);
                                        }
                                    } else {
                                        g->set_color(ezgl::BLACK);
                                        g->set_text_rotation(angle);
                                        if (street_name != "<unknown>" && (street_names_count.at(getInfoStreetSegment(segIdx).streetID) % interval == 0 || street_names_count.at(getInfoStreetSegment(segIdx).streetID) == 0)) {
                                            g->draw_text({(xy_1.first + xy_2.first) / 2 * EARTH_RADIUS_METERS, (xy_1.second + xy_2.second) / 2 * EARTH_RADIUS_METERS}, street_name + " →", (length) * EARTH_RADIUS_METERS, DBL_MAX);
                                        }
                                    }
                                    g->set_color(colours[0], colours[1], colours[2], colours[3]);

                                }
                            }
                        }
                    }

                    fromPoint = getStreetSegmentCurvePoint(getInfoStreetSegment(segIdx).curvePointCount - 1, segIdx);
                    toPoint = getIntersectionPosition(getInfoStreetSegment(segIdx).to);

                    std::pair<double, double> xy_1 = xy_from_point(fromPoint);
                    std::pair<double, double> xy_2 = xy_from_point(toPoint);

                    if (xy_1.first * EARTH_RADIUS_METERS < top_xy.first && xy_1.first * EARTH_RADIUS_METERS > bot_xy.first && xy_2.first * EARTH_RADIUS_METERS < top_xy.first && xy_2.first * EARTH_RADIUS_METERS > bot_xy.first) {
                        if (xy_1.second * EARTH_RADIUS_METERS < top_xy.second && xy_1.second * EARTH_RADIUS_METERS > bot_xy.second && xy_2.second * EARTH_RADIUS_METERS < top_xy.second && xy_2.second * EARTH_RADIUS_METERS > bot_xy.second) {
                            g->draw_line({xy_1.first*EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS},
                            {
                                xy_2.first*EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS
                            });
                        }
                    }

                }
            }

        }
    }

    auto& big_streets = data.segements_big;
    for (int j = 0; j < big_streets.size(); ++j) {
        int segIdx = (int) big_streets[j];
        OSMID way_id = getInfoStreetSegment(segIdx).wayOSMID;
        auto search_way = data.waypointer_from_wayid.find(way_id);
        const OSMWay* way = search_way->second;

        int draw_weight = 0;
        for (int i = 0; i < getTagCount(way); ++i) {
            std::string key, value;
            std::tie(key, value) = getTagPair(way, i);

            if (key == "highway") {
                draw_weight = find_weight(value);
                determine_factors(g, value);
            }
        }


        bool draw = false;
        if (zoom_level < X_4) {
            if (draw_weight > 0) draw = true;
        }
        if (zoom_level < X_3) {
            if (draw_weight > 1) {
                draw = true;
            }
        }
        if (zoom_level < X_2) {
            if (draw_weight > 1) {
                draw = true;
            }
        }
        if (zoom_level < X_1) {
            if (draw_weight > 1) {
                draw = true;
            }
        }

        if (zoom_level > X_1) {
            if (draw_weight > 2) {
                draw = true;
            }
        }

        if (draw) {

            if (getInfoStreetSegment(segIdx).curvePointCount == 0) {
                LatLon fromInt = getIntersectionPosition(getInfoStreetSegment(segIdx).from);
                LatLon toInt = getIntersectionPosition(getInfoStreetSegment(segIdx).to);

                std::pair<double, double> xy_1 = xy_from_point(fromInt);
                std::pair<double, double> xy_2 = xy_from_point(toInt);
                if (xy_1.first * EARTH_RADIUS_METERS < top_xy.first && xy_1.first * EARTH_RADIUS_METERS > bot_xy.first && xy_2.first * EARTH_RADIUS_METERS < top_xy.first && xy_2.first * EARTH_RADIUS_METERS > bot_xy.first) {
                    if (xy_1.second * EARTH_RADIUS_METERS < top_xy.second && xy_1.second * EARTH_RADIUS_METERS > bot_xy.second && xy_2.second * EARTH_RADIUS_METERS < top_xy.second && xy_2.second * EARTH_RADIUS_METERS > bot_xy.second) {


                        g->draw_line({xy_1.first*EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS},
                        {
                            xy_2.first*EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS
                        });
                    }

                }

            } else {
                LatLon fromPoint;
                LatLon toPoint;

                for (int k = 0; k < getInfoStreetSegment(segIdx).curvePointCount; ++k) {
                    if (k == 0) {
                        fromPoint = getIntersectionPosition(getInfoStreetSegment(segIdx).from);
                        toPoint = getStreetSegmentCurvePoint(k, segIdx);
                    } else {
                        fromPoint = getStreetSegmentCurvePoint(k - 1, segIdx);
                        toPoint = getStreetSegmentCurvePoint(k, segIdx);
                    }
                    std::pair<double, double> xy_1 = xy_from_point(fromPoint);
                    std::pair<double, double> xy_2 = xy_from_point(toPoint);

                    if (xy_1.first * EARTH_RADIUS_METERS < top_xy.first && xy_1.first * EARTH_RADIUS_METERS > bot_xy.first && xy_2.first * EARTH_RADIUS_METERS < top_xy.first && xy_2.first * EARTH_RADIUS_METERS > bot_xy.first) {
                        if (xy_1.second * EARTH_RADIUS_METERS < top_xy.second && xy_1.second * EARTH_RADIUS_METERS > bot_xy.second && xy_2.second * EARTH_RADIUS_METERS < top_xy.second && xy_2.second * EARTH_RADIUS_METERS > bot_xy.second) {
                            g->draw_line({xy_1.first*EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS},
                            {
                                xy_2.first*EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS
                            });
                        }
                    }
                }

                fromPoint = getStreetSegmentCurvePoint(getInfoStreetSegment(segIdx).curvePointCount - 1, segIdx);
                toPoint = getIntersectionPosition(getInfoStreetSegment(segIdx).to);

                std::pair<double, double> xy_1 = xy_from_point(fromPoint);
                std::pair<double, double> xy_2 = xy_from_point(toPoint);

                if (xy_1.first * EARTH_RADIUS_METERS < top_xy.first && xy_1.first * EARTH_RADIUS_METERS > bot_xy.first && xy_2.first * EARTH_RADIUS_METERS < top_xy.first && xy_2.first * EARTH_RADIUS_METERS > bot_xy.first) {
                    if (xy_1.second * EARTH_RADIUS_METERS < top_xy.second && xy_1.second * EARTH_RADIUS_METERS > bot_xy.second && xy_2.second * EARTH_RADIUS_METERS < top_xy.second && xy_2.second * EARTH_RADIUS_METERS > bot_xy.second) {
                        g->draw_line({xy_1.first*EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS},
                        {
                            xy_2.first*EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS
                        });
                    }
                }
            }
        }

    }

    std::unordered_map<std::string, int> text_lengths;
    for (int j = 0; j < big_streets.size(); j++) {

        int index = big_streets.at(j);
        LatLon fromInt = getIntersectionPosition(getInfoStreetSegment(index).from);
        LatLon toInt = getIntersectionPosition(getInfoStreetSegment(index).to);

        std::pair<double, double> xy_1 = xy_from_point(fromInt);
        std::pair<double, double> xy_2 = xy_from_point(toInt);
        std::string street_name = getStreetName(getInfoStreetSegment(index).streetID);

        street_names_count.at(getInfoStreetSegment(index).streetID)++;

        double deltaX = (xy_2.first - xy_1.first);
        double deltaY = (xy_2.second - xy_1.second);

        double length = sqrt(pow(deltaX, 2) + pow(deltaY, 2));

        text_lengths[street_name]++;


        double angle = std::atan2(deltaY, deltaX) / DEGREE_TO_RADIAN;

        if (angle > 90 || angle < -90) {
            g->set_color(ezgl::BLACK);
            g->set_text_rotation((angle - 180));
            if (street_name != "<unknown>" && text_lengths[street_name] % 61 == 0) {
                g->draw_text({(xy_1.first + xy_2.first) / 2 * EARTH_RADIUS_METERS, (xy_1.second + xy_2.second) / 2 * EARTH_RADIUS_METERS}, street_name, (length + adder) * EARTH_RADIUS_METERS, DBL_MAX);
            }
        } else {
            g->set_color(ezgl::BLACK);
            g->set_text_rotation(angle);
            if (street_name != "<unknown>" && text_lengths[street_name] % 61 == 0) {
                g->draw_text({(xy_1.first + xy_2.first) / 2 * EARTH_RADIUS_METERS, (xy_1.second + xy_2.second) / 2 * EARTH_RADIUS_METERS}, street_name, (length + adder) * EARTH_RADIUS_METERS, DBL_MAX);

            }
        }
    }
}

extern int mouse1, mouse2, internal_1, internal_2;
extern bool shown_sidebar;
extern ezgl::application* appication;
extern std::vector <POI> pois_selected;
extern bool shown;
extern bool shown_directions_bar;
extern void show_directions();
extern void hide_directions();
extern void find_directions(int, int);
extern gulong input_changed_id, input_changed_id2;
extern void draw_pois(ezgl::renderer * g);

// This function is used to find when a street is clicked, and calls the highlighting
// logic in order to highlight the street

void highlight_selected(ezgl::renderer*, LatLon pos, double radius, double x, double y) {

    std::vector<std::pair < std::string, int>> close_items = hit_detect(pos, radius);

    for (int i = 0; i < close_items.size(); ++i) {

        int id = close_items.at(i).second;

        if (close_items.at(i).first == "str") {

            //draw_street(g, data.segments_per_street.at(id), ezgl::YELLOW, ezgl::line_cap::round, ezgl::line_dash::none);

            show_popup(getStreetName(id));

        }

        if (close_items[i].first == "int") {


            if (mouse1 == -1) {

                mouse1 = close_items[i].second;

                std::cout << "M1 : " << mouse1 << "M2 : " << mouse2 << "\n";

                GtkEntry* text_input = (GtkEntry *) appication->get_object("TextInput");

                auto psy = getIntersectionName(mouse1);

                const char* phy = psy.c_str();

                g_signal_handler_block((GObject*) text_input, input_changed_id);

                gtk_entry_set_text(text_input, (const char*) phy);

                g_signal_handler_unblock((GObject*) text_input, input_changed_id);

                POI p;

                p.location = std::make_pair(x, y);

                pois_selected.push_back(p);

                draw_pois(appication->get_renderer());

                appication->flush_drawing();

                internal_1 = -1;

                internal_2 = -1;

                appication->update_message("Intersection recorded. Click on any other intersection to obtain route directions. To reset press the clear button.");

                continue;

            }

            if (mouse1 != -1 && mouse2 == -1) {

                mouse2 = close_items[i].second;

                std::cout << "M1 : " << mouse1 << "M2 : " << mouse2 << "\n";

                POI p;

                p.location = std::make_pair(x, y);

                pois_selected.push_back(p);

                draw_pois(appication->get_renderer());

                appication->flush_drawing();

                auto spane = (GtkWidget *) appication->get_object("spinner");

                gtk_spinner_start((GtkSpinner*) spane);

                appication->update_message("Please be patient while we calculate your route.");

                GtkEntry* text_input = (GtkEntry *) appication->get_object("destBox");

                auto psy = getIntersectionName(mouse2);

                const char* phy = psy.c_str();

                g_signal_handler_block((GObject*) text_input, input_changed_id2);

                gtk_entry_set_text(text_input, (const char*) phy);

                g_signal_handler_unblock((GObject*) text_input, input_changed_id2);

                if (!shown_directions_bar) {

                    GObject* spane_1 = (appication->get_object("directionsMode"));

                    gtk_widget_show((GtkWidget*) spane_1);

                    shown_directions_bar = true;

                }


            }


        } else {

            continue;

        }
    }
}

// This function is used in order to highlight a street, depending on user click

void draw_street(ezgl::renderer *g, std::vector<int> segments, ezgl::color COLOR, ezgl::line_cap cap, ezgl::line_dash dash) {

    g->set_line_cap(cap);

    g->set_color(COLOR);

    g->set_line_dash(dash);

    ezgl::rectangle draw_area = g->get_visible_world();

    g->set_line_width(5);

    double factor = 1.001;
    std::pair<double, double> bot_xy = std::make_pair(draw_area.m_first.x*factor, draw_area.m_first.y / factor);
    std::pair<double, double> top_xy = std::make_pair(draw_area.m_second.x / factor, draw_area.m_second.y * factor);

    for (int i = 0; i < segments.size(); ++i) {
        int segIdx = segments.at(i);

        //std::cout << segIdx << "\n";

        if (getInfoStreetSegment(segIdx).curvePointCount == 0) {
            LatLon fromInt = getIntersectionPosition(getInfoStreetSegment(segIdx).from);
            LatLon toInt = getIntersectionPosition(getInfoStreetSegment(segIdx).to);

            std::pair<double, double> xy_1 = xy_from_point(fromInt);
            std::pair<double, double> xy_2 = xy_from_point(toInt);

            if (xy_1.first * EARTH_RADIUS_METERS < top_xy.first && xy_1.first * EARTH_RADIUS_METERS > bot_xy.first && xy_2.first * EARTH_RADIUS_METERS < top_xy.first && xy_2.first * EARTH_RADIUS_METERS > bot_xy.first) {
                if (xy_1.second * EARTH_RADIUS_METERS < top_xy.second && xy_1.second * EARTH_RADIUS_METERS > bot_xy.second && xy_2.second * EARTH_RADIUS_METERS < top_xy.second && xy_2.second * EARTH_RADIUS_METERS > bot_xy.second) {
                    double deltaX = (xy_2.first - xy_1.first);
                    double deltaY = (xy_2.second - xy_1.second);
                    double angle;

                    if (deltaX == 0) {
                        angle = 90;
                    } else {
                        angle = std::atan2(deltaY, deltaX);
                    }

                    g->draw_line({xy_1.first*EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS},
                    {
                        xy_2.first*EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS
                    });


                    g->set_text_rotation(angle / DEGREE_TO_RADIAN);
                }
            }

        } else {
            LatLon fromPoint;
            LatLon toPoint;

            for (int k = 0; k < getInfoStreetSegment(segIdx).curvePointCount; ++k) {
                if (k == 0) {
                    fromPoint = getIntersectionPosition(getInfoStreetSegment(segIdx).from);
                    toPoint = getStreetSegmentCurvePoint(k, segIdx);
                } else {
                    fromPoint = getStreetSegmentCurvePoint(k - 1, segIdx);
                    toPoint = getStreetSegmentCurvePoint(k, segIdx);
                }
                std::pair<double, double> xy_1 = xy_from_point(fromPoint);
                std::pair<double, double> xy_2 = xy_from_point(toPoint);

                if (xy_1.first * EARTH_RADIUS_METERS < top_xy.first && xy_1.first * EARTH_RADIUS_METERS > bot_xy.first && xy_2.first * EARTH_RADIUS_METERS < top_xy.first && xy_2.first * EARTH_RADIUS_METERS > bot_xy.first) {
                    if (xy_1.second * EARTH_RADIUS_METERS < top_xy.second && xy_1.second * EARTH_RADIUS_METERS > bot_xy.second && xy_2.second * EARTH_RADIUS_METERS < top_xy.second && xy_2.second * EARTH_RADIUS_METERS > bot_xy.second) {
                        g->draw_line({xy_1.first*EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS},
                        {
                            xy_2.first*EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS
                        });
                    }
                }
            }

            fromPoint = getStreetSegmentCurvePoint(getInfoStreetSegment(segIdx).curvePointCount - 1, segIdx);
            toPoint = getIntersectionPosition(getInfoStreetSegment(segIdx).to);

            std::pair<double, double> xy_1 = xy_from_point(fromPoint);
            std::pair<double, double> xy_2 = xy_from_point(toPoint);

            if (xy_1.first * EARTH_RADIUS_METERS < top_xy.first && xy_1.first * EARTH_RADIUS_METERS > bot_xy.first && xy_2.first * EARTH_RADIUS_METERS < top_xy.first && xy_2.first * EARTH_RADIUS_METERS > bot_xy.first) {
                if (xy_1.second * EARTH_RADIUS_METERS < top_xy.second && xy_1.second * EARTH_RADIUS_METERS > bot_xy.second && xy_2.second * EARTH_RADIUS_METERS < top_xy.second && xy_2.second * EARTH_RADIUS_METERS > bot_xy.second) {

                    g->draw_line({xy_1.first*EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS},
                    {
                        xy_2.first*EARTH_RADIUS_METERS, xy_2.second * EARTH_RADIUS_METERS
                    });
                }
            }
        }
    }

    g->set_line_cap(ezgl::line_cap::butt);

    g->set_line_dash(ezgl::line_dash::none);

    g->set_color(ezgl::BLACK);

    g->set_line_width(1);
}
