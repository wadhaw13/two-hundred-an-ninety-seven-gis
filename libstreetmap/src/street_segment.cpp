#include "m1.h"
#include "StreetsDatabaseAPI.h"
#include <cmath>
#include <utility>
#include <vector>

#define KM_TO_M 1000
#define H_TO_S 3600

#include "Data_Structures.h"

// Returns a vector of ints that contains all the segments connected to an intersection
 std::vector<int> find_street_segments_of_intersection(int intersection_id) {
    return data.segments_per_intersection.at(intersection_id);
}
 
 // Returns a vector of ints that contains all the segments that belong to a single street
 std::vector<int> find_street_segments_of_street(int street_id) {
     return data.segments_per_street.at(street_id);
}

 // Returns the length of a particular street segment given the street segment ID
 double find_street_segment_length(int street_segment_id) {
     return data.segment_lengths.at(street_segment_id);
}
 
 // Returns the travel time of a street segment given a street segment ID
 double find_street_segment_travel_time(int street_segment_id) {
     return data.segment_travel_time.at(street_segment_id);
}



