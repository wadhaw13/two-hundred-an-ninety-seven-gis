#include "poi.h"

LatLon point_from_xy(double x, double y) {

    double COS = cos(DEGREE_TO_RADIAN * (LAT_TOP + LAT_BOT) / 2);

    double lon = x / (COS * DEGREE_TO_RADIAN * EARTH_RADIUS_METERS);

    double lat = y / (DEGREE_TO_RADIAN * EARTH_RADIUS_METERS);

    LatLon new_cord(lat, lon);

    return new_cord;

}

int load_points_of_interest() {

    try {

        for (int i = 0; i < getNumPointsOfInterest(); i++) {

            POI poi;

            poi.idx = i;

            data.points_of_interest.insert(std::make_pair(getPointOfInterestType(i), poi));

        }
    } catch (std::exception e) {

        std::cout << e.what() << "\n";

        return -1;

    }


    return 0;

}


