#include "m1.h"
#include "shortest_paths.h"
#include "adjacent_headers.h"
#pragma once


void algo_solution(
        const std::vector<DeliveryInfo>& deliveries,
        const std::vector<int>& depots,
        const float turn_penalty,
        const float truck_capacity,
        std::vector<CourierSubpath> & solution,
        int start_index);

int find_depot_intersection(int segment_id, const std::vector<int> & depots);
void pre_compute_paths(const float turn_penalty, const std::vector<DeliveryInfo>& deliveries);
double findTravelTime(std::vector <StreetSegmentIndex> vec);
double find_travel_time_courier(std::vector<CourierSubpath> & solution );
extern std::unordered_map<int, std::priority_queue<paths, std::vector<paths>, travel_time_comparator>> pre_computed;
extern std::unordered_map<int, std::vector<int>> delivery_map_global;
