/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GUIObject.h
 * Author: wadhaw13
 * ECE297 2020
 * Created on February 24, 2020, 4:02 PM
 */

#ifndef GUIOBJECT_H
#define GUIOBJECT_H

#include "ezgl/graphics.hpp"
#include "ezgl/application.hpp"

class GUIObject {

public:

    virtual int render() = 0;

    GUIObject(ezgl::renderer* g) : m_graphics(g) {
    }

    virtual ~GUIObject() {

    }

protected:

    ezgl::renderer* m_graphics;
    double m_x = 0;
    double m_y = 0;
    double m_w = 100;
    double m_h = 100;    

};



#endif /* GUIOBJECT_H */

