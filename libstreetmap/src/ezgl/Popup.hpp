/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Popup.hpp
 * Author: wadhaw13
 *
 * Created on February 24, 2020, 4:08 PM
 */

#ifndef POPUP_HPP
#define POPUP_HPP

#include "ezgl/GUIObject.h"
#include "ezgl/rectangle.hpp"
#include "ezgl/color.hpp"
#include <string>

class Popup : public GUIObject {

    public:
    
        Popup(ezgl::renderer* g, int x, int y, int w, int h);
        int render();
        void set_string(std::string);
        virtual ~Popup();

    private:
        
        ezgl::rectangle* m_rect;
        double m_r = 5.0;
        std::string m_text = "NULL";
        void draw_rects();
        void draw_arcs();

};

#endif /* POPUP_HPP */

