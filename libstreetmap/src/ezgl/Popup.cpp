/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Popup.cpp
 * Author: wadhaw13
 * 
 * Created on February 24, 2020, 4:08 PM
 */

#include "Popup.hpp"

Popup::Popup(ezgl::renderer* g, int x, int y, int w, int h) : GUIObject(g) {

    m_x = x;
    m_y = y;
    m_w = w;
    m_h = h;
    ezgl::point2d pt(m_x, m_y);
    m_rect = new ezgl::rectangle(pt, m_w, m_h);
    m_r = ((m_w+m_h)*0.5)/10.0;
}

int Popup::render() {

    m_graphics->set_coordinate_system(ezgl::SCREEN);

    m_graphics->set_color(ezgl::WHITE);

    m_graphics->set_text_rotation(0);

    draw_rects();
    
    draw_arcs();

    m_graphics->set_color(ezgl::BLACK);

    m_graphics->set_line_width(1);    

    m_graphics->set_font_size(11);

    m_graphics->set_color(ezgl::BLACK);

    m_graphics->draw_text({ (m_rect->left() + m_rect->right())/2 , m_rect->bottom() + 25}, m_text );

    m_graphics->set_coordinate_system(ezgl::WORLD);    

    return 0;

}

void Popup::set_string(std::string text) {

    this->m_text = text;

}

Popup::~Popup() {

    delete this->m_rect;

}

void Popup::draw_rects(){

    //top rect
    m_graphics->fill_rectangle({m_x + m_r, m_y}, m_w - 2*m_r, m_r) ;
    
    //middle rect
    m_graphics->fill_rectangle({m_x , m_y + m_r}, m_w, m_h - 2*m_r) ;
    
    //bottom rect
    m_graphics->fill_rectangle({m_x + m_r, m_y - m_r + m_h}, m_w - 2*m_r, m_r) ;
    
    
}

void Popup::draw_arcs(){    
    
    m_graphics ->fill_arc({m_x + m_r, m_y + m_r}, m_r, 90, 90);    
    
    m_graphics ->fill_arc({m_x + m_w - m_r, m_y + m_r}, m_r, 0, 90);
        
    m_graphics ->fill_arc({m_x + m_r, m_y + m_h - m_r}, m_r, 180, 90);
        
    m_graphics ->fill_arc({m_x + m_w - m_r, m_y + m_h -m_r}, m_r, 0, -90);
    
}

