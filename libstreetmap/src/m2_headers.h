/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   m2_headers.h
 * Author: wadhaw13
 * ECE297 2020
 * Created on March 10, 2020, 2:40 AM
 */

#ifndef M2_HEADERS_H
#define M2_HEADERS_H

#include "streets_draw.h"
#include "poi.h"
#include "DataService.h"
std::vector <struct POI> pois_selected;
#include "m1.h"
#include "Data_Structures.h"
#include "LatLon.h"
#include "ezgl/Popup.hpp"
#include "ezgl/camera.hpp"
#include <vector>
#include <fstream>
#include <thread>
#include <set>
#include "ezgl/control.hpp"

#define TOTAL_BTNS 19

DataService service;

ezgl::surface* png_surface;
ezgl::surface* marker_surf;
ezgl::surface* pedestrian_surf;
ezgl::surface* auto_surf;
ezgl::surface* cycl_surf;

GtkWidget* view;
GtkTextBuffer* buffer;

ezgl::application* appication;

double LAT_TOP;

double LAT_BOT;

double init_area;

bool vis = false;
ezgl::rectangle draw_this;

int calculate_initial_world();
int load_images();
int reset_world_camera();

// Callback functions for event handling
void act_on_mouse_press(ezgl::application *application, GdkEventButton *event, double x, double y);
void act_on_mouse_move(ezgl::application *application, GdkEventButton *event, double x, double y);
void act_on_key_press(ezgl::application *application, GdkEventKey *event, char *key_name);
void initial_setup(ezgl::application *application, bool new_window);
void test_button(GtkWidget *widget, ezgl::application *application);
void animate_button(GtkWidget *widget, ezgl::application *application);
void get_directions_event(GtkWidget *widget, ezgl::application * application);
void get_walk_dir_event(GtkWidget *widget, ezgl::application * application);
void focus_event(GtkWidget *widget, ezgl::application * application);
void close_help_menu(GtkWidget *widget, ezgl::application * application);
void show_help_menu(GtkWidget *widget, ezgl::application * application);
void ack_btn(GtkWidget *widget, ezgl::application * application);

void free_images();
void draw_highlighted(ezgl::renderer*);
void draw_pois(ezgl::renderer*);
void find_points_of_interest(GtkWidget *widget, ezgl::application *application);
void stop_load();
void run_load();
void find_oints_of_interest(std::string);
double get_pos_weight(int feature_id);
void store_streets_by_x_y();
int add_points_of_interest(std::string type);

//Added more features

void dent(GtkWidget *widget, ezgl::application *application);
void rest(GtkWidget *widget, ezgl::application *application);
void univ(GtkWidget *widget, ezgl::application *application);
void bench(GtkWidget *widget, ezgl::application *application);
void atm(GtkWidget *widget, ezgl::application *application);
void bar(GtkWidget *widget, ezgl::application *application);
void cine(GtkWidget *widget, ezgl::application *application);
void club(GtkWidget *widget, ezgl::application *application);
void casi(GtkWidget *widget, ezgl::application *application);
void phar(GtkWidget *widget, ezgl::application *application);
void hide_dir_btn(GtkWidget *widget, ezgl::application *application);
void text_input_changed(GtkWidget *widget, ezgl::application *application);
void search_option_clicked2(GtkWidget *widget, ezgl::application *application);
void dest_input_changed(GtkWidget *widget, ezgl::application *application);
void update_suggestions(std::vector<int>& suggestions, int opt);
void search_option_clicked(GtkWidget *widget, ezgl::application *application);
void enter_hit(GtkWidget *widget, ezgl::application *application);


void clear_btn(GtkWidget *widget, ezgl::application * app);
bool comp(const std::pair<double, int> &a, const std::pair<double, int> &b);
std::pair<LatLon, LatLon> set_initial_world();
bool comp3(const std::vector<double> &a, const std::vector<double> &b);

//Booleans
bool pep_click = false;
bool car_click = false;
bool cyc_click = false;
bool shown_sidebar = false;
bool shown = false;
bool shown_directions_bar = false;
bool show_suggestions_box = false;
bool show_suggestions_box2 = false;

void clear_directions_list();
void show_directions();
void hide_directions();
void load_directions(std::vector <std::string> & directions);
void delete_suggestions(int);

//GUI related funcs
void hide_sidebar(GtkWidget *widget, ezgl::application* application);
void poi_button(GtkWidget */*widget*/, ezgl::application *application);

int display_intersection_information(IntersectionIndex id, int);
int pan_to_intersection(IntersectionIndex, ezgl::application*);
int general_pan(LatLon);

std::vector <std::pair <GtkWidget*, IntersectionIndex>> street_suggestions;
std::vector <GtkWidget*> TEXTBOX_REFERENCES;
std::vector<int> intersection;

int internal_1 = -1, internal_2 = -1, mouse1 = -1, mouse2 = -1;
gulong input_changed_id, input_changed_id2;

//ADDING more defnitions here to get rid of compiler warnings
void text_input_notify(GtkWidget *widget, ezgl::application *application) ;
bool comp2(const std::pair<double, int> &a, const std::pair<double, int> &b);
void store_big_features_by_area_small_by_pos() ;
int reset_world_camera(ezgl::application * app);
double determine_poi_hit_radius(double zoom) ;
void change_map(GtkWidget *widget, ezgl::application * application);
#endif /* M2_HEADERS_H */

