/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DirectionClassifier.cpp
 * Author: wadhaw13
 * 
 * Created on March 23, 2020, 5:14 PM
 */

#include "DirectionClassifier.h"

Direction DirectionClassifier::classify(LatLon origin, LatLon dest) {

    //dy  //dx
    if ((-origin.lat() + dest.lat()) > 0 && (-origin.lon() + dest.lon()) > 0) return Direction::NORTH_EAST;
    
    else if ((-origin.lat() + dest.lat()) > 0 && (-origin.lon() + dest.lon()) < 0) return Direction::NORTH_WEST;

    else if ((-origin.lat() + dest.lat()) < 0 && (-origin.lon() + dest.lon()) < 0) return Direction::SOUTH_WEST;

    else if ((-origin.lat() + dest.lat()) < 0 && (-origin.lon() + dest.lon()) > 0) return Direction::SOUTH_EAST;

    else if ((-origin.lat() + dest.lat()) == 0 && (-origin.lon() + dest.lon()) > 0) return Direction::EAST;

    else if ((-origin.lat() + dest.lat()) > 0 && (-origin.lon() + dest.lon()) == 0) return Direction::NORTH;

    else if ((-origin.lat() + dest.lat()) < 0 && (-origin.lon() + dest.lon()) == 0) return Direction::SOUTH;

    else if ((-origin.lat() + dest.lat()) == 0 && (-origin.lon() + dest.lon()) < 0) return Direction::WEST;
    return Direction::DEFAULT;
}

std::ostream& operator<<(std::ostream& lhs, Direction& d) {

    if (d == Direction::EAST) lhs << "EAST";
    else if (d == Direction::NORTH) lhs << "NORTH";
    else if (d == Direction::SOUTH) lhs << "SOUTH";
    else if (d == Direction::WEST) lhs << "WEST";
    else if (d == Direction::NORTH_EAST) lhs << "N_EAST";
    else if (d == Direction::SOUTH_EAST) lhs << "S_EAST";
    else if (d == Direction::SOUTH_WEST) lhs << "S_WEST";
    else if (d == Direction::NORTH_WEST) lhs << "N_WEST";

    return lhs;

}

std::string operator<<(std::string& lhs, Direction& d) {

    if (d == Direction::EAST) lhs = "EAST";
    else if (d == Direction::NORTH) lhs = "NORTH";
    else if (d == Direction::SOUTH) lhs = "SOUTH";
    else if (d == Direction::WEST) lhs = "WEST";
    else if (d == Direction::NORTH_EAST) lhs = "N_EAST";
    else if (d == Direction::SOUTH_EAST) lhs = "S_EAST";
    else if (d == Direction::SOUTH_WEST) lhs = "S_WEST";
    else if (d == Direction::NORTH_WEST) lhs = "N_WEST";

    return lhs;

}