/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RequestBuilder.h
 * Author: wadhaw13
 * ECE297 2020
 * Created on March 26, 2020, 1:26 AM
 */

#ifndef REQUESTBUILDER_H
#define REQUESTBUILDER_H

#include <regex>

class RequestBuilder {
public:

    RequestBuilder(std::string key = "") {

        if (key == "") {

            this->API_KEY = API_KEY_DEFAULT;

        }

    }

    std::string TextSearch(std::string text) {

        std::regex space(" ");

        auto chopped = std::regex_replace(text, space, "%20");

        std::string command = TEXT_URL + (INPUT + chopped) + "&" + INPUT_TYPE + "&" + KEY + API_KEY;

        return command;

    }

    std::string LocationSearch(double lat, double lon, int radius = 500) {

        return (LOC_URL + LOCATION + std::to_string(lat) + "," + std::to_string(lon) + "&" + RAD + std::to_string(radius) + "&" + KEY + API_KEY);

    }

    std::string ImageFromReference(std::string ref) {

        return (PHOTOS_URL + "&" + REFERENCE + ref + "&" + KEY + API_KEY);

    }

private:

    std::string API_KEY = "";

    const std::string TEXT_URL = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?";

    const std::string LOC_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";

    const std::string PHOTOS_URL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400";

    const std::string API_KEY_DEFAULT = "AIzaSyCml_R3ysATLmqW1j3YdmVhp-hXL84GzFA";

    const std::string KEY = "key=";

    const std::string LOCATION = "location=";

    const std::string RAD = "radius=";

    const std::string REFERENCE = "photoreference=";

    const std::string INPUT = "input=";

    const std::string INPUT_TYPE = "inputtype=textquery&fields=photos,formatted_address,name,geometry";


};

#endif /* REQUESTBUILDER_H */

