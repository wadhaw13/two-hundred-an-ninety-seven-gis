#include "m1.h"
#include "StreetsDatabaseAPI.h"
#include <cmath>
#include <cctype>
#include "OSMDatabaseAPI.h"
#include <vector>
#include "Data_Structures.h"
#include <algorithm>
#include <boost/algorithm/string.hpp> 
double find_distance_between_two_points(std::pair<LatLon, LatLon> points) {
     double cos_lat_avg_rad = cos(DEGREE_TO_RADIAN * (points.first.lat() + points.second.lat()) / 2);
    std::pair<double, double> xy_1 = std::make_pair(DEGREE_TO_RADIAN * points.first.lon() * cos_lat_avg_rad, DEGREE_TO_RADIAN * points.first.lat());

    std::pair<double, double> xy_2 = std::make_pair(DEGREE_TO_RADIAN * points.second.lon() * cos_lat_avg_rad, DEGREE_TO_RADIAN * points.second.lat());

    double distance = EARTH_RADIUS_METERS * sqrt(pow((xy_2.second - xy_1.second), 2) + pow((xy_2.first - xy_1.first), 2));

    return distance;

}

bool are_directly_connected(std::pair<int, int> intersection_ids) {
    std::vector<int>seg_s, seg_f;
    int first, second;
    first = intersection_ids.first;
    second = intersection_ids.second;
    if (first == second) return true;
    
    //use data to get the segments for both intersections
    seg_f = data.segments_per_intersection[first];
    seg_s = data.segments_per_intersection[second];

    int numSeg_f = seg_f.size();
    int numSeg_s = seg_s.size();
    
    //loop through to check is common
    for (int i = 0; i < numSeg_f; i++) {
        for (int k = 0; k < numSeg_s; k++) {
            ///CAN MAKE EVEN FSTER using setIntersection() because they are already sorted but some edge case data
            if (seg_f[i] == seg_s[k]) {
                InfoStreetSegment info_1 = getInfoStreetSegment(seg_f[i]);
                //check oneway condition (1->2 is allowed)
                if (info_1.oneWay) {
                    if (info_1.from != first) continue;
                    else return true;
                } else return true;
            }
        }

    } //none found to match (or oneway is false)
    return false;
}

std::vector<int> find_street_ids_from_partial_street_name(std::string street_prefix) {
    std::vector<int> ids;
    int length_pre = street_prefix.length();

    if (length_pre == 0) return ids;

    while (street_prefix[length_pre - 1] == ' ') {
        street_prefix.pop_back(); //trim any ending spaces
    }
    while (street_prefix[0] == ' ') {
        street_prefix.erase(0); //trim any leading spaces
    }
    
    //lowercase and get rid of spaces
    boost::algorithm::to_lower(street_prefix);
    std::string street_prefix_nospace = street_prefix;
    street_prefix_nospace.std::string::erase(std::remove(street_prefix_nospace.begin(), street_prefix_nospace.end(), ' '), street_prefix_nospace.end());
    //const auto &a =  data.adjacent_intersections;
    //add +1 ascii to street_prefix which is useful for lower_bound(don't want to add to z check)
    std::string street_prefix_upper = street_prefix_nospace;
    int size =street_prefix_upper.size(); 
    int i = 1;
    int val = street_prefix_upper[size -i];
    while(val == 'z'){
        i++;
        if(i >= size) return ids;
        val = street_prefix_upper[size - i];
    }
    street_prefix_upper[size-i] = char(street_prefix_upper[size-i] + 1);
    
    //search multimap data for prefix, using lower_bound functions 
    auto qualify = data.street_names_ordered.lower_bound(street_prefix_upper);
    for (auto it = data.street_names_ordered.lower_bound(street_prefix_nospace); it != std::end(data.street_names_ordered) && it != qualify; ++it) {
        ids.push_back(it->second);
    }
    return ids;
}

double find_feature_area(int feature_id) {

    double area = 0;
    int n = getFeaturePointCount(feature_id);
    //check if closed if not return 0
    if (getFeaturePoint(0, feature_id).lat() != getFeaturePoint(n - 1, feature_id).lat()
            || getFeaturePoint(0, feature_id).lon() != getFeaturePoint(n - 1, feature_id).lon()
            ) return area;
    
    //polygon algorithm loop through one way and add
    int j = n - 1; 
    for (int i = 0; i < n; i++) {
        LatLon lli = getFeaturePoint(i, feature_id);
        LatLon llj = getFeaturePoint(j, feature_id);

        double cos_lat_avg_rad = cos(DEGREE_TO_RADIAN * (lli.lat() + llj.lat()) / 2);

        std::pair<double, double> xy_1(DEGREE_TO_RADIAN * lli.lon() * cos_lat_avg_rad, DEGREE_TO_RADIAN * lli.lat());
        std::pair<double, double> xy_2(DEGREE_TO_RADIAN * llj.lon() * cos_lat_avg_rad, DEGREE_TO_RADIAN * llj.lat());

        double xi = EARTH_RADIUS_METERS * xy_1.first;
        double yi = EARTH_RADIUS_METERS * xy_1.second;

        double xj = EARTH_RADIUS_METERS * xy_2.first;
        double yj = EARTH_RADIUS_METERS * xy_2.second;
        area += (xj + xi) * (yj - yi);

        j = i;

    }
    //some data given in reverse so *-1
    if (area < 0) area *= -1;
    area = area / (2);
    return area;
}

double find_way_length(OSMID way_id) {
    //need to get OSMway pointer by the way_id, use hash table
    double length = 0;
    const OSMWay* way;
    auto search_way = data.waypointer_from_wayid.find(way_id);
    if (search_way == data.waypointer_from_wayid.end()) return 0.0; //not found return
    way = search_way->second;

    std::vector<OSMID> nodes = getWayMembers(way); //nodes in way

    int size = nodes.size();
    int j = 1;
    bool closed = isClosedWay(way); //need to add one more segment if closed way
    for (int i = 0; j < size; i++) {
        std::pair<LatLon, LatLon> conseq_nodes;
         //get node pointer from node id
        const OSMNode* nodei;
        const OSMNode* nodej;
        auto node1 = data.nodepointer_from_nodeid.find(nodes[i]);
        auto node2 = data.nodepointer_from_nodeid.find(nodes[j]);

        if (node1 == data.nodepointer_from_nodeid.end()) return 0.0;
        if (node2 == data.nodepointer_from_nodeid.end()) return 0.0;

        nodei = node1->second;
        nodej = node2->second;
        
        //find distance between and add
        conseq_nodes.first = getNodeCoords(nodei);
        conseq_nodes.second = getNodeCoords(nodej);
        length += find_distance_between_two_points(conseq_nodes);
        j++;
    }

    if (closed) { //check if closed and add one more iteration
        int i = 0;
        int k = size - 1;
        std::pair<LatLon, LatLon> conseq_nodes;

        const OSMNode* nodei;
        const OSMNode* nodej;
        auto node1 = data.nodepointer_from_nodeid.find(nodes[i]);
        auto node2 = data.nodepointer_from_nodeid.find(nodes[k]);
        if (node1 == data.nodepointer_from_nodeid.end()) return 0.0;
        if (node2 == data.nodepointer_from_nodeid.end()) return 0.0;

        nodei = node1->second;
        nodej = node2->second;

        conseq_nodes.first = getNodeCoords(nodei);
        conseq_nodes.second = getNodeCoords(nodej);;
        length += find_distance_between_two_points(conseq_nodes);

    }

    return length;
}
