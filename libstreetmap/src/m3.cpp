#include "m3.h"
#include "m1.h"
#include "streets_draw.h"
#include "feature_find.h"
#include "StreetsDatabaseAPI.h"
#include "Node.h"
#include "AStar_New.h"
#include "ezgl/application.hpp"
#include <set>
#include <chrono>

// Returns the time required to travel along the path specified, in seconds.
// The path is given as a vector of street segment ids, and this function can
// assume the vector either forms a legal path or has size == 0.  The travel
// time is the sum of the length/speed-limit of each street segment, plus the
// given turn_penalty (in seconds) per turn implied by the path.  If there is
// no turn, then there is no penalty. Note that whenever the street id changes
// (e.g. going from Bloor Street West to Bloor Street East) we have a turn.
//H
std::vector<int> get_optimal_segs(std::vector<int> & intersections, double walking_speed);

void find_path_between_intersections_walk(
        const IntersectionIndex intersect_id_start,
        const IntersectionIndex intersect_id_end,
        const double turn_penalty, double walking_speed, double walking_time_limit, std::vector<int>& walk_path,  std::vector<int>& drive_path);

double compute_path_travel_time(const std::vector<StreetSegmentIndex>& path,
        const double turn_penalty) {

    //Algorithm
    //Iterate through all the street segments.
    //Add each street segment's travel time to the sum.
    //Store the current Street_id.
    //Upon the next iteration if the street_id changes add a turn penalty to the sum.

    /****************** WE ARE GIVEN THE TURN PENALTY*/
    double sum = 0.0;
    int currentIndex = -1;

    for (auto it = path.begin(); it != path.end(); it++) {
        auto info = getInfoStreetSegment(*it);
        StreetIndex index = info.streetID;
        sum += data.segment_travel_time.at(*it);
        
        if(it == path.begin()) currentIndex = index; 
        
        if (index != currentIndex) {
            currentIndex = index;
            sum += turn_penalty;
        }
    }
    return sum;

}


// Returns a path (route) between the start intersection and the end
// intersection, if one exists. This routine should return the shortest path
// between the given intersections, where the time penalty to turn right or
// left is given by turn_penalty (in seconds).  If no path exists, this routine
// returns an empty (size == 0) vector.  If more than one path exists, the path
// with the shortest travel time is returned. The path is returned as a vector
// of street segment ids; traversing these street segments, in the returned
// order, would take one from the start to the end intersection.

std::vector<StreetSegmentIndex> find_path_between_intersections(
        const IntersectionIndex intersect_id_start,
        const IntersectionIndex intersect_id_end,
        const double turn_penalty) {

    std::vector <int> street_segs;
    algorithm_a_star(nodes.at(intersect_id_start), nodes.at(intersect_id_end), turn_penalty, street_segs);
   
    std::reverse(street_segs.begin(), street_segs.end());

    for (int i = 0; i < nodes.size(); ++i){
        (nodes.at(i))->resetNode();
    }
    return street_segs;
}


// Returns the time required to "walk" along the path specified, in seconds.
// The path is given as a vector of street segment ids. The vector can be of
// size = 0, and in this case, it the function should return 0. The walking
// time is the sum of the length/<walking_speed> for each street segment, plus
// the given turn penalty, in seconds, per turn implied by the path. If there
// is no turn, then there is no penalty.  As mentioned above, going from Bloor
// Street West to Bloor street East is considered a turn
//H

double compute_path_walking_time(const std::vector<StreetSegmentIndex>& path,
        const double walking_speed,
        const double turn_penalty) {

    //Algorithm
    //Iterate through all the street segments.
    //Add each street segment's travel time to the sum.
    //Store the current Street_id.
    //Upon the next iteration if the street_id changes add a turn penalty to the sum.
    //We are assuming that the walking speed is provided in the units of km/h

    double sum = 0.0;
    int currentIndex = -1;
    double length = 0.0;

    for (auto it = path.begin(); it != path.end(); it++) {
        auto info = getInfoStreetSegment(*it);
        StreetIndex index = info.streetID;

        //get length in metres and convert it to kilometres.
        length = data.segment_lengths[*it];

        //calculate and add the time taken
        sum += (length / walking_speed);
        
        if(it == path.begin()) currentIndex = index; 
        
        if (index != currentIndex) {
            currentIndex = index;
            sum += turn_penalty;
        }
    }
    return sum;
}


// This is an "uber pool"-like function. The idea is to minimize driving travel
// time by walking to a pick-up intersection (within walking_time_limit secs)
// from start_intersection while waiting for the car to arrive.  While walking,
// you can ignore speed limits of streets and just account for given
// walking_speed [m/sec]. However, pedestrians should also wait for traffic
// lights, so turn_penalty applies to whether you are driving or walking.
// Walking in the opposite direction of one-way streets is fine. Driving is
// NOT!  The routine returns a pair of vectors of street segment ids. The first
// vector is the walking path starting at start_intersection and ending at the
// pick-up intersection. The second vector is the driving path from pick-up
// intersection to end_interserction.  Note that a start_intersection can be a
// pick-up intersection. If this happens, the first vector should be empty
// (size = 0).  If there is no driving path found, both returned vectors should
// be empty (size = 0). 
// If the end_intersection turns out to be within the walking time limit, 
// you can choose what to return, but you should not crash. If your user 
// interface can call this function for that case, the UI should handle
// whatever you return in that case.

std::pair<std::vector<StreetSegmentIndex>, std::vector<StreetSegmentIndex>> find_path_with_walk_to_pick_up(
        const IntersectionIndex start_intersection,
        const IntersectionIndex end_intersection,
        const double turn_penalty,
        const double walking_speed,
        const double walking_time_limit) {
    
    std::vector<StreetSegmentIndex> drive_path;
    std::vector<StreetSegmentIndex> walk_path;


    if (start_intersection == end_intersection) {
       return std::make_pair(walk_path, drive_path); //return empty
    }

    if(walking_time_limit == 0 || walking_speed == 0){
        drive_path = find_path_between_intersections(start_intersection, end_intersection, turn_penalty);
        return std::make_pair(walk_path, drive_path);
    }
    
    std::vector<int> qualified_intersections;
    for(int i = 0; i < getNumIntersections(); i++){
		if(find_distance_between_two_points(std::make_pair(getIntersectionPosition(start_intersection), getIntersectionPosition(i)))/walking_speed <= walking_time_limit){
			//found feasable intersections to walk to
			qualified_intersections.push_back(i);
		}
	}
	
    //std::cout << "qualified intersections by distance " << qualified_intersections.size() << std::endl;
	
	//now use astar time limited 
    std::vector<int> more_qualified;
    std::map<int, std::vector<int>> walkable_paths;//hasmap from end node that is walkable that is key to the segments that represent that path
    algorithm_a_star_walking(nodes.at(start_intersection), nodes.at(end_intersection), turn_penalty, walking_speed, walking_time_limit, walkable_paths, qualified_intersections);
    //std::cout << "walkable path size " <<walkable_paths.size() << std::endl;
    for (int i = 0; i < nodes.size(); ++i) {
        (nodes.at(i))->resetNode();
    }
    
    for(auto it = walkable_paths.begin(); it != walkable_paths.end(); it++){
        auto intersection_id = it->first; //should be an intersection id
        auto path = it->second;
        if(path.size() == 0){ //this is start node
            more_qualified.push_back(intersection_id);
        }
        
        double path_time = compute_path_walking_time(path, walking_speed, turn_penalty);
        if( path_time <= walking_time_limit){
            more_qualified.push_back(intersection_id);
            //std::cout << "intersection id " << intersection_id << std::endl;
        }
        else{
            //std::cout << "path time is > walking limit " << path_time << std::endl;
        }
        
    }
    //std::cout << "paths in hasmap less than wlk time limit " << more_qualified.size() << std::endl;
    if(more_qualified.size() != 0){
        algorithm_a_star(more_qualified, nodes.at(end_intersection), turn_penalty, drive_path); //overloaded 
    }
    else{
        algorithm_a_star(nodes.at(start_intersection), nodes.at(end_intersection), turn_penalty, drive_path); //all intersections to far to walk
        std::reverse(drive_path.begin(), drive_path.end());
       // std::cout << "returning  empty walk path" << std::endl;
        for (int i = 0; i < nodes.size(); ++i) {
            (nodes.at(i))->resetNode();
        }
       return  std::make_pair(walk_path, drive_path); //return empty walk path
    }
    for (int i = 0; i < nodes.size(); ++i) {
        (nodes.at(i))->resetNode();
    }
    //std::cout << "drive path size " << drive_path.size() << std::endl;
    if(drive_path.size() == 0){
        //idk why this would happen
        if(more_qualified.size()  > 0){
            int i = 0; //avoid the start node path
            walk_path = walkable_paths.find(more_qualified[i])->second;
            while(walk_path.size() == 0){
                i++;
                walk_path = walkable_paths.find(more_qualified[i])->second;
            }
            //std::cout << "here "<< walk_path.size() << std::endl;
            std::reverse(walk_path.begin(), walk_path.end());
            return std::make_pair(walk_path, drive_path);
        }
        return std::make_pair(drive_path, drive_path); //return both empty vectors
        
    }
    //need to determine which intersection from qualified was picked
    //determine last intersection
    int last_intersection = -1;
    if(drive_path.size() > 1){
        if(getInfoStreetSegment(drive_path.back()).from == getInfoStreetSegment(drive_path[drive_path.size() -2]).from) last_intersection = getInfoStreetSegment(drive_path.back()).to; 
        if(getInfoStreetSegment(drive_path.back()).from == getInfoStreetSegment(drive_path[drive_path.size() -2]).to) last_intersection = getInfoStreetSegment(drive_path.back()).to; 
        if(getInfoStreetSegment(drive_path.back()).to == getInfoStreetSegment(drive_path[drive_path.size() -2]).from) last_intersection = getInfoStreetSegment(drive_path.back()).from; 
        if(getInfoStreetSegment(drive_path.back()).to == getInfoStreetSegment(drive_path[drive_path.size() -2]).to) last_intersection = getInfoStreetSegment(drive_path.back()).from; 
    }
    else{
        //drive_path.size() ==1
        if(getInfoStreetSegment(drive_path.back()).from == end_intersection) last_intersection = getInfoStreetSegment(drive_path.back()).to;
        if(getInfoStreetSegment(drive_path.back()).to == end_intersection) last_intersection = getInfoStreetSegment(drive_path.back()).from;
    }
    int walk_int = *(std::find(more_qualified.begin(), more_qualified.end(), last_intersection));
   // std::cout << "intersection to walk to " << walk_int << std::endl;
    
    walk_path = walkable_paths.find(walk_int)->second;
    
    std::reverse(walk_path.begin(), walk_path.end());
    std::reverse(drive_path.begin(), drive_path.end());
    
    return std::make_pair(walk_path, drive_path);
}



