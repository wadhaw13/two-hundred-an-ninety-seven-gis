#ifndef NODE_H
#define NODE_H

#include "Data_Structures.h"

class Node {
public:

    bool visited = false;
    double globalGoal;
    double localGoal;
    LatLon position;
    std::vector<Node*> neighbours;
    Node* parent;
    IntersectionIndex id;
    StreetSegmentIndex cameFrom;

    Node(LatLon pos, int _id);

    Node(LatLon pos, Node* _parent, int _id);

    ~Node();

    // Set the value of the function in order to determine node priority
    void setGlobalGoal(LatLon pos);
    
    // Set the value of the function to determine node priority for walking
    void setGlobalGoal_walking();

    // Set the parent of the node
    void setParent(Node* & _parent);

    // Set the value of the total travel time from start node to current node
    void setLocalGoal(double travel_time);

    // Store the neighbours of the current node
    void storeNeighbours();
    void storeNeighbours_multiple_start();
    void storeNeighbours_walking();
    
    // Reset the node to initial values
    void resetNode();

    bool operator==(const Node* rhs);


};

extern std::vector<Node*> nodes;

#endif /* NODE_H */

