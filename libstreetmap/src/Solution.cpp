//#include "Solution.h"
//
//Timer stop_watch;
//
//Solution::Solution() {
//
//    //this->uid = new boost::random::uniform_int_distribution<int>(INT_MIN, INT_MAX);
//
//}
//
//Solution::~Solution() {
//
//    //delete this->uid;
//
//}
//
////Local Search Implementation
////In the preliminary solution check for any overlapping paths
////Add those paths to a list to keep track of them
////Apply the 2opt algorithm
////Then check if the algorithm provided a legal solution 
////If the travel time is smaller than what was previously computed, save the solution
////Continue until all the overlaps have been checked
//
////How do you select which paths to change: 
////Either pick two paths that you know for sure intersect, or
////Pick any two random paths
//
//std::vector <int> start_intersections;
//
//std::vector <CourierSubpath> Solution::two_opt(std::vector<DeliveryInfo>& deliveries,
//        const std::vector<int>& depots,
//        const float turn_penalty,
//        const float truck_capacity
//        ) {
//
//    m_cap = truck_capacity;
//
//    std::vector <SolutionGenerated> answer;
//
//    std::vector <CourierSubpath> solution;
//
//    algo_solution(
//            deliveryRandomizer(deliveries),
//            depots,
//            turn_penalty,
//            truck_capacity,
//            solution);
//
//    //We will apply the 2opt algorithm for a fixed number of times at first.       
//
//    int i = 0;
//
//    for (i = 0; i < solution.size(); i++) {
//
//        //pick random intersections to swap 
//        int index1 = -1, index2 = index1;
//
//        //ensure that index1 and index2 are distinct
//        while (index1 != index2) {
//
//            index1 = getRandom(0, solution.size());
//            index2 = getRandom(0, solution.size());
//
//        }
//
//        //find the greater index
//        int largerIndex = -1, smallerIndex = -1;
//
//        if (index1 > index2) {
//
//            largerIndex = index1;
//            smallerIndex = index2;
//
//        } else {
//
//            largerIndex = index2;
//            smallerIndex = index1;
//
//        }
//
//        //now reverse from [smallerIndex, largerIndex)
//        std::reverse(solution.begin() + smallerIndex, solution.begin() + largerIndex);
//
//        std::vector <std::vector<unsigned>> indeces;
//
//        for (i = 0; i < solution.size(); i++)
//            indeces.push_back(solution[i].pickUp_indices);
//
//        // if (checkLegality(indeces)) answer.push_back({solution, calculateMetric(solution)});
//
//    }
//
//    return answer[0].data;
//
//}
//
////std::vector <CourierSubpath> Solution::multiStart(const std::vector<DeliveryInfo>& deliveries,
////        const std::vector<int>& depots,
////        const float turn_penalty,
////        const float truck_capacity
////        ) {
//
////    //ALGORITHM 
////    //Get a solution with random input values.
////    //Compute metric
////    //Repeat a set number of times
////    //Return the solution with the smallest cost.
////
////    //reset the timer
////    stop_watch.reset();
////
////    //a vector to save the solutions generated
////    std::vector <SolutionGenerated> answer;
////
////    std::vector<DeliveryInfo> delivery = deliveries;
////
////    std::cout << "OG delivery order.\n";
////
////    for (int i = 0; i < 2; i++) std::cout << deliveries[i].pickUp << " ";
////
////    std::cout << " ...\n";
////
////    int i = 0;
////
////    std::vector <CourierSubpath> solution;
////
////    for (i = 0; i < deliveries.size(); i++) {
////
////        std::vector<DeliveryInfo> randomized_delivery;
////
////        //make sure the first element is not the same as the original delivery vector's first element
////        do {
////
////            randomized_delivery = deliveryRandomizer(delivery);
////
////            //make sure the starting intersection is different eachtime
////        } while (randomized_delivery[0].pickUp == deliveries[0].pickUp);
////
////        
////        
////        start_intersections.push_back(randomized_delivery[0].pickUp);
////        
////        std::cout << "New delivery order.\n";
////
////        for (int j = 0; j < 2; j++) std::cout << randomized_delivery[j].pickUp << " ";
////
////        std::cout << " ...\n";
////
////        //do some setup
////        store_deliveries(randomized_delivery, delivery_map_global); //store this for use later
////        pre_compute_paths(turn_penalty, randomized_delivery);
////
////
////        algo_solution(
////                randomized_delivery,
////                depots,
////                turn_penalty,
////                truck_capacity,
////                solution);
////
////        //calculate cost
////        double time = calculateMetric(solution);
////
////        std::cout << "The sol'n generated has the cost: " << time << ".\n";
////
////        Timer x;
////        x.reset();
////        
////        if (isLegal(randomized_delivery, depots, solution, truck_capacity)) {
////        
////            std::cout << "LegalityCheck took " << x.ellapsed() << "\n";
////            answer.push_back({solution, time});
////            std::cout << "This solution is legal and it has been added to the list of possible solutions.\n";
////            
////        }
////
////        if (stop_watch.ellapsed() > 40) {
////
////            std::cout << stop_watch.ellapsed() << ": Close to the time limit. Aborting further tests.\n";
////            break;
////        }
////
////        pre_computed.clear();
////        delivery_map_global.clear();
////
////    }
////
////    //find the solution with the least cost 
////    auto result = std::min_element(answer.begin(), answer.end(), comparator_soln);
////
////    if (result != answer.end()) {
////
////        std::cout << "The solution with the metric " << result->cost << " was chosen.\n";
////
////        return (result->data);
////
////    } else return answer[getRandom(0, answer.size())].data;
//
////}
//
//double Solution::findTravelTime(std::vector <StreetSegmentIndex> vec) {
//
//    double travelTime = 0;
//
//    for (int i = 0; i < vec.size(); i++)
//
//        travelTime += data.segment_travel_time[vec[i]];
//
//    return travelTime;
//
//}
//
//bool pick_up_at_intersection_capacity(const std::vector<DeliveryInfo>& deliveries,
//        const std::multimap<IntersectionIndex, size_t>& intersections_to_pick_up,
//        const std::vector<unsigned> & pickup_indices,
//        const IntersectionIndex curr_intersection,
//        std::vector<bool>& deliveries_picked_up,
//        float & current_total_weight) {
//
//    // check if start_intersection is a pickup order
//    if (pickup_indices.size() > 0) // this is a pickup intersection
//    {
//        if (intersections_to_pick_up.find(curr_intersection) ==
//                intersections_to_pick_up.end()) {
//            std::cerr << "Invalid pick-up intersection!" << std::endl;
//            return false;
//        }
//
//        for (unsigned idx = 0; idx < pickup_indices.size(); idx++) {
//            if (deliveries_picked_up[pickup_indices[idx]])
//                continue;
//
//            deliveries_picked_up[pickup_indices[idx]] = true;
//
//            current_total_weight += deliveries[pickup_indices[idx]].itemWeight;
//        }
//    }
//    return true;
//}
//
//bool drop_off_at_intersection_capacity(const std::vector<DeliveryInfo>& deliveries,
//        const std::multimap<IntersectionIndex, size_t>& intersections_to_drop_off,
//        const std::vector<bool>& deliveries_picked_up,
//        const IntersectionIndex curr_intersection,
//        std::vector<bool>& deliveries_dropped_off,
//        float & current_total_weight) {
//
//    auto drop_off_it = intersections_to_drop_off.equal_range(curr_intersection);
//
//    for (auto itr = drop_off_it.first; itr != drop_off_it.second; ++itr) {
//        unsigned delivery_index = itr->second;
//        if (deliveries_picked_up[delivery_index]) // cannot drop off before picking up
//        {
//            if (deliveries_dropped_off[delivery_index]) // Already dropped it off
//                continue;
//
//            deliveries_dropped_off[delivery_index] = true;
//            current_total_weight = current_total_weight - deliveries[delivery_index].itemWeight;
//        }
//    }
//
//    if (current_total_weight < -FLOAT_EPSILON) {
//        std::cerr << "Invalid drop-off. Total weight in truck is less than 0" << std::endl;
//        return false;
//    }
//    return true;
//}
//
//bool delivered_all_packages(const std::vector<DeliveryInfo>& deliveries,
//        const std::vector<bool>& deliveries_picked_up,
//        const std::vector<bool>& deliveries_dropped_off) {
//
//    //
//    //Check how many undelivered packages there are
//    //
//    size_t undelivered_packages = 0;
//    for (size_t delivery_idx = 0; delivery_idx < deliveries.size(); ++delivery_idx) {
//        if (!deliveries_dropped_off[delivery_idx]) {
//            ++undelivered_packages;
//        } else {
//            //If it was dropped-off it must have been picked-up
//            assert(deliveries_picked_up[delivery_idx]);
//        }
//    }
//
//    //
//    //Report to the user the missing packages
//    //
//    if (undelivered_packages > 0) {
//        std::cerr << "Invalid courier path: " << undelivered_packages << " packages were never delivered:\n";
//
//        for (size_t delivery_idx = 0; delivery_idx < deliveries.size(); ++delivery_idx) {
//            //Un-picked up
//            if (!deliveries_picked_up[delivery_idx]) {
//                std::cerr << "\tDelivery " << delivery_idx
//                        << " was never picked-up at intersection " << deliveries[delivery_idx].pickUp
//                        << "\n";
//            }
//
//            //Un-delivered up
//            if (!deliveries_dropped_off[delivery_idx]) {
//                std::cerr << "\tDelivery " << delivery_idx
//                        << " was never dropped-off at intersection " << deliveries[delivery_idx].dropOff
//                        << "\n";
//            }
//        }
//
//        //Some un-delivered
//        return false;
//    }
//
//    //All delivered
//    return true;
//}
//
//bool traverse_segment(const std::vector<StreetSegmentIndex>& path,
//        const unsigned path_idx,
//        const IntersectionIndex curr_intersection,
//        IntersectionIndex& next_intersection) {
//    assert(path_idx < path.size());
//
//    InfoStreetSegment seg_info = getInfoStreetSegment(path[path_idx]);
//
//    //
//    //Are we moving forward or back along the segment?
//    //
//    bool seg_traverse_forward;
//    if (seg_info.from == static_cast<int> (curr_intersection)) {
//        //We take care to check 'from' first. This ensures
//        //we get a reasonable traversal direction even in the 
//        //case of a self-looping one-way segment
//
//        //Moving forwards
//        seg_traverse_forward = true;
//
//    } else if (seg_info.to == static_cast<int> (curr_intersection)) {
//        //Moving backwards
//        seg_traverse_forward = false;
//
//    } else {
//        assert(seg_info.from != static_cast<int> (curr_intersection) && seg_info.to != static_cast<int> (curr_intersection));
//
//        //This segment isn't connected to the current intersection
//        std::cerr << "Invalid courier path: path disconnected"
//                << " (intersection " << curr_intersection
//                << " does not connect to street segment " << path[path_idx]
//                << " at path index " << path_idx << ")\n";
//        return false;
//    }
//
//    //
//    //Are we going the wrong way along the segment?
//    //
//    if (!seg_traverse_forward && seg_info.oneWay) {
//        std::cerr << "Invalid courier path: Attempting to traverse against one-way"
//                << " (from intersection " << curr_intersection
//                << " along street segment " << path[path_idx]
//                << " at path index " << path_idx << ")\n";
//        return false;
//    }
//
//    //
//    //Advance to the next intersection
//    //
//    next_intersection = (seg_traverse_forward) ? seg_info.to : seg_info.from;
//
//    return true;
//}
//
//bool Solution::isLegal(const std::vector<DeliveryInfo>& deliveries,
//        const std::vector<IntersectionIndex>&,
//        const std::vector<CourierSubpath>& path,
//        const float truck_capacity) {
//
//    //
//    //Ensuree we have a valid problem specification
//    //
//    //if (!valid_courier_problem(deliveries, depots)) {
//    //   return false;
//    //}   
//
//    if (path.empty()) {
//        //If it is a valid path it must contain at-least one segment
//        //(since we gaurentee there is at-least one depot and delivery)
//        std::cerr << "Invalid courier path: path was empty\n";
//
//        return false;
//
//    } else {
//        assert(!path.empty());
//
//        //
//        // Check that start_intersection of the first subpath is a depot
//        //
//        //        if (!is_depot(depots, path[0].start_intersection)) {
//        //            // Not a valid start intersection
//        //            std::cerr << "Invalid courier path: start_intersection of first CourierSubpath is not a depot" << std::endl;
//        //            return false;
//        //        }
//
//        //
//        //We should end at a depot
//        //
//        //        if (!is_depot(depots, path[path.size() - 1].end_intersection)) {
//        //            //Not at a valid end intersection
//        //            std::cerr << "Invalid courier path: end_intersection of last CourierSubpath is not a depot" << std::endl;
//        //            return false;
//        //        }
//
//        //        // Make sure that overall path of segments is not empty 
//        //        std::vector<StreetSegmentIndex> overall_path;
//        //        for (size_t sub_idx = 0; sub_idx < path.size(); sub_idx++)
//        //            overall_path.insert(overall_path.end(), path[sub_idx].subpath.begin(), path[sub_idx].subpath.end());
//        //
//        //        if (overall_path.empty()) {
//        //            std::cerr << "Invalid courier path: Overall courier route is empty!" << std::endl;
//        //            return false;
//        //        }
//
//        //We store whether each delivery has been picked-up or dropped-off
//        std::vector<bool> deliveries_picked_up(deliveries.size(), false);
//        std::vector<bool> deliveries_dropped_off(deliveries.size(), false);
//
//        //We also build fast-lookups from: intersection id to DeliveryInfo index for both
//        //pickUp and dropOff intersections
//        std::multimap<IntersectionIndex, size_t> intersections_to_pick_up; //Intersection_ID -> deliveries index
//        std::multimap<IntersectionIndex, size_t> intersections_to_drop_off; //Intersection_ID -> deliveries index
//
//        //Load the look-ups
//        for (size_t delivery_idx = 0; delivery_idx < deliveries.size(); ++delivery_idx) {
//            IntersectionIndex pick_up_intersection = deliveries[delivery_idx].pickUp;
//            IntersectionIndex drop_off_intersection = deliveries[delivery_idx].dropOff;
//
//            intersections_to_pick_up.insert(std::make_pair(pick_up_intersection, delivery_idx));
//            intersections_to_drop_off.insert(std::make_pair(drop_off_intersection, delivery_idx));
//        }
//
//        // 
//        // Validating CourierSubpath and capacity
//        //
//        float current_total_weight = 0.0;
//        for (size_t sub_idx = 0; sub_idx < path.size(); sub_idx++) {
//
//            CourierSubpath courier_subpath = path[sub_idx];
//            IntersectionIndex start_intersection = courier_subpath.start_intersection;
//            IntersectionIndex end_intersection = courier_subpath.end_intersection;
//            std::vector<unsigned> pickup_indices = courier_subpath.pickUp_indices;
//
//            auto print_subpath_error_message = [&](auto&& message_printer) {
//                std::cerr << "Invalid courier subpath at index " << sub_idx << " of " << (path.size() - 1) << ": ";
//                message_printer(std::cerr);
//                std::cerr << std::endl;
//            };
//
//            if (courier_subpath.subpath.empty() && courier_subpath.start_intersection != courier_subpath.end_intersection) {
//                print_subpath_error_message([&](auto&&s) {
//                    s << "subpath is empty, but start_intersection != end_intersection";
//                });
//                return false;
//            }
//
//            // Check if start_intersection = end_intersection of previous CourierSubpath
//            if (sub_idx > 0) {
//                if (start_intersection != path[sub_idx - 1].end_intersection) {
//                    print_subpath_error_message([&](auto&&s) {
//                        s << "start_intersection is not equal to end_intersection of previous subpath";
//                    });
//                    return false;
//                }
//                // Check that start intersection is at least a pickup intersection or drop-off intersection (already checked if first one is depot above)
//                if (intersections_to_pick_up.find(start_intersection) == intersections_to_pick_up.end() &&
//                        intersections_to_drop_off.find(start_intersection) == intersections_to_drop_off.end()) {
//                    print_subpath_error_message([&](auto&&s) {
//                        s << "start_intersection is not a pick-up or drop-off intersection!";
//                    });
//                    return false;
//                }
//            }
//
//            //            // check that start_intersection of each subpath is actually at the beginning of the subpath
//            //            if (!is_start_intersection_correct(courier_subpath)) {
//            //                print_subpath_error_message([&](auto&&s) {
//            //                    s << "start_intersection is not the start of its subpath";
//            //                });
//            //                return false;
//            //            }
//            //
//            //            // check that end_intersection of each subpath is actually at the end of the subpath
//            //            if (!is_end_intersection_correct(courier_subpath)) {
//            //                print_subpath_error_message([&](auto&&s) {
//            //                    s << "end_intersection is not the end of its subpath";
//            //                });
//            //                return false;
//            //            }
//
//            // Checking capacity
//            /*
//             * Must ensure that the total weight of all picked up packages at each subpath does not 
//             * exceed the truck capacity
//             */
//
//            // Process Packages
//            if (!pick_up_at_intersection_capacity(deliveries,
//                    intersections_to_pick_up,
//                    pickup_indices,
//                    start_intersection,
//                    deliveries_picked_up,
//                    current_total_weight))
//                return false;
//
//            if (!drop_off_at_intersection_capacity(deliveries,
//                    intersections_to_drop_off,
//                    deliveries_picked_up,
//                    start_intersection,
//                    deliveries_dropped_off,
//                    current_total_weight))
//                return false;
//
//            if (current_total_weight > truck_capacity + FLOAT_EPSILON) {
//                //std::cerr << "Invalid Courier Path. Total weight of picked up items exceeds truck_capacity!" << std::endl;
//                return false;
//            }
//
//            //We verify the subpath by walking along each segment and:
//            //  * Checking that the next step along the path is valid (see traverse_segment())
//            IntersectionIndex curr_intersection = start_intersection;
//            for (size_t subpath_idx = 0; subpath_idx < courier_subpath.subpath.size(); ++subpath_idx) {
//
//                IntersectionIndex next_intersection; //Set by traverse_segment
//
//                if (!traverse_segment(courier_subpath.subpath, subpath_idx, curr_intersection, next_intersection)) {
//                    return false;
//                }
//
//                //Advance
//                curr_intersection = next_intersection;
//            }
//
//            // must end at end_intersection
//            assert(curr_intersection == end_intersection);
//        }
//
//        //
//        //Check everything was delivered
//        //
//        if (!delivered_all_packages(deliveries, deliveries_picked_up, deliveries_dropped_off)) {
//            return false;
//        }
//
//        //        // current_total_weight should be zero
//        //        if (current_total_weight < -FLOAT_EPSILON ||
//        //                current_total_weight > FLOAT_EPSILON) {
//        //            std::cerr << "Invalid courier route: Truck is not empty at the end depot!" << std::endl;
//        //            return false;
//        //        }
//    }
//
//    //Everything validated
//    return true;
//}
//
//bool Solution::checkLegality(std::vector <CourierSubpath> solution, std::vector <DeliveryInfo>&) {
//
//    //extract the pickUp indeces
//    std::vector <std::vector<unsigned int>> pickUp;
//
//    for (int i = 0; i < solution.size(); i++) {
//
//        pickUp.push_back(solution[i].pickUp_indices);
//
//    }
//
//    std::vector <IntersectionIndex> already_picked = {0};
//
//    for (int i = 0; i < pickUp.size(); i++) {
//
//        for (int j = 0; j < pickUp[i].size(); i++) {
//
//
//            //if we do not find a courier in the list of items that is already picked up, we add it to the list of items we have picked.
//            if (std::find(already_picked.begin(), already_picked.end(), pickUp[i][j]) != already_picked.end()) {
//
//                already_picked.push_back(pickUp[i][j]);
//
//            } else return false;
//
//            //if this pick up contains more couriers than the capacity of the truck, then return false
//            //int sum = 0;
//
//            //for (int i = 0 ; i < )
//
//
//
//        }
//
//    }
//
//
//    return true;
//}
//
//void Solution::visualise(std::vector <CourierSubpath> soln) {
//
//
//    for (int i = 0; i < soln.size(); i++) {
//
//        //        draw_street(appication->get_renderer(), soln[i].subpath, ezgl::RED, ezgl::line_cap::round, ezgl::line_dash::none);
//
//    }
//
//    //  appication->flush_drawing();
//
//}
//
////returns a random number between [start, stop)
//int Solution::getRandom(int start, int stop) {
//
//    return rng() % (stop - start + 1) + start;
//
//}
//
//int myrandom(int i) {
//
//    std::srand(unsigned ( std::time(0)));
//
//    return std::rand() % i;
//
//}
//
//std::vector<DeliveryInfo> Solution::deliveryRandomizer(const std::vector<DeliveryInfo>& info_old) {
//
//    std::vector <DeliveryInfo> info = info_old;
//
//    //Lambda encapsulated RNG specifier
//    std::random_shuffle(info.begin(), info.end(), myrandom);
//
//    return info;
//
//}
//
//double Solution::calculateMetric(std::vector <CourierSubpath> solution) {
//
//    double Travel_time = 0;
//
//    //Solution benchmark
//    for (int i = 0; i < solution.size(); i++) {
//        Travel_time = 0;
//
//        Travel_time += findTravelTime(solution[i].subpath);
//    }
//
//    return Travel_time;
//
//}
//
//bool comparator_soln(SolutionGenerated& i, SolutionGenerated& j) {
//
//    return i.cost < j.cost;
//
//}
//
////std::find(start_intersections.begin(),start_intersections.end(), randomized_delivery[0].pickUp) != start_intersections.end()