#include "m2_headers.h"
#include "m3_headers.h"

void search_option_clicked(GtkWidget *widget, ezgl::application *application) {

    for (int i = 0; i < street_suggestions.size(); i++) {


        if (street_suggestions[i].first == widget) {

            display_intersection_information(street_suggestions[i].second, 0);

            auto spane = (application->get_object("directionsMode"));

            if (spane != nullptr) gtk_widget_show((GtkWidget*) spane);

            shown_directions_bar = true;

            spane = (application->get_object("TextInput"));

            gtk_entry_set_text((GtkEntry*) spane, gtk_button_get_label((GtkButton*) widget));

            spane = (application->get_object("img_btn"));

            if (spane != nullptr) gtk_widget_show((GtkWidget*) spane);

            spane = (application->get_object("suggestion_window"));

            if (show_suggestions_box) {

                gtk_widget_hide((GtkWidget*) spane);

                show_suggestions_box = false;

            }

            internal_1 = street_suggestions[i].second;

            mouse1 = -1;

            mouse2 = mouse1;

            application->update_message("");

        }

    }

}

void search_option_clicked2(GtkWidget *widget, ezgl::application *application) {

    for (int i = 0; i < street_suggestions.size(); i++) {


        if (street_suggestions[i].first == widget) {

            display_intersection_information(street_suggestions[i].second, 0);

            if (shown) {
                hide_directions();
                shown = false;
            }

            if (shown_directions_bar) {

                GObject* spane = (appication->get_object("directionsMode"));

                gtk_widget_hide((GtkWidget*) spane);

                shown_directions_bar = false;

            }

            delete_suggestions(2);

            auto spane = (application->get_object("suggestion_window2"));

            gtk_widget_hide((GtkWidget*) spane);

            show_suggestions_box2 = false;

            spane = appication->get_object("destBox");

            internal_2 = street_suggestions[i].second;

            //std::cout << internal_1 << " " << internal_2 << "\n";

            gtk_entry_set_text((GtkEntry*) spane, gtk_button_get_label((GtkButton*) widget));

            auto pane = (application->get_object("suggestion_window2"));

            gtk_widget_hide((GtkWidget*) pane);

            show_suggestions_box2 = false;

            spane = (appication->get_object("directionsMode"));

            gtk_widget_show((GtkWidget*) spane);

            shown_directions_bar = true;

        }

    }

}

void hide_dir_btn(GtkWidget *widget, ezgl::application *application) {

    if (shown) {
        hide_directions();
        shown = false;
    }

    if (shown_directions_bar) {

        GObject* spane = (appication->get_object("directionsMode"));

        gtk_widget_hide((GtkWidget*) spane);

        shown_directions_bar = false;

    }

    gtk_widget_hide((GtkWidget*) widget);

    auto spane = (application->get_object("suggestion_window2"));

    gtk_widget_hide((GtkWidget*) spane);

    show_suggestions_box2 = false;

}

//called when the search entry emits a "changed" signal

void update_suggestions(std::vector<int>& suggestions, int opt) {

    //if the index is already displayed don't add it.    

    GtkWidget* decepticon;

    if (opt == 1) decepticon = (GtkWidget*) appication->get_object("box_dir_buttons");

    else decepticon = (GtkWidget*) appication->get_object("box_dir_buttons2");
    ;

    delete_suggestions(opt);

    for (int i = 0; i < suggestions.size(); i++) {

        auto autobot = gtk_button_new_with_label(getIntersectionName(suggestions[i]).c_str());

        street_suggestions.push_back(std::make_pair((GtkWidget*) autobot, suggestions[i]));

        gtk_box_pack_start((GtkBox*) decepticon,
                autobot,
                0,
                0,
                3);

        if (opt == 1) g_signal_connect(autobot, "clicked", G_CALLBACK(search_option_clicked), appication);

        else g_signal_connect(autobot, "clicked", G_CALLBACK(search_option_clicked2), appication);

        gtk_widget_show(autobot);

    }



}

void text_input_changed(GtkWidget *, ezgl::application *application) {

    std::set <std::string> kwal;

    GtkEntry* text_input = (GtkEntry *) application->get_object("TextInput");

    const char* text = gtk_entry_get_text(text_input);

    std::string input = text;

    std::size_t pos = input.find("&");

    std::size_t nos = input.find("and");

    if (pos > input.size() && nos > input.size()) return; //"&" was not found

    intersection.clear();

    auto x = application->get_object("suggestion_window");

    gtk_widget_show((GtkWidget*) x);

    show_suggestions_box = true;

    std::string name1, name2;

    if (pos < input.size()) {

        name1 = input.substr(0, pos);

        name2 = input.substr(pos);

        name2.erase(0, 1);

    } else {

        name1 = input.substr(0, nos);

        name2 = input.substr(nos + 3);

        //name2.erase(0, 1);


    }

    boost::algorithm::trim(name1);

    boost::algorithm::trim(name2);

    //std::cout << name1 << " " << name2 << "\n";

    std::vector<int> street1 = find_street_ids_from_partial_street_name(name1);

    std::vector<int> street2 = find_street_ids_from_partial_street_name(name2);

    for (int i = 0; i < street1.size(); i++) {

        for (int j = 0; j < street2.size(); j++) {
            // intersection.insert(find_intersections_of_two_streets(std::make_pair(street1[i], street2[j])));
            std::vector<int> temp = find_intersections_of_two_streets(std::make_pair(street1[i], street2[j]));

            for (int l = 0; l < temp.size(); l++) {

                try{

                    auto k = kwal.insert(getIntersectionName(temp[l]));

                    if (k.second) {
                        intersection.push_back(temp[l]);
                    }

                }

                catch(...) {

                }

            }


        }
    }

    update_suggestions(intersection, 1);

}

void dest_input_changed(GtkWidget *, ezgl::application *application) {

    std::set <std::string> kwal;

    GtkEntry* text_input = (GtkEntry *) application->get_object("destBox");

    const char* text = gtk_entry_get_text(text_input);

    std::string input = text;

    std::size_t pos = input.find("&");

    std::size_t nos = input.find("and");

    if (pos > input.size() && nos > input.size()) return; //"&" was not found

    intersection.clear();

    auto x = application->get_object("suggestion_window2");

    gtk_widget_show((GtkWidget*) x);

    show_suggestions_box = true;

    std::string name1, name2;

    if (pos < input.size()) {

        name1 = input.substr(0, pos);

        name2 = input.substr(pos);

        name2.erase(0, 1);

    } else {

        name1 = input.substr(0, nos);

        name2 = input.substr(nos + 3);

        //name2.erase(0, 1);


    }

    boost::algorithm::trim(name1);

    boost::algorithm::trim(name2);

    //std::cout << name1 << " " << name2 << "\n";

    std::vector<int> street1 = find_street_ids_from_partial_street_name(name1);

    std::vector<int> street2 = find_street_ids_from_partial_street_name(name2);

    for (int i = 0; i < street1.size(); i++) {

        for (int j = 0; j < street2.size(); j++) {
            // intersection.insert(find_intersections_of_two_streets(std::make_pair(street1[i], street2[j])));
            std::vector<int> temp = find_intersections_of_two_streets(std::make_pair(street1[i], street2[j]));

            for (int l = 0; l < temp.size(); l++) {

                try{

                    auto k = kwal.insert(getIntersectionName(temp[l]));

                    if (k.second) {
                        intersection.push_back(temp[l]);
                    }

                }

                catch(...) {

                }

            }


        }
    }

    update_suggestions(intersection, 2);

}

void delete_suggestions(int) {

    //    GtkWidget* decepticon;
    //
    //    if (l == 1) decepticon = (GtkWidget*) appication->get_object("box_dir_buttons");
    //
    //    else decepticon = (GtkWidget*) appication->get_object("box_dir_buttons2");

    for (int i = 0; i < street_suggestions.size(); i++) {

        gtk_widget_destroy((GtkWidget*) street_suggestions[i].first);

    }

    street_suggestions.clear();

}

//called when the search entry emits a notify signal

void text_input_notify(GtkWidget *, ezgl::application *) {

    std::cout << "notified\n";

}

//called when the enter is hit on the search bar

void enter_hit(GtkWidget *widget, ezgl::application *application) {

    delete_suggestions(1);

    GtkEntry* text_input = (GtkEntry *) application->get_object("TextInput");

    const char* text = gtk_entry_get_text(text_input);

    std::string input = text;

    if (input == "directions") {

        if (shown == false) {

            show_directions();
            shown = true;

        } else if (shown == true) {

            hide_directions();
            shown = false;

        }


    } else if (input.find(" & ") > 0 || input.find("&") > 0) {

        test_button(widget, application);

        auto draw_area = (application->get_renderer())->get_visible_world();

        //centre of the screen xy (world))
        auto position_of_centre_of_view = std::make_pair(draw_area.center_x(), draw_area.center_y());

        //std::cout << position_of_centre_of_view.first << " " << position_of_centre_of_view.second << "\n";

        POI poi_centre;

        poi_centre.location = position_of_centre_of_view;

        pois_selected.push_back(poi_centre);

    }

    auto answer = service.InvokeRequest(input);

    if (answer.size() > 0) {

        LatLon new_pos(answer[0].Lat, answer[0].Lon);

        general_pan(new_pos);

        show_popup(answer[0].name + " " + answer[0].addr);

    } else {

        appication->update_message("No results found!");

    }

}

//clear list of directions from the directions box

void clear_directions_list() {

    //    auto dirBox = appication->get_object("dirBox");

    for (auto it = TEXTBOX_REFERENCES.begin(); it != TEXTBOX_REFERENCES.end(); it++) {

        //std::cout << *it << "\n";

        GtkWidget* widget = static_cast<GtkWidget*> (*it);

        gtk_widget_hide(widget);

        gtk_widget_destroy(widget);

    }

    TEXTBOX_REFERENCES.clear();

}

//display the directions box

void show_directions() {

    if (TEXTBOX_REFERENCES.empty()) return;

    GObject * spane = (appication->get_object("FixedDirections"));

    gtk_widget_show((GtkWidget*) spane);

    spane = (appication->get_object("img_btn"));

    gtk_widget_show((GtkWidget*) spane);

}

//hide the directions box

void hide_directions() {

    GObject* spane = (appication->get_object("FixedDirections"));

    gtk_widget_hide((GtkWidget*) spane);

    spane = (appication->get_object("img_btn"));

    gtk_widget_hide((GtkWidget*) spane);


}

//load a new vector of strings.

void load_directions(std::vector <std::string>& directions) {

    clear_directions_list();

    auto dirBox = appication->get_object("dirBox");

    GtkSeparator* sep = (GtkSeparator*) gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);

    gtk_box_pack_start((GtkBox*) dirBox, (GtkWidget*) (sep), FALSE, FALSE, 1);

    gtk_widget_show((GtkWidget*) sep);

    gtk_widget_destroy(view);

    TEXTBOX_REFERENCES.push_back((GtkWidget*) sep);

    for (auto it = directions.begin(); it != directions.end(); it++) {

        view = gtk_text_view_new();

        buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));

        gtk_text_buffer_set_text(buffer, (*it).c_str(), -1);

        if (*it == "Driving Directions.") {

            GtkTextIter start, end;

            gtk_text_buffer_get_start_iter(buffer, &start);

            gtk_text_buffer_get_end_iter(buffer, &end);

            auto tag = gtk_text_buffer_create_tag(buffer, "red_foreground",
                    "foreground", "red", NULL);

            gtk_text_buffer_apply_tag(buffer, tag, &start, &end);


        } else if (*it == "Walking Directions.") {


            GtkTextIter start, end;

            gtk_text_buffer_get_start_iter(buffer, &start);

            gtk_text_buffer_get_end_iter(buffer, &end);

            auto tag = gtk_text_buffer_create_tag(buffer, "green_foreground",
                    "foreground", "green", NULL);

            gtk_text_buffer_apply_tag(buffer, tag, &start, &end);


        }


        GtkWidget * p = static_cast<GtkWidget*> (gtk_text_view_new_with_buffer(buffer));

        //        PANGO_STYLE_NORMAL;

        gtk_text_view_set_wrap_mode((GtkTextView*) p, GtkWrapMode::GTK_WRAP_WORD);

        TEXTBOX_REFERENCES.push_back(p);

        gtk_box_pack_start((GtkBox*) dirBox, p, FALSE, FALSE, 1);

        GtkSeparator* sep_2 = (GtkSeparator*) gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);

        gtk_box_pack_start((GtkBox*) dirBox, (GtkWidget*) (sep), FALSE, TRUE, 1);

        gtk_widget_show(p);

        TEXTBOX_REFERENCES.push_back((GtkWidget*) sep_2);

        gtk_widget_show((GtkWidget*) sep_2);

        gtk_widget_destroy(view);

    }

}

void phar(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("pharmacy");

}

void casi(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("casino");

}

void club(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("club");

}

void cine(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("cinema");

}

void bar(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("bar");

}

void atm(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("atm");

}

void bench(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("bench");

}

void univ(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("university");

}

void rest(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("restaurant");

}

void dent(GtkWidget *, ezgl::application *) {

    find_oints_of_interest("dentist");

}

void show_popup(std::string data_2) {

    int sizeX = 400, sizeY = 45;

    auto canvas = appication->get_canvas("MainCanvas");

    double posX = (canvas->width() - sizeX) / 2;

    double posY = (canvas->height() - sizeY - 10);

    Popup pop(appication->get_renderer(), posX, posY, sizeX, sizeY);

    pop.set_string(data_2);

    pop.render();

    appication->flush_drawing();

}

void hide_sidebar(GtkWidget *, ezgl::application * application) {

    if (shown_sidebar) {

        auto spane = GTK_PANED(application->get_object("main_pane"));

        gtk_paned_set_position(GTK_PANED(spane), 1200);

        shown_sidebar = false;

    } else {

        auto spane = GTK_PANED(application->get_object("main_pane"));

        gtk_paned_set_position(GTK_PANED(spane), 1040);

        shown_sidebar = true;

    }

}

void poi_button(GtkWidget */*widget*/, ezgl::application * application) {


    if (gtk_toggle_button_get_active((GtkToggleButton*) application->get_object("DisplayPOI"))) {

        auto panes = (application->get_object("LayerBar"));

        gtk_widget_show((GtkWidget*) panes);

    } else {

        auto panes = GTK_FIXED(application->get_object("LayerBar"));

        gtk_widget_hide((GtkWidget*) panes);

    }

}

void draw_main_canvas(ezgl::renderer * g);

static ezgl::rectangle initial_world{
    {0, 0}, 100, 100
};

std::pair<LatLon, LatLon> set_initial_world() {

    int total_intersections = getNumIntersections();

    LatLon abs_min = LatLon(INT_MAX, INT_MAX);

    LatLon abs_max = LatLon(INT_MIN, INT_MIN);
    //i holds the current intersection index
    for (int i = 0; i < total_intersections; ++i) {

        LatLon pos = getIntersectionPosition(i);

        if (pos.lat() < abs_min.lat() && pos.lon() < abs_min.lon()) abs_min = pos;

        if (pos.lat() > abs_max.lat() && pos.lon() > abs_max.lon()) abs_max = pos;
    }

    LAT_TOP = abs_max.lat();

    LAT_BOT = abs_min.lat();

    return std::make_pair(abs_min, abs_max);
}

bool comp(const std::pair<double, int> &a, const std::pair<double, int> &b) { //sort from largest to smallest

    return a.first > b.first;
}

bool comp2(const std::pair<double, int> &a, const std::pair<double, int> &b) { //sort from largest to smallest

    return a.first > b.first;
}

bool comp3(const std::vector<double> &a, const std::vector<double> &b) { //sort from largest to smallest

    return a[0] > b[0];
}

double get_pos_weight(int feature_id) {
    //since small feature the first node xy position and last node xy position will be used as weights

    LatLon ll = getFeaturePoint(0, feature_id);
    std::pair <double, double> xy_1 = xy_from_point(ll);

    return xy_1.first;

}

void store_big_features_by_area_small_by_pos() {

    int n = getNumFeatures();
    for (int i = 0; i < n; i++) {
        double area = find_feature_area(i);
        double pos = get_pos_weight(i);
        if (area > 1500)
            data.big_features_area_id.push_back(std::make_pair(area, i));

        else
            data.small_features_pos_id.push_back(std::make_pair(pos, i));
    }
    auto& a = data.big_features_area_id;
    auto& b = data.small_features_pos_id;
    std::sort(a.begin(), a.end(), comp);
    std::sort(b.begin(), b.end(), comp2);

}

void store_streets_by_x_y() {
    for (int segIdx = 0; segIdx < getNumStreetSegments(); ++segIdx) {
        OSMID way_id = getInfoStreetSegment(segIdx).wayOSMID;
        auto search_way = data.waypointer_from_wayid.find(way_id);
        const OSMWay* way = search_way->second;
        //get first node->node coords
        std::vector<OSMID> nodes = getWayMembers(way);
        LatLon nodes_i;
        LatLon nodes_j;
        //get node pointer from node id
        const OSMNode* nodei;
        const OSMNode* nodej;
        auto node1 = data.nodepointer_from_nodeid.find(nodes[0]);
        auto node2 = data.nodepointer_from_nodeid.find(nodes[nodes.size() - 1]);
        if (node1 == data.nodepointer_from_nodeid.end()) return;

        int draw_weight = 0;
        for (int i = 0; i < getTagCount(way); ++i) {
            std::string key, value;
            std::tie(key, value) = getTagPair(way, i);
            if (key == "highway") {
                draw_weight = find_weight(value);
            }
        }
        if (draw_weight > 3) { //this is big road //can hold more information in this vector too
            data.segements_big.push_back(segIdx);
        } else {

            nodei = node1->second;
            nodej = node2->second;
            nodes_i = getNodeCoords(nodei); //avg of first and last points in the way are saved and checked on draw
            nodes_j = getNodeCoords(nodej);
            std::pair <double, double> xy_1 = xy_from_point(nodes_i);
            std::pair <double, double> xy_2 = xy_from_point(nodes_j);

            std::vector<double> inner = {(xy_1.first + xy_2.first) / 2, (xy_1.second + xy_2.second) / 2, (double) segIdx};
            data.segments_x_y_id.push_back(inner);
        }
    }
    std::sort(data.segments_x_y_id.begin(), data.segments_x_y_id.end(), comp3);
}

int calculate_initial_world() {

    auto min_max = set_initial_world();
    auto min_xy = xy_from_point(min_max.first);
    auto max_xy = xy_from_point(min_max.second);
    initial_world.m_first = ezgl::point2d(min_xy.first * EARTH_RADIUS_METERS, min_xy.second * EARTH_RADIUS_METERS);
    initial_world.m_second = ezgl::point2d(max_xy.first *EARTH_RADIUS_METERS, max_xy.second * EARTH_RADIUS_METERS);
    init_area = initial_world.area();
    store_big_features_by_area_small_by_pos(); //need to call here because need LAT_TOP and bOTTOM to be defined before I figure out positions
    store_streets_by_x_y();

    return 0;
}

int reset_world_camera(ezgl::application * app) {

    app->get_canvas("MainCanvas")->get_camera().reset_world(initial_world);
    return 0;
}

void draw_map() {

    ezgl::application::settings settings;

    calculate_initial_world();

    settings.main_ui_resource = ("libstreetmap/resources/pain.ui");
    // Note: the "main.ui" file has a GtkWindow called         "MainWindow".
    settings.window_identifier = "MainWindow";

    // Note: the "main.ui" file has a GtkDrawingArea called "MainCanvas".
    settings.canvas_identifier = "MainCanvas";

    // Create our EZGL application.
    ezgl::application application(settings);

    application.add_canvas("MainCanvas", draw_main_canvas, initial_world);

    if (load_images() != 0) std::cout << "Error loading images. That's all we know.\n";

    appication = &application;

    Parser p;

    p.ParsePath("libstreetmap/resources/safety_data.json");

    data.safety_vector = p.data();

    application.run(initial_setup, act_on_mouse_press, act_on_mouse_move, act_on_key_press);

    free_images();
}

void draw_main_canvas(ezgl::renderer * g) {

    g->set_color(217, 215, 206, 100);

    ezgl::rectangle draw_area = g->get_visible_world();

    g->fill_rectangle(draw_area);

    feature_draw(g);

    streets_draw(g);

    draw_collisions(g);

    draw_pois(g);

    display_path(g);

}

double determine_poi_hit_radius(double zoom) {
    if (zoom < X_4) {
        return 1;
    } else if (zoom < X_3) {
        return 1;
    } else if (zoom < X_2) {
        return 2.5;
    } else if (zoom < X_1) {
        return 4;
    }
    else if (zoom > X_1) {

        return 5;
    }
    return 0;
}

void act_on_mouse_press(ezgl::application *application, GdkEventButton *, double x, double y) {


    ezgl::renderer* g = application->get_renderer();

    double zoom_level = zoom_ratio(g);

    double radius = determine_poi_hit_radius(zoom_level);

    for (auto it = pois_selected.begin(); it != pois_selected.end(); it++) {

        LatLon ll = point_from_xy(x, y);

        double dist = find_distance_between_two_points(std::make_pair(ll, it->position));

        if (dist < 50 * radius) {

            application->flush_drawing();
            //std::cout << getPointOfInterestName(it->idx) << " hit =============================\n";

            show_popup(getPointOfInterestName(it->idx));

        }
    }
    LatLon xx = point_from_xy(x, y);

    highlight_selected(application->get_renderer(), xx, 10, x, y);



}

void act_on_mouse_move(ezgl::application *, GdkEventButton *, double, double) {



}

void act_on_key_press(ezgl::application *, GdkEventKey *, char *) {


}

std::string buttons[] = {"r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "r16", "r17", "r18", "r19"};

void get_directions_event(GtkWidget *, ezgl::application * application) {

    auto spane = (GtkWidget *) application->get_object("spinner");

    gtk_spinner_start((GtkSpinner*) spane);

    appication->update_message("Please be patient while we calculate your route.");

    if (mouse1 == -1 && mouse2 == -1)

        find_directions(internal_1, internal_2);

    else if (internal_1 == -1 && internal_2 == -1)

        find_directions(mouse1, mouse2);

    if (shown) {
        hide_directions();
        shown = false;
    }

    if (shown_directions_bar) {

        GObject* spane_2 = (appication->get_object("directionsMode"));

        gtk_widget_hide((GtkWidget*) spane_2);

        shown_directions_bar = false;

    }

    show_directions();
    shown = true;

}

void get_walk_dir_event(GtkWidget *, ezgl::application * application) {

    auto spane = (GtkWidget *) application->get_object("spinner");

    gtk_spinner_start((GtkSpinner*) spane);

    appication->update_message("Please be patient while we calculate your route.");

    spane = (GtkWidget *) application->get_object("dialogue");

    gtk_widget_show((GtkWidget*) spane);

    //std::cout << "The dialog has returned and so have I.\n";
}

void ack_btn(GtkWidget *, ezgl::application * application) {

    application->update_message("");

    auto spane = (GtkWidget *) application->get_object("dialogue");

    gtk_widget_hide((GtkWidget*) spane);

    double speed = 0, limit = 0;

    try{

        speed = std::stod(gtk_entry_get_text((GtkEntry*) application->get_object("walkSpeed")));

        limit = std::stod(gtk_entry_get_text((GtkEntry *) application->get_object("walkLimit")));

    }

    catch(std::exception e) {

        application->update_message("Invalid input received. Try again.");

        return;

    }

    std::cout << "M1 : " << mouse1 << "M2 : " << mouse2 << "\n";

    if (mouse1 == -1 && mouse2 == -1) {

        std::cout << "Finding internal directions.\n";

        find_walking_directions(internal_1, internal_2, speed, limit);

    } else if (internal_1 == -1 && internal_2 == -1) {

        std::cout << "Finding mouse directions.\n";

        find_walking_directions(mouse1, mouse2, speed, limit);

    }


    if (shown) {

        hide_directions();

        shown = false;
    }

    if (shown_directions_bar) {

        GObject* spane_2 = (appication->get_object("directionsMode"));

        gtk_widget_hide((GtkWidget*) spane_2);

        shown_directions_bar = false;

    }

    show_directions();

    shown = true;

}

void focus_event(GtkWidget* widget, ezgl::application *) {

    auto text = std::string(gtk_entry_get_text((GtkEntry*) widget));

    if (text == "Enter Walking Speed" || text == "Enter Walking Limit")

        gtk_entry_set_text((GtkEntry*) widget, "");


}

GtkWidget* non = nullptr;

void show_help_menu(GtkWidget *, ezgl::application * application) {

    //std::cout << "SHOW\n";

    GtkWidget* x = (GtkWidget*) application->get_object("helpMenu");

    non = x;

    gtk_widget_show((GtkWidget*) x);

}

void close_help_menu(GtkWidget *, ezgl::application *) {

    gtk_widget_hide(non);

}

void change_map(GtkWidget *, ezgl::application * application) {


    int i = 0;

    std::string n = "def";

    for (i = 0; i < TOTAL_BTNS; i++) {


        auto p = GTK_TOGGLE_BUTTON(application->get_object(buttons[i].c_str()));

        if (gtk_toggle_button_get_active(p)) {

            std::string city_name = gtk_button_get_label(GTK_BUTTON(p));

            // std::cout << "Active is " << buttons[i] << " " << gtk_button_get_label(GTK_BUTTON(p)) << "\n";

            if (city_name == "New Delhi") n = std::string("/cad2/ece297s/public/maps/") + "new-delhi_india" + ".streets.bin";

            else if (city_name == "Toronto") n = std::string("/cad2/ece297s/public/maps/") + "toronto_canada" + ".streets.bin";

            else if (city_name == "Cairo") n = std::string("/cad2/ece297s/public/maps/") + "cairo_egypt" + ".streets.bin";

            else if (city_name == "Beijing") n = std::string("/cad2/ece297s/public/maps/") + "beijing_china" + ".streets.bin";

            else if (city_name == "Cape Town") n = std::string("/cad2/ece297s/public/maps/") + "cape-town_south-africa" + ".streets.bin";

            else if (city_name == "Golden Horsehose") n = std::string("/cad2/ece297s/public/maps/") + "golden-horseshoe_canada" + ".streets.bin";

            else if (city_name == "Hamilton, Canada") n = std::string("/cad2/ece297s/public/maps/") + "hamilton_canada" + ".streets.bin";

            else if (city_name == "Hong Kong") n = std::string("/cad2/ece297s/public/maps/") + "hong-kong_china" + ".streets.bin";

            else if (city_name == "Iceland") n = std::string("/cad2/ece297s/public/maps/") + "iceland" + ".streets.bin";

            else if (city_name == "Interlaken, CH") n = std::string("/cad2/ece297s/public/maps/") + "interlaken_switzerland" + ".streets.bin";

            else if (city_name == "London, England") n = std::string("/cad2/ece297s/public/maps/") + "london_england" + ".streets.bin";

            else if (city_name == "Tehran") n = std::string("/cad2/ece297s/public/maps/") + "tehran_iran" + ".streets.bin";

            else if (city_name == "Singapore") n = std::string("/cad2/ece297s/public/maps/") + "singapore" + ".streets.bin";

            else if (city_name == "Sydney, Australia") n = std::string("/cad2/ece297s/public/maps/") + "sydney_australia" + ".streets.bin";

            else if (city_name == "Tokyo") n = std::string("/cad2/ece297s/public/maps/") + "tokyo_japan" + ".streets.bin";

            else if (city_name == "Saint Helena") n = std::string("/cad2/ece297s/public/maps/") + "saint-helena" + ".streets.bin";

            else if (city_name == "Rio-de-Janeiro") n = std::string("/cad2/ece297s/public/maps/") + "rio-de-janeiro_brazil" + ".streets.bin";

            else if (city_name == "New York") n = std::string("/cad2/ece297s/public/maps/") + "new-york_usa" + ".streets.bin";

            else if (city_name == "Moscow") n = std::string("/cad2/ece297s/public/maps/") + "moscow_russia" + ".streets.bin";

            break;

        }

    }

    if (n == "def") {

        //std::cout << "Please enter a proper name.";

        return;

    }

    //std::cout << n << "\n";

    close_map();

    load_map(n);

    calculate_initial_world();

    reset_world_camera(application);

    application->refresh_drawing();


}

void initial_setup(ezgl::application *application, bool) {
    application->create_button("Find Intersections", 7, test_button);

    application->create_button("Find POIs", 8, find_points_of_interest);

    GObject *load_btn = application->get_object("load_map");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(change_map), application);

    load_btn = application->get_object("helpMenu");

    g_signal_connect(load_btn, "response", G_CALLBACK(close_help_menu), application);

    //g_signal_connect(load_btn, "notify", G_CALLBACK(close_help_menu), application);

    //g_signal_connect(load_btn, "activate-link", G_CALLBACK(close_help_menu), application);

    //g_signal_connect(load_btn, "button-press-event", G_CALLBACK(close_help_menu), application);

    load_btn = application->get_object("getDirectionsButton");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(get_directions_event), application);

    load_btn = application->get_object("TextInput");

    g_signal_connect(load_btn, "activate", G_CALLBACK(enter_hit), application);

    load_btn = application->get_object("dent");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(dent), application);

    load_btn = application->get_object("bench");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(bench), application);

    load_btn = application->get_object("atm");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(atm), application);

    load_btn = application->get_object("bar");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(bar), application);

    load_btn = application->get_object("rest");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(rest), application);

    load_btn = application->get_object("cine");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(cine), application);

    load_btn = application->get_object("club");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(club), application);

    load_btn = application->get_object("casi");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(casi), application);

    load_btn = application->get_object("phar");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(phar), application);

    load_btn = application->get_object("univ");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(univ), application);

    load_btn = application->get_object("layer1");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(draw_layer_1), application);

    load_btn = application->get_object("layer2");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(draw_layer_2), application);

    load_btn = application->get_object("layer3");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(draw_layer_3), application);

    load_btn = application->get_object("clear");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(clear_btn), application);

    load_btn = application->get_object("LayerButton");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(hide_sidebar), application);

    load_btn = application->get_object("DisplayPOI");

    g_signal_connect(load_btn, "toggled", G_CALLBACK(poi_button), application);

    load_btn = application->get_object("img_btn");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(hide_dir_btn), application);

    load_btn = application->get_object("TextInput");

    input_changed_id = g_signal_connect(load_btn, "changed", G_CALLBACK(text_input_changed), application);

    load_btn = application->get_object("destBox");

    input_changed_id2 = g_signal_connect(load_btn, "changed", G_CALLBACK(dest_input_changed), application);

    load_btn = application->get_object("getWalkingDirection");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(get_walk_dir_event), application);

    //load_btn = application->get_object("walkSpeed");

    //g_signal_connect(load_btn, "button-press-event", G_CALLBACK(focus_event), application);

    load_btn = application->get_object("walkOK");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(ack_btn), application);

    load_btn = application->get_object("walkCancel");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(ack_btn), application);

    load_btn = application->get_object("Find");

    g_signal_connect(load_btn, "clicked", G_CALLBACK(show_help_menu), application);

    //load_btn = application->get_object("walkLimit");

    //g_signal_connect(load_btn, "button-press-event", G_CALLBACK(focus_event), application);


}

void clear_btn(GtkWidget *, ezgl::application * app) {

    pois_selected.clear();

    clear_route_info();

    draw_main_canvas(app->get_renderer());

    //    auto spane = GTK_FIXED(appication->get_object("directionsMode"));

    if (shown) {

        hide_directions();

        shown = false;

    }

    //gtk_widget_hide((GtkWidget*) spane);

    appication->refresh_drawing();

    mouse1 = -1;

    mouse2 = -1;

    appication->update_message("");

}

void test_button(GtkWidget *, ezgl::application * application) {


    GtkEntry* text_input = (GtkEntry *) application->get_object("TextInput");

    const char* text = gtk_entry_get_text(text_input);

    std::string input = text;

    std::size_t pos = input.find("&");

    if (pos > input.size()) return; //"&" was not found

    std::string name1 = input.substr(0, pos);

    std::string name2 = input.substr(pos);

    name2.erase(0, 1);

    boost::algorithm::trim(name1);

    boost::algorithm::trim(name2);

    std::vector<int> street1 = find_street_ids_from_partial_street_name(name1);

    std::vector<int> street2 = find_street_ids_from_partial_street_name(name2);

    std::vector<int> intersection_2;

    for (int i = 0; i < street1.size(); i++) {
        for (int j = 0; j < street2.size(); j++) {
            // intersection_2.insert(find_intersections_of_two_streets(std::make_pair(street1[i], street2[j])));
            std::vector<int> temp = find_intersections_of_two_streets(std::make_pair(street1[i], street2[j]));
            intersection_2.insert(intersection_2.end(), temp.begin(), temp.end());
        }
    }

    if (intersection_2.size() == 1) {


        display_intersection_information(intersection_2[0], 0);

        if (shown) {

            hide_directions();

            shown = false;

        }

        auto spane = (application->get_object("directionsMode"));

        if (spane != nullptr) gtk_widget_show((GtkWidget*) spane);

        shown_directions_bar = true;

        spane = (application->get_object("img_btn"));

        if (spane != nullptr) gtk_widget_show((GtkWidget*) spane);

    } else if (intersection_2.size() > 1) {

        display_intersection_information(intersection_2[0], 1);

    } else {

        show_popup("Not Found");
    }
}

void animate_button(GtkWidget *, ezgl::application *) {
}

void find_oints_of_interest(std::string data1) {

    std::for_each(data1.begin(), data1.end(), [](char & c) {
        c = ::tolower(c);
    });

    if (add_points_of_interest(std::string(data1)) != 0) {

        show_popup("Point of interest not found. That's all we know.");

    }

    draw_main_canvas(appication->get_renderer());

    appication->refresh_drawing();

}

void find_points_of_interest(GtkWidget *, ezgl::application * application) {

    GtkEntry* text_input = (GtkEntry *) application->get_object("TextInput");

    const char* text = gtk_entry_get_text(text_input);

    std::string data1 = text;

    //std::cout << data1 << " " << text << "\n";

    std::for_each(data1.begin(), data1.end(), [](char & c) {
        c = ::tolower(c);
    });

    if (add_points_of_interest(std::string(data1)) != 0) {

        show_popup("Problem finding points of interest. Check spellings.\n");

    }

    draw_main_canvas(application->get_renderer());

    application->refresh_drawing();


}

void draw_collisions(ezgl::renderer * g) {

    for (auto it = data.pep_collisions.begin(); it != data.pep_collisions.end(); it++) {
        std::pair<double, double> xy_1 = xy_from_point(*it);

        ezgl::point2d pt(xy_1.first * EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS);
        //if(it)
        g->draw_surface(pedestrian_surf, pt);

    }

    for (auto it = data.car_collisions.begin(); it != data.car_collisions.end(); it++) {
        std::pair<double, double> xy_1 = xy_from_point(*it);

        ezgl::point2d pt(xy_1.first * EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS);
        //if(it)
        g->draw_surface(auto_surf, pt);

    }

    for (auto it = data.cyc_collisions.begin(); it != data.cyc_collisions.end(); it++) {

        std::pair<double, double> xy_1 = xy_from_point(*it);

        ezgl::point2d pt(xy_1.first * EARTH_RADIUS_METERS, xy_1.second * EARTH_RADIUS_METERS);
        //if(it)
        g->draw_surface(cycl_surf, pt);

    }

    appication->flush_drawing();
}

int load_images() {

    try{

        //small star
        png_surface = ezgl::renderer::load_png("libstreetmap/resources/small_image.png");
        //marker
        marker_surf = ezgl::renderer::load_png("libstreetmap/resources/marker.png");
        //small star
        pedestrian_surf = ezgl::renderer::load_png("libstreetmap/resources/pedestrian.png");
        //marker
        auto_surf = ezgl::renderer::load_png("libstreetmap/resources/auto.png");
        //small star
        cycl_surf = ezgl::renderer::load_png("libstreetmap/resources/cycl.png");


    }

    catch(std::exception e) {

        //std::cout << e.what() << "\n";

        return -1;

    }

    return 0;

}

void free_images() {

    ezgl::renderer::free_surface(png_surface);

    ezgl::renderer::free_surface(marker_surf);

    ezgl::renderer::free_surface(pedestrian_surf);

    ezgl::renderer::free_surface(auto_surf);

    ezgl::renderer::free_surface(cycl_surf);

}

void draw_pois(ezgl::renderer * g) {

    for (auto it = pois_selected.begin(); it != pois_selected.end(); it++) {

        ezgl::point2d pt(it->location.first * EARTH_RADIUS_METERS, it->location.second * EARTH_RADIUS_METERS);

        g->draw_surface(marker_surf, pt);

    }

}

void draw_layer_1(GtkWidget *, ezgl::application *) {
    pep_click = !pep_click;

    //std::cout << "CLICKED\n";

    auto& pep = data.safety_vector;
    if (pep_click) {
        for (auto it = pep.begin(); it != pep.end(); it++) {
            if (it->pedestrian == "Yes") {
                data.pep_collisions.push_back(it->position);
            }
        }
    } else {

        data.pep_collisions.clear();
    }
}

void draw_layer_2(GtkWidget *, ezgl::application * app) {
    car_click = !car_click;
    auto& car = data.safety_vector;
    if (car_click) {
        for (auto it = car.begin(); it != car.end(); it++) {
            if (it->automotive == "Yes") {
                data.car_collisions.push_back(it->position);
            }
        }
    } else {

        data.car_collisions.clear();
    }
    app->flush_drawing();
}

void draw_layer_3(GtkWidget *, ezgl::application * app) {
    cyc_click = !cyc_click;
    auto& cyc = data.safety_vector;
    if (cyc_click) {
        for (auto it = cyc.begin(); it != cyc.end(); it++) {
            if (it->cyclist == "Yes") {
                data.cyc_collisions.push_back(it->position);
            }
        }
    } else {

        data.cyc_collisions.clear();
    }

    app->flush_drawing();
}

int display_intersection_information(IntersectionIndex id, int op) {

    //  std::cout << "The closest intersection is " << getIntersectionName(id) << "\n";    
    if (op == 0) {

        pan_to_intersection(id, appication);

        show_popup(getIntersectionName(id) + "\n");

    } else {

        show_popup(getIntersectionName(id) + " Too many hits");

    }

    return 0;

}

int general_pan(LatLon location) {

    //position of the intersection in xy (world))
    auto position_of_intersection = xy_from_point(location);

    //std::cout << position_of_intersection.first * EARTH_RADIUS_METERS << " pos inter " << position_of_intersection.second  * EARTH_RADIUS_METERS << "\n";

    auto draw_area = (appication->get_renderer())->get_visible_world();

    //cetnre of the screen xy (world))
    auto position_of_centre_of_view = std::make_pair(draw_area.center_x(), draw_area.center_y());

    //std::cout << position_of_centre_of_view.first << "  pos centre "  << position_of_centre_of_view.second << "\n";   

    //draw a marker at our departure
    POI poi_destination;

    poi_destination.location = position_of_intersection;

    pois_selected.push_back(poi_destination);

    //don't forget to multiply by the Earth's radius
    auto dx = (position_of_intersection.first * EARTH_RADIUS_METERS) - position_of_centre_of_view.first;

    auto dy = (position_of_intersection.second * EARTH_RADIUS_METERS) - position_of_centre_of_view.second;

    //std::cout << dx << " d bew pois " << dy << "\n";

    translate(appication->get_canvas("MainCanvas"), dx, dy);

    return 0;

}

int pan_to_intersection(IntersectionIndex id, ezgl::application * app) {

    auto lat = getIntersectionPosition(id);

    general_pan(lat);

    draw_pois(app->get_renderer());

    return 0; //idk why there is a return // it is necessary
}

int add_points_of_interest(std::string type) {

    pois_selected.clear();

    auto result = data.points_of_interest.equal_range(type);

    for (auto start = result.first; start != result.second; start++) {

        POI poi = start->second;

        poi.position = getPointOfInterestPosition(poi.idx);
        poi.location = xy_from_point(poi.position);
        pois_selected.push_back(poi);

    }

    return (pois_selected.size() > 0) ? 0 : -1;

}
