#include "Node_m4.h"
#include "adjacent_headers.h"

Node_m4::Node_m4(LatLon pos, int _id) {
    position = pos;
    parent = nullptr;
    localGoal = DBL_MAX;
    id = _id;
}

Node_m4::Node_m4(LatLon pos, Node_m4* _parent, int _id) {
    position = pos;
    parent = _parent;
    localGoal = DBL_MAX;
    id = _id;

}

Node_m4::~Node_m4() {

}

void Node_m4::setParent(Node_m4* & _parent) {
    parent = _parent;
}

void Node_m4::setLocalGoal(double travel_time) {
    localGoal = travel_time + parent->localGoal;
}

void Node_m4::storeNeighbours(std::vector<Node_m4*> & nodes_m4) {
    std::vector<int> adj;
    find_adjacent_inter(id, adj);
    for (auto it = adj.begin(); it != adj.end(); it++) {
        neighbours.push_back(nodes_m4.at(*it));
    }
}

bool Node_m4::operator == (const Node_m4* rhs) {
    return id == rhs->id;
}

void Node_m4::resetNode(){
    parent = nullptr;
    localGoal = DBL_MAX;
    neighbours.clear();
    visited = false;
}

