 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TransmissionInvoker.h
 * Author: wadhaw13
 *
 * Created on March 9, 2020, 9:56 PM
 */

#ifndef TRANSMISSIONINVOKER_H
#define TRANSMISSIONINVOKER_H

#include <curl/curl.h>
#include <time.h>
#include "TToken.h"
#include <iostream>

class TransmissionInvoker{


    public :

    static TransmissionInvoker & getInstance() {

        static TransmissionInvoker the_instance;

        return the_instance;

    }

    TransmissionToken & getToken() {

        return this->m_token;

    }

    int invokeTransmissionSTDOUT(std::string request) {

        curl_easy_reset(m_handle);

        CURLcode res = curl_easy_setopt(m_handle, CURLOPT_URL, request.c_str());

        CURLcode new_res = CURLE_SEND_ERROR;

        // Specify the error message buffer
        if (res == CURLE_OK)
            res = curl_easy_setopt(m_handle, CURLOPT_ERRORBUFFER, errbuf);

        if (res != CURLE_OK) {
            std::cout << "ERROR: Unable to set libcurl option" << std::endl;
            std::cout << curl_easy_strerror(res) << std::endl;
        } else {
            new_res = curl_easy_perform(m_handle);
        }
        if (res == CURLE_OK) {
            return res;
        } else {
            std::cout << "ERROR: res == " << res << std::endl;
            return res;
        }

        if (this->m_function != nullptr) res = curl_easy_setopt(m_handle, CURLOPT_WRITEFUNCTION, m_function);

        return new_res;

    }

    Response invokeTransmission(std::string request) {

        CURLcode res = curl_easy_setopt(m_handle, CURLOPT_URL, request.c_str());

        if (res == CURLE_OK) curl_easy_setopt(m_handle, CURLOPT_WRITEFUNCTION, m_function);

        if (res != CURLE_OK) return {
            res, 0
        };

        TransmissionStruct t_struct{0,0,0};                        

        // Specify the error message buffer
        if (res == CURLE_OK)
            res = curl_easy_setopt(m_handle, CURLOPT_ERRORBUFFER, errbuf);

        if (res == CURLE_OK)
            res = curl_easy_setopt(m_handle, CURLOPT_WRITEDATA, &t_struct);

        if (res != CURLE_OK) {
            std::cout << "ERROR: Unable to set libcurl option" << std::endl;
            std::cout << curl_easy_strerror(res) << std::endl;
        } else {
            res = curl_easy_perform(m_handle);
        }
        if (res == CURLE_OK) {
            return
            {
                res, t_struct
            };
            ;
        } else {
            std::cout << "ERROR: res == " << res << std::endl;
            return
            {
                res, t_struct
            };
            ;
        }

        return
        {res, t_struct};

    }


    //Use a custom function to receive the incoming data from the internet.
    //The function must have a strict definition as 
    //size_t write_data(void*, size_t, size_t, void*);

    int setCustomReceiver(size_t(*write_data) (void*, size_t, size_t, void*)) {

        this->m_function = (void*) write_data;

        return curl_easy_setopt(m_handle, CURLOPT_WRITEFUNCTION, write_data);

    }

private:

    char errbuf[CURL_ERROR_SIZE] =
    {0}; // Buffer for storing error messages

    TransmissionInvoker() {

        using namespace std;

        CURLcode res = curl_global_init(CURL_GLOBAL_ALL);

        if (res != CURLE_OK) {
            std::cout << "ERROR: Unable to initialize libcurl" << std::endl;
            std::cout << curl_easy_strerror(res) << endl;
        }

        m_handle = curl_easy_init();

        if (!m_handle) {
            std::cout << "ERROR: Unable to get easy handle" << endl;
        }

        curl_easy_setopt(m_handle, CURLOPT_FOLLOWLOCATION, true);

        this->m_token.public_id = clock();

        this->m_token.m_handle = this->m_handle;

    }

    TransmissionInvoker(const TransmissionInvoker & orig) = delete;

    TransmissionInvoker& operator = (const TransmissionInvoker&) = delete;

    CURL *m_handle;

    TransmissionToken m_token;

    virtual ~TransmissionInvoker() {

        curl_easy_cleanup(m_handle);

        m_handle = nullptr;

        curl_global_cleanup();

    }

    void* m_function = nullptr;


};

#endif /* TRANSMISSIONINVOKER_H */

