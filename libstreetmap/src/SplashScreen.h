/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SplashScreen.h
 * Author: wadhaw13
 *
 * Created on March 28, 2020, 12:47 AM
 */

#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include <string>
#include <iostream>
#include <thread>
#include <gtk/gtk.h>
#include <gtk/gtkmain.h>

//g++ `pkg-config --cflags gtk+-3.0` asd.c `pkg-config --libs gtk+-3.0`

gboolean close_screen(gpointer);

class SplashScreen {

public:
    
    SplashScreen(std::string);       
    
    void showAndAutoHide();
    
    void setTimeout(int);

    void setImage();
    
    virtual ~SplashScreen();
    
private:

    GtkBuilder* builder = nullptr;
    
    GObject* window = nullptr;
    
    int m_out = 5;      
    
};

#endif /* SPLASHSCREEN_H */

