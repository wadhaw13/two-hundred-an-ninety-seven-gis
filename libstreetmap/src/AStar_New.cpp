#include "AStar_New.h"
#include "m3.h"
std::vector<Node*> nodes;

int getSegment(int id1, int id2) {
    for (int i = 0; i < getIntersectionStreetSegmentCount(id1); i++) {
        int segment = getIntersectionStreetSegment(id1, i);
        if (getInfoStreetSegment(segment).to == id2) {
            return segment;
        } else if (getInfoStreetSegment(segment).from == id2) {
            return segment;
        }
    }
    return -1;
}

std::vector<int> getSegment_vec(int id1, int id2) { //to handle multiple street vectors
    std::vector<int> segs;
    for (int i = 0; i < getIntersectionStreetSegmentCount(id1); i++) {
        int segment = getIntersectionStreetSegment(id1, i);
        if (getInfoStreetSegment(segment).to == id2) {
            segs.push_back(segment);
        } else if (getInfoStreetSegment(segment).from == id2) {
            segs.push_back(segment);
        }
    }
    return segs;
}

void getSegment_vec(int id1, int id2, std::vector<int>& segs) { //to handle multiple street vectors
    for (int i = 0; i < getIntersectionStreetSegmentCount(id1); i++) {
        int segment = getIntersectionStreetSegment(id1, i);
        if (getInfoStreetSegment(segment).to == id2) {
            segs.push_back(segment);
        } else if (getInfoStreetSegment(segment).from == id2) {
            segs.push_back(segment);
        }
    }
}

bool sortMethod(Node* & lhs, Node* & rhs) {
    
    return lhs->globalGoal < rhs->globalGoal;
}

bool apply_turn_penalty(int prev_seg, int next_seg){
    if(getInfoStreetSegment(prev_seg).streetID ==  getInfoStreetSegment(next_seg).streetID){
        return false; //same street
    }
    else return true; //not same street
}

bool oneWayStreet(int seg_id, int inter_id){
    return (getInfoStreetSegment(seg_id).oneWay && getInfoStreetSegment(seg_id).to == inter_id);
}



std::vector<StreetSegmentIndex> algorithm_a_star(Node* start, Node* end, double turn_penalty, std::vector<int>& path) {    
    double t1, t2, t3;
    Node* current = start;
    start->localGoal = 0;
    start->setGlobalGoal(getIntersectionPosition(end->id));

    std::priority_queue<Node*, std::vector<Node*> , myComparator > notTested;
    notTested.push(start);

    while (!notTested.empty() && current != end) {
        
        // sort the nodes
      //  notTested.sort(sortMethod);

        // if node has been visited, remove from list
        while (!notTested.empty() && notTested.top()->visited) {
            notTested.pop();
        }

        if (notTested.empty()) {
            break;
        }
        current = notTested.top();
        auto starting = std::chrono::high_resolution_clock::now();
        current->storeNeighbours();
        auto diff = std::chrono::high_resolution_clock::now() - starting;
        auto t11 = std::chrono::duration_cast<std::chrono::nanoseconds>(diff);
        t1 += t11.count();
        
        current->visited = true;

        // check the neighbours
        for (auto it2 = current->neighbours.begin(); it2 != current->neighbours.end(); it2++) {
            
            auto starter = std::chrono::high_resolution_clock::now();

            double maybeLower = DBL_MAX;
            int seg = -1;
            std::vector<int> evaluating_segment;
            getSegment_vec(current->id, (*it2)->id, evaluating_segment);
            
            auto diff1 = std::chrono::high_resolution_clock::now() - starter;
            auto t22 = std::chrono::duration_cast<std::chrono::nanoseconds>(diff1);
            t2 += t22.count();
            
            auto start1 = std::chrono::high_resolution_clock::now();

            
            if(evaluating_segment.size() == 0) maybeLower = DBL_MAX;
            else{
                for(int i = 0; i < evaluating_segment.size(); i++){
                    if (oneWayStreet(evaluating_segment.at(i), current->id)) continue;
             //       else if (data.segment_travel_time.at(evaluating_segment.at(i)) == 0) continue;
                    else if(current == start){
                        double temp = current->localGoal + data.segment_travel_time.at(evaluating_segment.at(i));
                        if (temp < maybeLower){
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }
                    else{
                        double temp = current->localGoal + data.segment_travel_time.at(evaluating_segment.at(i));
                        if(apply_turn_penalty(current->cameFrom, evaluating_segment.at(i))){
                                temp+=turn_penalty;
                            }
                        if (temp < maybeLower){
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }
                    
                }
                
            }
            if (maybeLower < (*it2)->localGoal){
                (*it2)->parent = current;
                (*it2)->localGoal = maybeLower;
                (*it2)->setGlobalGoal(getIntersectionPosition(end->id));
                (*it2)->cameFrom = seg;                        
            }
            
           if (!(*it2)->visited) {
                notTested.push(*it2);
            }
           auto diff3 = std::chrono::high_resolution_clock::now() - start1;
            auto t33 = std::chrono::duration_cast<std::chrono::nanoseconds>(diff1);
            t3 += t33.count(); 
            
        }
    }

    // get the path
    auto starter = std::chrono::high_resolution_clock::now();

    if (current == end) {
        while (current != start) {
            path.push_back(current->cameFrom);
            current = current->parent;
        }
     //   path.push_back(current->id);
    }
    
    auto diff1 = std::chrono::high_resolution_clock::now() - starter;
    auto t22 = std::chrono::duration_cast<std::chrono::nanoseconds>(diff1);
    
  //  //std::cout << "Storing neighbours: " << t1 << std::endl;
   // //std::cout << "Getting segment: " << t2 << std::endl;
    ////std::cout << "Saving segments: " << t22.count() << std::endl;
   // //std::cout << "eval segments: " << t3 << std::endl;



//    for (int i = 0; i < path.size(); ++i){
//        //std::cout << getIntersectionName(path.at(i)) << std::endl;
//    }
//    
    return path;

}

std::vector<int> algorithm_a_star(std::vector<int>& start_array, Node* end, double turn_penalty, std::vector<int>& path){
    double t1, t2, t3;
    if(start_array.size() == 0) {
        std::cout << "empty start vector" << std::endl;
        return start_array;
    }

   // Node* start = nodes.at(start_array[0]);
    Node* current = nodes.at(start_array[0]);
   // std::cout << start_array[0] << "  " << start->id << std::endl;
    std::priority_queue<Node*, std::vector<Node*> , myComparator > notTested;
    
    for(int i = 0; i < start_array.size(); i++){
        Node* start = nodes.at(start_array[i]);
      //  std::cout << start_array[i] << " " << start->id<< std::endl;
        start->localGoal = 0;
        start->setGlobalGoal(getIntersectionPosition(end->id));
        //start->setGlobalGoal_walking();
        notTested.push(start);
    }
    while (!notTested.empty() && current != end) {
        //std::cout << "nottested size " << notTested.size() << std::endl;
        // sort the nodes
      //  notTested.sort(sortMethod);

        // if node has been visited, remove from list
        //std::cout << (notTested.top())->id << " " << std::endl;
        while (!notTested.empty() && notTested.top()->visited) {
            notTested.pop();
           // std::cout << "popef" << std::endl;
        }
        //std::cout << "nottested size after poping" << notTested.size() << std::endl;
        if (notTested.empty()) {
            //std::cout << "breaking out because notTested is ecmpty" <<std::endl;
            break;
        }
        current = notTested.top();
        auto starting = std::chrono::high_resolution_clock::now();
        current->storeNeighbours_multiple_start();
        auto diff = std::chrono::high_resolution_clock::now() - starting;
        auto t11 = std::chrono::duration_cast<std::chrono::nanoseconds>(diff);
        t1 += t11.count();
        
        current->visited = true;

        // check the neighbours
        for (auto it2 = current->neighbours.begin(); it2 != current->neighbours.end(); it2++) {
            
            auto starter = std::chrono::high_resolution_clock::now();

            double maybeLower = DBL_MAX;
            int seg = -1;
            std::vector<int> evaluating_segment;
            getSegment_vec(current->id, (*it2)->id, evaluating_segment);
            
            auto diff1 = std::chrono::high_resolution_clock::now() - starter;
            auto t22 = std::chrono::duration_cast<std::chrono::nanoseconds>(diff1);
            t2 += t22.count();
            
            auto start1 = std::chrono::high_resolution_clock::now();

            
            if(evaluating_segment.size() == 0) maybeLower = DBL_MAX;
            else{
                for(int i = 0; i < evaluating_segment.size(); i++){
                    if (oneWayStreet(evaluating_segment.at(i), current->id)) continue;
                    //else if (data.segment_travel_time.at(evaluating_segment.at(i)) == 0) continue;
                    else if(std::find(start_array.begin(), start_array.end(), current-> id) != start_array.end()){
                        double temp = current->localGoal + data.segment_travel_time.at(evaluating_segment.at(i));
                        if (temp < maybeLower){
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }
                    else{
                        double temp = current->localGoal + data.segment_travel_time.at(evaluating_segment.at(i));
                        if(apply_turn_penalty(current->cameFrom, evaluating_segment.at(i))){
                                temp+=turn_penalty;
                            }
                        if (temp < maybeLower){
                            maybeLower = temp;
                            seg = evaluating_segment.at(i);
                        }
                    }
                    
                }
                
            }
            if (maybeLower < (*it2)->localGoal){
                (*it2)->parent = current;
                (*it2)->localGoal = maybeLower;
                (*it2)->setGlobalGoal(getIntersectionPosition(end->id));
                //(*it2)->setGlobalGoal_walking();
                (*it2)->cameFrom = seg;                        
            }
            
           if (!(*it2)->visited) {
                notTested.push(*it2);
            }
           auto diff3 = std::chrono::high_resolution_clock::now() - start1;
            auto t33 = std::chrono::duration_cast<std::chrono::nanoseconds>(diff1);
            t3 += t33.count(); 
            
        }
    }

    // get the path
    auto starter = std::chrono::high_resolution_clock::now();
    //std::cout << current->id << " " << end->id << std::endl;
    if (current == end) {
        //std::cout << "found end" << std::endl;
        while (current->parent != nullptr) { //crashes here because maybe it used another start node but now only checking for one start node
            path.push_back(current->cameFrom);
            current = current->parent;
        }
        if(current->parent == nullptr){
           // std::cout << "found start node " << current->id << std::endl;
        }
     //   path.push_back(current->id);
    }
    
    auto diff1 = std::chrono::high_resolution_clock::now() - starter;
    auto t22 = std::chrono::duration_cast<std::chrono::nanoseconds>(diff1);
    
  //  std::cout << "Storing neighbours: " << t1 << std::endl;
   // std::cout << "Getting segment: " << t2 << std::endl;
    //std::cout << "Saving segments: " << t22.count() << std::endl;
   // std::cout << "eval segments: " << t3 << std::endl;



//    for (int i = 0; i < path.size(); ++i){
//        std::cout << getIntersectionName(path.at(i)) << std::endl;
//    }
//    
    return path;    
}

//std::vector<int> algorithm_a_star(std::vector<int>& start_array, Node* end, double turn_penalty, std::vector<int>& path){
//        double path_travel_time = DBL_MAX;
//        //find the shortest path
//        //std::cout << "start array size " << start_array.size() << std::endl;
//        for(int i = 0; i < start_array.size(); i++){
//                Node* start = nodes.at(start_array[i]);
//                std::vector<int> test_path; 
//                double test_travel_time;
//                algorithm_a_star(start, end, turn_penalty, test_path);
//
//                for (int j = 0; j < nodes.size(); ++j){
//                    (nodes.at(j))->resetNode();
//                } 
//                //std::cout << "testpath size " << test_path.size() << std::endl;
//                if(test_path.size() == 0) continue;
//        test_travel_time = compute_path_travel_time(test_path, turn_penalty);
//        if(test_travel_time < path_travel_time){
//                        path_travel_time = test_travel_time;
//                        path = test_path;
//                }
//        }
//        
//    return path;    
//}
