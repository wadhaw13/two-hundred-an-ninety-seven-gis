/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Timer.h
 * Author: wadhaw13
 *
 * Created on April 6, 2020, 11:42 AM
 */

#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <iostream>

class Timer {

public:

    Timer();        
    
    void reset();        
    
    double ellapsed();
    
    void operator() ();
    
private:
    
    std::chrono::time_point<std::chrono::high_resolution_clock> previous;

};

#endif /* TIMER_H */

