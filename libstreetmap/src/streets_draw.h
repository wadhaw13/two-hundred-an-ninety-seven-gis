#ifndef STREETS_DRAW_H
#define STREETS_DRAW_H

#include "ezgl/application.hpp"
#include "ezgl/graphics.hpp"
#include "m1.h"

void streets_draw(ezgl::renderer *g);
void highlight_selected(ezgl::renderer *g, LatLon pos, double radius, double, double);
void draw_street(ezgl::renderer *g, std::vector<int> segments, ezgl::color COLOR, ezgl::line_cap CAP, ezgl::line_dash DASH);
void determine_factors(ezgl::renderer *g);

#endif /* STREETS_DRAW_H */

