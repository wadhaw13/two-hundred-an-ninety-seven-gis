/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataService.h
 * Author: wadhaw13
 *
 * Created on March 9, 2020, 10:25 PM
 */

#ifndef DATASERVICE_H
#define DATASERVICE_H

#include "TransmissionInvoker.h"
#include "TToken.h"
#include "RequestBuilder.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <fstream>
#include <string>
#include <time.h>

using boost::property_tree::ptree;
using boost::property_tree::read_json;

struct PlaceFromText {

    double Lat, Lon;
    std::string name;
    std::string addr;
    std::vector <std::string> ref;

};


class DataService {
public:

    DataService();

    DataService(const DataService & orig) = delete;

    std::vector <PlaceFromText>  InvokeRequest(std::string request);

    void InvokeRequest(std::string & request, double, double);

    //returns the file path of the downloaded image    
    std::string InvokePhotoRequest(std::string & params);

    void* GetLastData();

    void updateKey(std::string);

    virtual ~DataService();        

private:

    std::vector <PlaceFromText> m_text_list;
    
    void cleanData(std::istringstream& );
    
    TransmissionToken m_token;

    RequestBuilder m_builder;
    
    ptree m_tree;

};

#endif /* DATASERVICE_H */

