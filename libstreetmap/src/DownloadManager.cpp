#include "TToken.h"
#include <string.h>

size_t curl_default(void *buffer, size_t , size_t nmemb, void *userp) {


    if (buffer && nmemb && userp) {

        TransmissionStruct* pMyStruct = (TransmissionStruct *) userp;

        if (pMyStruct->response == nullptr) {

            // Case when first time write_data() is invoked
            pMyStruct->response = new char[nmemb];

            memcpy(pMyStruct->response, (char *) buffer, nmemb);

        } else {

            // Case when second or subsequent time write_data() is invoked
            char *oldResp = pMyStruct->response;

            pMyStruct->response = new char[pMyStruct->size + nmemb];

            // Copy old data
            memcpy(pMyStruct->response, oldResp, pMyStruct->size);

            // Append new data
            memcpy(pMyStruct->response + pMyStruct->size, (char *) buffer, nmemb);

            delete []oldResp;
        }

        pMyStruct->size += nmemb;

        pMyStruct->response[pMyStruct->size] = '\0';

    }

    return nmemb;
}

size_t json_data(void *buffer, size_t , size_t nmemb, void *userp) {

    if (buffer && nmemb && userp) {

        TransmissionStruct* pMyStruct = (TransmissionStruct *) userp;

        // Writes to struct passed in from main
        if (pMyStruct->response == nullptr) {
            // Case when first time write_data() is invoked
            pMyStruct->response = new char[nmemb + 1];

            strncpy(pMyStruct->response, (char *) buffer, nmemb);
        }
        else {
            // Case when second or subsequent time write_data() is invoked
            char *oldResp = pMyStruct->response;

            pMyStruct->response = new char[pMyStruct->size + nmemb + 1];

            // Copy old data
            strncpy(pMyStruct->response, oldResp, pMyStruct->size);

            // Append new data
            strncpy(pMyStruct->response + pMyStruct->size, (char *) buffer, nmemb);

            delete []oldResp;
        }

        pMyStruct->size += nmemb;

        pMyStruct->response[pMyStruct->size] = '\0';
    }

    return nmemb;
}

