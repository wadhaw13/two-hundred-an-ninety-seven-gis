/* 
 * File:   ChangeStreetStruct.h
 * Author: wadhaw13
 * ECE297 2020
 * Created on March 23, 2020, 4:33 AM
 */

#ifndef CHANGESTREETSTRUCT_H
#define CHANGESTREETSTRUCT_H

#include "poi.h"

struct ChangeStreetStruct {

    //previous street segment's non common intersection
    POI from;
    //common intersection between current and the previous intersection
    POI middle;
    //current street segment's non common intersection
    POI to;

};


#endif /* CHANGESTREETSTRUCT_H */

