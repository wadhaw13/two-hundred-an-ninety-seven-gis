#ifndef NODE_M4_H
#define NODE_M4_H

#include "Data_Structures.h"

class Node_m4 {
public:

    bool visited = false;
    double localGoal;
    LatLon position;
    std::vector<Node_m4*> neighbours;
    Node_m4* parent;
    IntersectionIndex id;
    StreetSegmentIndex cameFrom;

    Node_m4(LatLon pos, int _id);

    Node_m4(LatLon pos, Node_m4* _parent, int _id);

    ~Node_m4();

    // Set the parent of the node
    void setParent(Node_m4* & _parent);

    // Set the value of the total travel time from start node to current node
    void setLocalGoal(double travel_time);

    // Store the neighbours of the current node
    void storeNeighbours(std::vector<Node_m4*> & nodes_m4);
    
    // Reset the node to initial values
    void resetNode();

    bool operator==(const Node_m4* rhs);


};

#endif /* NODE_M4_H */

