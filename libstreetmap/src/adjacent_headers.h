/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   adjacent_headers.h
 * Author: wadhaw13
 *
 * Created on March 26, 2020, 4:47 AM
 */

#ifndef ADJACENT_HEADERS_H
#define ADJACENT_HEADERS_H

#include <vector>

std::vector<int> find_adjacent_inter(int intersection_id, std::vector<int> &myvector);
std::vector<int> find_adjacent_intersections_nocheck(int intersection_id);

#endif /* ADJACENT_HEADERS_H */

