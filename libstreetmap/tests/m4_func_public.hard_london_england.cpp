//#include <random>
//#include <iostream>
//#include <unittest++/UnitTest++.h>
//
//#include "StreetsDatabaseAPI.h"
//#include "m1.h"
//#include "m3.h"
//#include "m4.h"
//
//#include "unit_test_util.h"
//#include "courier_verify.h"
//
//using ece297test::relative_error;
//using ece297test::courier_path_is_legal_with_capacity;
//
//
//SUITE(hard_london_england_public) {
//    TEST(hard_london_england) {
//        std::vector<DeliveryInfo> deliveries;
//        std::vector<IntersectionIndex> depots;
//        float turn_penalty;
//        float truck_capacity;
//        std::vector<CourierSubpath> result_path;
//        bool is_legal;
//
//        deliveries = {DeliveryInfo(145106, 107580, 44.50791), DeliveryInfo(72833, 54816, 41.23623), DeliveryInfo(142731, 47033, 114.29684), DeliveryInfo(210332, 105966, 23.00182), DeliveryInfo(160080, 75617, 120.91389), DeliveryInfo(198462, 146977, 34.02746), DeliveryInfo(218575, 168940, 139.60036), DeliveryInfo(214903, 229516, 48.80639), DeliveryInfo(220508, 145226, 132.93713), DeliveryInfo(243941, 72079, 8.27069), DeliveryInfo(20845, 189460, 34.73044), DeliveryInfo(64450, 20584, 73.16920), DeliveryInfo(178028, 77008, 150.29890), DeliveryInfo(21952, 151181, 78.67591), DeliveryInfo(46501, 193612, 164.98338), DeliveryInfo(125444, 126224, 112.69604), DeliveryInfo(126244, 120705, 150.61919), DeliveryInfo(104444, 149413, 138.51927), DeliveryInfo(137377, 141974, 63.80986), DeliveryInfo(236961, 191304, 165.68761), DeliveryInfo(147419, 207131, 106.51025), DeliveryInfo(97570, 22953, 156.08191), DeliveryInfo(182269, 96938, 29.51083), DeliveryInfo(235716, 133426, 117.06736), DeliveryInfo(222309, 17567, 158.52562), DeliveryInfo(40378, 101648, 190.48668), DeliveryInfo(221008, 181239, 182.50621), DeliveryInfo(237290, 44455, 156.72679)};
//        depots = {5};
//        turn_penalty = 15.000000000;
//        truck_capacity = 380.315582275;
//        {
//        	ECE297_TIME_CONSTRAINT(45000);
//        	
//        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
//        }
//
//        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
//        CHECK(is_legal);
//
//        if(is_legal) {
//        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
//        	std::cout << "QoR hard_london_england: " << path_cost << std::endl;
//        } else {
//        	std::cout << "QoR hard_london_england: INVALID" << std::endl;
//        }
//
//    } //hard_london_england
//
//    TEST(hard_multi_london_england) {
//        std::vector<DeliveryInfo> deliveries;
//        std::vector<IntersectionIndex> depots;
//        float turn_penalty;
//        float truck_capacity;
//        std::vector<CourierSubpath> result_path;
//        bool is_legal;
//
//        deliveries = {DeliveryInfo(242736, 213887, 193.81795), DeliveryInfo(184660, 7301, 186.19882), DeliveryInfo(128901, 57215, 2.90214), DeliveryInfo(195870, 169115, 89.01582), DeliveryInfo(208889, 109633, 82.47247), DeliveryInfo(45067, 35135, 28.59369), DeliveryInfo(93003, 94067, 46.00364), DeliveryInfo(145667, 45906, 41.82777), DeliveryInfo(80757, 211933, 68.05492), DeliveryInfo(226286, 213887, 79.20071), DeliveryInfo(29608, 45306, 97.61277), DeliveryInfo(229435, 193876, 65.87426), DeliveryInfo(7342, 213887, 16.54139), DeliveryInfo(119392, 38802, 69.46088), DeliveryInfo(5742, 142079, 146.33839), DeliveryInfo(75014, 203297, 100.59782), DeliveryInfo(40316, 151234, 157.35182), DeliveryInfo(110910, 199473, 129.96677), DeliveryInfo(229435, 53679, 25.39208), DeliveryInfo(196869, 241411, 101.23836), DeliveryInfo(43904, 133774, 77.03854), DeliveryInfo(229435, 137461, 127.61972), DeliveryInfo(228776, 213887, 131.37521), DeliveryInfo(229435, 92734, 13.02049), DeliveryInfo(151778, 21706, 112.16380), DeliveryInfo(175518, 41168, 59.02165), DeliveryInfo(49692, 88911, 34.13472), DeliveryInfo(229435, 48808, 117.05125)};
//        depots = {11, 41690};
//        turn_penalty = 15.000000000;
//        truck_capacity = 337.152282715;
//        {
//        	ECE297_TIME_CONSTRAINT(45000);
//        	
//        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
//        }
//
//        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
//        CHECK(is_legal);
//
//        if(is_legal) {
//        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
//        	std::cout << "QoR hard_multi_london_england: " << path_cost << std::endl;
//        } else {
//        	std::cout << "QoR hard_multi_london_england: INVALID" << std::endl;
//        }
//
//    } //hard_multi_london_england
//
//} //hard_london_england_public
//
