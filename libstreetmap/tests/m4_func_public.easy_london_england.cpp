//#include <random>
//#include <iostream>
//#include <unittest++/UnitTest++.h>
//
//#include "StreetsDatabaseAPI.h"
//#include "m1.h"
//#include "m3.h"
//#include "m4.h"
//
//#include "unit_test_util.h"
//#include "courier_verify.h"
//
//using ece297test::relative_error;
//using ece297test::courier_path_is_legal_with_capacity;
//
//
//SUITE(easy_london_england_public) {
//    TEST(easy_london_england) {
//        std::vector<DeliveryInfo> deliveries;
//        std::vector<IntersectionIndex> depots;
//        float turn_penalty;
//        float truck_capacity;
//        std::vector<CourierSubpath> result_path;
//        bool is_legal;
//
//        deliveries = {DeliveryInfo(139505, 56515, 114.79240), DeliveryInfo(165433, 193351, 144.10674), DeliveryInfo(197111, 65856, 176.41570), DeliveryInfo(221579, 190173, 161.74615), DeliveryInfo(62536, 166986, 48.78675), DeliveryInfo(133587, 105094, 185.07404), DeliveryInfo(47564, 183048, 108.93574)};
//        depots = {16};
//        turn_penalty = 15.000000000;
//        truck_capacity = 974.874633789;
//        {
//        	ECE297_TIME_CONSTRAINT(45000);
//        	
//        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
//        }
//
//        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
//        CHECK(is_legal);
//
//        if(is_legal) {
//        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
//        	std::cout << "QoR easy_london_england: " << path_cost << std::endl;
//        } else {
//        	std::cout << "QoR easy_london_england: INVALID" << std::endl;
//        }
//
//    } //easy_london_england
//
//} //easy_london_england_public
//
