#include <random>
#include <iostream>
#include <unittest++/UnitTest++.h>

#include "StreetsDatabaseAPI.h"
#include "m1.h"
#include "m3.h"
#include "m4.h"

#include "unit_test_util.h"
#include "courier_verify.h"

using ece297test::relative_error;
using ece297test::courier_path_is_legal_with_capacity;


SUITE(easy_new_york_usa_public) {
    TEST(easy_new_york_usa) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        float turn_penalty;
        float truck_capacity;
        std::vector<CourierSubpath> result_path;
        bool is_legal;

        deliveries = {DeliveryInfo(151100, 135706, 53.31088), DeliveryInfo(191139, 52311, 169.31749), DeliveryInfo(135081, 39949, 124.38162), DeliveryInfo(193832, 143856, 25.56255), DeliveryInfo(154517, 79207, 129.98366), DeliveryInfo(192967, 171268, 41.05344), DeliveryInfo(189700, 73148, 90.72692), DeliveryInfo(149790, 116805, 79.29823), DeliveryInfo(49438, 60236, 4.35320), DeliveryInfo(59649, 182300, 133.52373), DeliveryInfo(91933, 46103, 123.70870), DeliveryInfo(123085, 158139, 142.89053), DeliveryInfo(116858, 189160, 69.00546), DeliveryInfo(146075, 68310, 162.74165), DeliveryInfo(205654, 173430, 102.08238), DeliveryInfo(211285, 123431, 18.80107), DeliveryInfo(169139, 177660, 146.41916), DeliveryInfo(122035, 166673, 198.81140), DeliveryInfo(38307, 114683, 24.81209), DeliveryInfo(41608, 198443, 104.19133), DeliveryInfo(166359, 116666, 19.50759), DeliveryInfo(57610, 14458, 50.89673), DeliveryInfo(114759, 54019, 36.02774), DeliveryInfo(160126, 63643, 94.95014), DeliveryInfo(105967, 102323, 138.08812)};
        depots = {14, 54705, 172428, 144716};
        turn_penalty = 15.000000000;
        truck_capacity = 2402.911621094;
        {
        	ECE297_TIME_CONSTRAINT(45000);
        	
        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        }

        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
        CHECK(is_legal);

        if(is_legal) {
        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
        	std::cout << "QoR easy_new_york_usa: " << path_cost << std::endl;
        } else {
        	std::cout << "QoR easy_new_york_usa: INVALID" << std::endl;
        }

    } //easy_new_york_usa

} //easy_new_york_usa_public

