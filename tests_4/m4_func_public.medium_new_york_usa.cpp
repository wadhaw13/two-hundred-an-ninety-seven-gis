#include <random>
#include <iostream>
#include <unittest++/UnitTest++.h>

#include "StreetsDatabaseAPI.h"
#include "m1.h"
#include "m3.h"
#include "m4.h"

#include "unit_test_util.h"
#include "courier_verify.h"

using ece297test::relative_error;
using ece297test::courier_path_is_legal_with_capacity;


SUITE(medium_new_york_usa_public) {
    TEST(medium_new_york_usa) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        float turn_penalty;
        float truck_capacity;
        std::vector<CourierSubpath> result_path;
        bool is_legal;

        deliveries = {DeliveryInfo(72026, 36545, 13.61199), DeliveryInfo(19597, 119904, 64.46082), DeliveryInfo(12775, 57217, 188.11136), DeliveryInfo(51095, 129443, 122.91425), DeliveryInfo(80315, 3571, 194.16504), DeliveryInfo(93915, 4301, 140.79376), DeliveryInfo(61471, 141877, 55.83173), DeliveryInfo(67886, 184851, 53.42627), DeliveryInfo(34126, 127024, 139.53435), DeliveryInfo(191809, 169800, 62.54547), DeliveryInfo(100101, 107300, 132.25157), DeliveryInfo(55011, 80127, 115.65539), DeliveryInfo(185807, 97827, 1.23825), DeliveryInfo(10046, 110481, 171.64159), DeliveryInfo(124748, 186496, 111.32867), DeliveryInfo(26048, 178889, 146.61469), DeliveryInfo(210231, 41979, 37.15287), DeliveryInfo(76813, 125553, 5.99976), DeliveryInfo(79266, 66099, 14.50743), DeliveryInfo(128238, 76216, 88.43934), DeliveryInfo(81428, 170582, 55.23724), DeliveryInfo(131241, 103483, 157.20303), DeliveryInfo(207847, 75233, 147.67279), DeliveryInfo(50143, 149352, 113.03408), DeliveryInfo(51802, 148100, 68.13067), DeliveryInfo(70536, 67779, 135.47527), DeliveryInfo(92630, 129760, 126.80992), DeliveryInfo(155554, 163721, 41.46676), DeliveryInfo(129984, 140611, 41.83722), DeliveryInfo(162244, 206560, 124.71667), DeliveryInfo(159758, 125796, 198.43752), DeliveryInfo(181451, 201657, 177.14240), DeliveryInfo(151015, 201190, 40.57933), DeliveryInfo(141289, 176518, 4.58260), DeliveryInfo(85392, 82288, 6.86531), DeliveryInfo(207913, 42866, 195.18217), DeliveryInfo(141230, 113705, 39.01222), DeliveryInfo(37765, 79813, 159.00911), DeliveryInfo(136294, 17568, 128.93202), DeliveryInfo(164575, 65379, 77.88078), DeliveryInfo(161988, 164505, 183.02200), DeliveryInfo(205279, 34857, 55.50504), DeliveryInfo(138949, 97414, 83.74704), DeliveryInfo(40404, 104865, 153.42888), DeliveryInfo(108625, 70941, 165.24220), DeliveryInfo(78847, 72782, 6.51730), DeliveryInfo(134540, 18631, 196.69681), DeliveryInfo(194042, 170290, 151.72243), DeliveryInfo(156340, 31995, 193.50153), DeliveryInfo(37976, 149705, 111.69054)};
        depots = {19, 72940, 86939, 121473, 186960, 162714, 12846, 126960, 11070, 208883};
        turn_penalty = 15.000000000;
        truck_capacity = 384.027862549;
        {
        	ECE297_TIME_CONSTRAINT(45000);
        	
        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        }

        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
        CHECK(is_legal);

        if(is_legal) {
        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
        	std::cout << "QoR medium_new_york_usa: " << path_cost << std::endl;
        } else {
        	std::cout << "QoR medium_new_york_usa: INVALID" << std::endl;
        }

    } //medium_new_york_usa

} //medium_new_york_usa_public

