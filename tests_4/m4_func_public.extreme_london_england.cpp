#include <random>
#include <iostream>
#include <unittest++/UnitTest++.h>

#include "StreetsDatabaseAPI.h"
#include "m1.h"
#include "m3.h"
#include "m4.h"

#include "unit_test_util.h"
#include "courier_verify.h"

using ece297test::relative_error;
using ece297test::courier_path_is_legal_with_capacity;


SUITE(extreme_london_england_public) {
    TEST(extreme_london_england) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        float turn_penalty;
        float truck_capacity;
        std::vector<CourierSubpath> result_path;
        bool is_legal;

        deliveries = {DeliveryInfo(216856, 57843, 62.81969), DeliveryInfo(139505, 164450, 169.45415), DeliveryInfo(218500, 83619, 120.87811), DeliveryInfo(131100, 53426, 107.26342), DeliveryInfo(90545, 179244, 112.84417), DeliveryInfo(131187, 41514, 101.12486), DeliveryInfo(193351, 150009, 197.98770), DeliveryInfo(68859, 180777, 64.06311), DeliveryInfo(65856, 239487, 190.58286), DeliveryInfo(190533, 79754, 24.36632), DeliveryInfo(195785, 5476, 186.71504), DeliveryInfo(220591, 214659, 121.88356), DeliveryInfo(190173, 208396, 41.05192), DeliveryInfo(68188, 1114, 17.21058), DeliveryInfo(56515, 121640, 172.17006), DeliveryInfo(59799, 23427, 20.52225), DeliveryInfo(140704, 94722, 29.68092), DeliveryInfo(121136, 78088, 127.78984), DeliveryInfo(203092, 120555, 143.52280), DeliveryInfo(166986, 180684, 189.07495), DeliveryInfo(176636, 52703, 36.50724), DeliveryInfo(241531, 212481, 41.17766), DeliveryInfo(197111, 128351, 86.97710), DeliveryInfo(165433, 231024, 71.48365), DeliveryInfo(133526, 140368, 187.51186), DeliveryInfo(133587, 98608, 184.37437), DeliveryInfo(47564, 68698, 135.49522), DeliveryInfo(105094, 77596, 89.92654), DeliveryInfo(72754, 206446, 43.82170), DeliveryInfo(62536, 49056, 117.18779), DeliveryInfo(43791, 4402, 171.42778), DeliveryInfo(183048, 133366, 189.99094), DeliveryInfo(198257, 83519, 52.73379), DeliveryInfo(45668, 125635, 112.71260), DeliveryInfo(171233, 122109, 149.59293), DeliveryInfo(221579, 141100, 199.91759), DeliveryInfo(226851, 238383, 21.89534), DeliveryInfo(172730, 56053, 110.20898), DeliveryInfo(154418, 27431, 98.34561), DeliveryInfo(16528, 178140, 41.08351), DeliveryInfo(235094, 90053, 142.18568), DeliveryInfo(216238, 61752, 45.62378), DeliveryInfo(155133, 116970, 105.59533)};
        depots = {16};
        turn_penalty = 15.000000000;
        truck_capacity = 1604.034423828;
        {
        	ECE297_TIME_CONSTRAINT(45000);
        	
        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        }

        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
        CHECK(is_legal);

        if(is_legal) {
        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
        	std::cout << "QoR extreme_london_england: " << path_cost << std::endl;
        } else {
        	std::cout << "QoR extreme_london_england: INVALID" << std::endl;
        }

    } //extreme_london_england

    TEST(extreme_multi_london_england) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        float turn_penalty;
        float truck_capacity;
        std::vector<CourierSubpath> result_path;
        bool is_legal;

        deliveries = {DeliveryInfo(153799, 47989, 198.43752), DeliveryInfo(150028, 20083, 177.14240), DeliveryInfo(186007, 187157, 40.57933), DeliveryInfo(58410, 143526, 4.58260), DeliveryInfo(240326, 201787, 6.86531), DeliveryInfo(221820, 137068, 195.18217), DeliveryInfo(59217, 155804, 39.01222), DeliveryInfo(161448, 188054, 159.00911), DeliveryInfo(186007, 194667, 128.93202), DeliveryInfo(161515, 194107, 77.88078), DeliveryInfo(182627, 126296, 183.02200), DeliveryInfo(97616, 169300, 55.50504), DeliveryInfo(219266, 119877, 83.74704), DeliveryInfo(177822, 49002, 153.42888), DeliveryInfo(186007, 86003, 165.24220), DeliveryInfo(58410, 145208, 6.51730), DeliveryInfo(105890, 75561, 196.69681), DeliveryInfo(186007, 36575, 151.72243), DeliveryInfo(138862, 75561, 193.50153), DeliveryInfo(238784, 234665, 111.69054), DeliveryInfo(124175, 87126, 13.62533), DeliveryInfo(138862, 126296, 108.52561), DeliveryInfo(29777, 65408, 39.88314), DeliveryInfo(43413, 81097, 199.38493), DeliveryInfo(11485, 94067, 110.40933), DeliveryInfo(58410, 185177, 169.11343), DeliveryInfo(83381, 75561, 74.80182), DeliveryInfo(114431, 4917, 158.71130), DeliveryInfo(43171, 143804, 153.48274), DeliveryInfo(186007, 118297, 165.48535), DeliveryInfo(145134, 155804, 143.22774), DeliveryInfo(90134, 147973, 145.80942), DeliveryInfo(59217, 126296, 165.93225), DeliveryInfo(58410, 148336, 115.51421), DeliveryInfo(138862, 188054, 186.55743), DeliveryInfo(186007, 188054, 113.96986), DeliveryInfo(91812, 204497, 39.26939), DeliveryInfo(146595, 4082, 172.52121), DeliveryInfo(59217, 21298, 171.07536), DeliveryInfo(59217, 39847, 178.49565), DeliveryInfo(185469, 91598, 163.94293), DeliveryInfo(207426, 230525, 89.37597), DeliveryInfo(142606, 91239, 67.24535), DeliveryInfo(93084, 122660, 0.24800), DeliveryInfo(39012, 185177, 171.24522), DeliveryInfo(148592, 129982, 177.68697), DeliveryInfo(138862, 111831, 127.29369), DeliveryInfo(186007, 201787, 194.07770), DeliveryInfo(59217, 170732, 124.66125), DeliveryInfo(238784, 162187, 123.42377), DeliveryInfo(212406, 77481, 188.49313), DeliveryInfo(145134, 194667, 152.16040), DeliveryInfo(77605, 75561, 134.62073), DeliveryInfo(46187, 155804, 77.00558), DeliveryInfo(14603, 194667, 136.25827), DeliveryInfo(22402, 236129, 123.08618), DeliveryInfo(90613, 185177, 93.16288), DeliveryInfo(57321, 213193, 65.22521), DeliveryInfo(186007, 194667, 86.37160), DeliveryInfo(138862, 188054, 43.23465), DeliveryInfo(82337, 201787, 179.61800), DeliveryInfo(172633, 155804, 140.31825), DeliveryInfo(107359, 111359, 102.37948), DeliveryInfo(186007, 75561, 159.82764), DeliveryInfo(70270, 41777, 40.12196), DeliveryInfo(178721, 195001, 127.22589)};
        depots = {22};
        turn_penalty = 15.000000000;
        truck_capacity = 2497.282226562;
        {
        	ECE297_TIME_CONSTRAINT(45000);
        	
        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        }

        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
        CHECK(is_legal);

        if(is_legal) {
        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
        	std::cout << "QoR extreme_multi_london_england: " << path_cost << std::endl;
        } else {
        	std::cout << "QoR extreme_multi_london_england: INVALID" << std::endl;
        }

    } //extreme_multi_london_england

} //extreme_london_england_public

