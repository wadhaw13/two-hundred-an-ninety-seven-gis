#include <random>
#include <iostream>
#include <unittest++/UnitTest++.h>

#include "StreetsDatabaseAPI.h"
#include "m1.h"
#include "m3.h"
#include "m4.h"

#include "unit_test_util.h"
#include "courier_verify.h"

using ece297test::relative_error;
using ece297test::courier_path_is_legal_with_capacity;


SUITE(medium_london_england_public) {
    TEST(medium_london_england) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        float turn_penalty;
        float truck_capacity;
        std::vector<CourierSubpath> result_path;
        bool is_legal;

        deliveries = {DeliveryInfo(172633, 97616, 66.16212), DeliveryInfo(161515, 178721, 111.82599), DeliveryInfo(146595, 90613, 152.69069), DeliveryInfo(161448, 29777, 132.31892), DeliveryInfo(150028, 219266, 166.83708), DeliveryInfo(153799, 107359, 192.64343), DeliveryInfo(43171, 185469, 90.85158), DeliveryInfo(148592, 43413, 96.51131), DeliveryInfo(57321, 70270, 97.80071), DeliveryInfo(105890, 39012, 138.12209), DeliveryInfo(182627, 142606, 91.23636), DeliveryInfo(14603, 93084, 70.16471), DeliveryInfo(207426, 91812, 120.72267), DeliveryInfo(46187, 22402, 4.01150)};
        depots = {22, 83381, 99384, 138862, 213724, 186007, 14685, 145134, 12655, 238784, 87809, 59217, 80633, 58410, 90134, 11485, 124175, 240326, 221820, 212406};
        turn_penalty = 15.000000000;
        truck_capacity = 276.145690918;
        {
        	ECE297_TIME_CONSTRAINT(45000);
        	
        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        }

        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
        CHECK(is_legal);

        if(is_legal) {
        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
        	std::cout << "QoR medium_london_england: " << path_cost << std::endl;
        } else {
        	std::cout << "QoR medium_london_england: INVALID" << std::endl;
        }

    } //medium_london_england

} //medium_london_england_public

