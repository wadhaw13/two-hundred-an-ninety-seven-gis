#include <random>
#include <iostream>
#include <unittest++/UnitTest++.h>

#include "StreetsDatabaseAPI.h"
#include "m1.h"
#include "m3.h"
#include "m4.h"

#include "unit_test_util.h"
#include "courier_verify.h"

using ece297test::relative_error;
using ece297test::courier_path_is_legal_with_capacity;


SUITE(hard_new_york_usa_public) {
    TEST(hard_new_york_usa) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        float turn_penalty;
        float truck_capacity;
        std::vector<CourierSubpath> result_path;
        bool is_legal;

        deliveries = {DeliveryInfo(155734, 31642, 110.03049), DeliveryInfo(127040, 32440, 81.80647), DeliveryInfo(194909, 19054, 80.24950), DeliveryInfo(206198, 58269, 123.66103), DeliveryInfo(63713, 47723, 41.62642), DeliveryInfo(165735, 85000, 148.90109), DeliveryInfo(110417, 176901, 4.63238), DeliveryInfo(120174, 128979, 9.46895), DeliveryInfo(200775, 142540, 75.58027), DeliveryInfo(67364, 170159, 135.20731), DeliveryInfo(97741, 148525, 191.90411), DeliveryInfo(37426, 133440, 3.63959), DeliveryInfo(187992, 99824, 86.45992), DeliveryInfo(20031, 20572, 106.93497), DeliveryInfo(124858, 195690, 57.69484), DeliveryInfo(126935, 192285, 187.63477), DeliveryInfo(110435, 58004, 117.53798), DeliveryInfo(100236, 161156, 75.60889), DeliveryInfo(178571, 115938, 116.86310), DeliveryInfo(16344, 114204, 98.46211), DeliveryInfo(183993, 35469, 64.50115), DeliveryInfo(94738, 108118, 135.15018), DeliveryInfo(84799, 120073, 33.65907), DeliveryInfo(40678, 171331, 156.83765), DeliveryInfo(213394, 168048, 109.61403), DeliveryInfo(35322, 181664, 178.73633), DeliveryInfo(108116, 32793, 181.46185), DeliveryInfo(144562, 149685, 145.42374), DeliveryInfo(212798, 212476, 149.13022), DeliveryInfo(56379, 32360, 64.30288), DeliveryInfo(132249, 157638, 164.20685), DeliveryInfo(20078, 167357, 29.04740), DeliveryInfo(124168, 36485, 147.23224), DeliveryInfo(105590, 210712, 47.43656), DeliveryInfo(116718, 195457, 10.11875), DeliveryInfo(27620, 161500, 42.38461), DeliveryInfo(159444, 135650, 147.31917), DeliveryInfo(167348, 52332, 43.10066), DeliveryInfo(158544, 109379, 111.97853), DeliveryInfo(14304, 170353, 115.63534), DeliveryInfo(192895, 122553, 33.46420), DeliveryInfo(38888, 126032, 150.56319), DeliveryInfo(35152, 78454, 35.71830), DeliveryInfo(7998, 8868, 158.04697), DeliveryInfo(71807, 129648, 85.17799), DeliveryInfo(40930, 122677, 26.68764), DeliveryInfo(88919, 117940, 39.38340), DeliveryInfo(123749, 127177, 76.09099), DeliveryInfo(94109, 54687, 188.02750), DeliveryInfo(124195, 66482, 74.70991), DeliveryInfo(173610, 42450, 122.17814), DeliveryInfo(91365, 44215, 60.90444), DeliveryInfo(193332, 144249, 117.98656), DeliveryInfo(149796, 125524, 128.95979), DeliveryInfo(181193, 1555, 18.63571), DeliveryInfo(66147, 169977, 164.53555), DeliveryInfo(83588, 9136, 95.16597), DeliveryInfo(15367, 120837, 156.55751), DeliveryInfo(147785, 103909, 187.95767), DeliveryInfo(85352, 2017, 104.87618), DeliveryInfo(47952, 63389, 78.48692), DeliveryInfo(18006, 26825, 41.83202), DeliveryInfo(88349, 68419, 73.51675), DeliveryInfo(169367, 186706, 126.80535), DeliveryInfo(130702, 204247, 21.23693), DeliveryInfo(207288, 203481, 127.91350), DeliveryInfo(194471, 163257, 112.43081), DeliveryInfo(19203, 37239, 147.78925), DeliveryInfo(63053, 24663, 134.35677), DeliveryInfo(207576, 138980, 136.07088), DeliveryInfo(41143, 24353, 77.44774), DeliveryInfo(128572, 131680, 79.88121), DeliveryInfo(140034, 177656, 145.73557), DeliveryInfo(109735, 205558, 1.41712), DeliveryInfo(92697, 84359, 5.78096)};
        depots = {4, 18235, 128958, 191204};
        turn_penalty = 15.000000000;
        truck_capacity = 554.403442383;
        {
        	ECE297_TIME_CONSTRAINT(45000);
        	
        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        }

        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
        CHECK(is_legal);

        if(is_legal) {
        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
        	std::cout << "QoR hard_new_york_usa: " << path_cost << std::endl;
        } else {
        	std::cout << "QoR hard_new_york_usa: INVALID" << std::endl;
        }

    } //hard_new_york_usa

    TEST(hard_multi_new_york_usa) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        float turn_penalty;
        float truck_capacity;
        std::vector<CourierSubpath> result_path;
        bool is_legal;

        deliveries = {DeliveryInfo(25901, 63285, 189.80901), DeliveryInfo(187103, 3111, 70.15913), DeliveryInfo(143615, 52432, 51.18974), DeliveryInfo(42696, 64880, 179.91382), DeliveryInfo(85145, 109374, 20.06098), DeliveryInfo(30735, 38108, 163.61295), DeliveryInfo(82287, 52432, 160.49899), DeliveryInfo(104441, 128213, 47.32206), DeliveryInfo(1785, 64721, 83.25284), DeliveryInfo(126106, 120266, 97.80219), DeliveryInfo(120248, 95446, 9.26475), DeliveryInfo(25901, 49326, 18.93790), DeliveryInfo(188218, 17428, 151.16054), DeliveryInfo(18988, 4311, 70.41461), DeliveryInfo(104441, 139355, 183.80823), DeliveryInfo(39423, 21433, 7.27917), DeliveryInfo(39633, 43511, 172.91985), DeliveryInfo(104441, 74478, 13.86993), DeliveryInfo(177839, 70633, 115.38968), DeliveryInfo(70305, 194046, 175.26952), DeliveryInfo(5023, 126258, 35.07595), DeliveryInfo(211147, 52432, 151.21779), DeliveryInfo(189476, 108551, 33.72619), DeliveryInfo(74676, 82603, 196.92422), DeliveryInfo(171343, 53650, 129.00230), DeliveryInfo(97021, 48707, 70.30033), DeliveryInfo(40157, 74050, 67.31814), DeliveryInfo(70938, 199649, 113.67527), DeliveryInfo(175371, 72971, 19.22806), DeliveryInfo(32689, 100828, 157.47266), DeliveryInfo(147938, 176933, 162.92372), DeliveryInfo(74852, 30907, 90.84747), DeliveryInfo(65620, 48707, 98.26041), DeliveryInfo(46957, 207819, 128.60576), DeliveryInfo(127426, 196668, 128.41371), DeliveryInfo(6387, 48707, 58.09481), DeliveryInfo(28608, 84900, 94.46448), DeliveryInfo(134729, 44849, 94.87312), DeliveryInfo(25901, 107865, 20.23751), DeliveryInfo(132295, 192515, 84.76921), DeliveryInfo(195483, 116539, 94.63832), DeliveryInfo(102639, 13961, 86.20132), DeliveryInfo(15997, 56852, 23.95706), DeliveryInfo(212339, 156909, 31.27068), DeliveryInfo(200127, 48913, 66.92841), DeliveryInfo(33889, 1789, 101.12639), DeliveryInfo(172216, 84922, 71.43660), DeliveryInfo(33049, 63512, 116.09393), DeliveryInfo(182731, 25699, 170.35597), DeliveryInfo(81122, 170122, 53.37529), DeliveryInfo(55240, 140865, 78.76680), DeliveryInfo(197949, 121649, 152.18198), DeliveryInfo(176698, 148880, 176.05496), DeliveryInfo(104441, 168719, 149.41982), DeliveryInfo(95904, 88430, 44.35627), DeliveryInfo(153539, 36600, 121.80888), DeliveryInfo(40063, 65586, 35.97311), DeliveryInfo(81860, 126779, 57.91961), DeliveryInfo(70644, 112066, 37.27143), DeliveryInfo(185394, 48707, 129.07108), DeliveryInfo(117022, 41144, 190.33194), DeliveryInfo(77777, 30659, 113.11504), DeliveryInfo(170704, 48707, 175.91534), DeliveryInfo(33943, 37616, 9.75238), DeliveryInfo(50050, 116008, 156.97383), DeliveryInfo(211180, 27226, 83.66404), DeliveryInfo(35268, 18272, 147.03349), DeliveryInfo(142694, 52432, 53.61071), DeliveryInfo(36013, 170000, 42.47386), DeliveryInfo(200472, 4035, 55.82699), DeliveryInfo(174494, 125507, 24.86163), DeliveryInfo(124287, 17736, 95.57848), DeliveryInfo(132771, 104664, 68.71355), DeliveryInfo(169598, 136839, 72.14176), DeliveryInfo(161536, 39906, 154.89548)};
        depots = {9, 36470, 43469, 167960, 200704, 81357, 6423};
        turn_penalty = 15.000000000;
        truck_capacity = 930.009887695;
        {
        	ECE297_TIME_CONSTRAINT(45000);
        	
        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        }

        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
        CHECK(is_legal);

        if(is_legal) {
        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
        	std::cout << "QoR hard_multi_new_york_usa: " << path_cost << std::endl;
        } else {
        	std::cout << "QoR hard_multi_new_york_usa: INVALID" << std::endl;
        }

    } //hard_multi_new_york_usa

} //hard_new_york_usa_public

