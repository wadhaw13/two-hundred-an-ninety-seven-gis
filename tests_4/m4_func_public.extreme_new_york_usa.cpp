#include <random>
#include <iostream>
#include <unittest++/UnitTest++.h>

#include "StreetsDatabaseAPI.h"
#include "m1.h"
#include "m3.h"
#include "m4.h"

#include "unit_test_util.h"
#include "courier_verify.h"

using ece297test::relative_error;
using ece297test::courier_path_is_legal_with_capacity;


SUITE(extreme_new_york_usa_public) {
    TEST(extreme_new_york_usa) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        float turn_penalty;
        float truck_capacity;
        std::vector<CourierSubpath> result_path;
        bool is_legal;

        deliveries = {DeliveryInfo(151100, 32850, 155.92310), DeliveryInfo(73990, 128165, 163.42424), DeliveryInfo(213173, 180440, 51.30645), DeliveryInfo(67879, 197693, 13.29368), DeliveryInfo(174498, 166357, 99.40691), DeliveryInfo(146075, 68400, 71.12291), DeliveryInfo(52311, 86958, 173.85931), DeliveryInfo(158139, 180430, 163.02940), DeliveryInfo(20493, 203716, 192.42633), DeliveryInfo(38630, 214359, 11.89855), DeliveryInfo(26604, 202733, 155.09703), DeliveryInfo(174012, 75250, 88.89195), DeliveryInfo(135706, 40942, 103.44039), DeliveryInfo(174809, 191580, 171.26430), DeliveryInfo(16681, 60876, 98.94067), DeliveryInfo(102323, 120855, 165.21535), DeliveryInfo(73061, 158175, 110.33611), DeliveryInfo(4790, 56543, 34.52993), DeliveryInfo(109456, 150188, 194.27785), DeliveryInfo(172428, 126419, 185.85349), DeliveryInfo(154517, 100294, 133.31874), DeliveryInfo(156996, 90899, 28.80888), DeliveryInfo(20159, 96422, 33.61533), DeliveryInfo(69766, 76647, 45.38171), DeliveryInfo(153213, 22004, 20.51671), DeliveryInfo(14458, 129610, 162.29688), DeliveryInfo(79207, 48919, 32.79430), DeliveryInfo(80475, 1177, 13.74151), DeliveryInfo(173430, 201057, 116.28961), DeliveryInfo(68310, 130688, 16.25003), DeliveryInfo(78776, 116242, 5.46588), DeliveryInfo(123085, 31825, 43.55830), DeliveryInfo(164062, 39144, 2.50282), DeliveryInfo(106818, 178345, 13.51135), DeliveryInfo(4667, 161831, 6.35845), DeliveryInfo(181549, 65278, 128.95584), DeliveryInfo(167084, 137101, 27.57326), DeliveryInfo(166673, 211133, 188.71782), DeliveryInfo(38307, 123384, 198.02077), DeliveryInfo(192967, 68691, 60.70238), DeliveryInfo(205654, 81036, 164.49094), DeliveryInfo(143856, 98032, 141.81566), DeliveryInfo(180594, 10957, 183.98149), DeliveryInfo(20916, 154276, 170.53242), DeliveryInfo(23996, 105450, 171.11041), DeliveryInfo(193832, 203240, 70.96735), DeliveryInfo(97320, 210573, 65.19470), DeliveryInfo(109902, 112210, 13.52282), DeliveryInfo(156798, 205734, 160.03049), DeliveryInfo(91933, 157296, 31.23666), DeliveryInfo(187779, 1722, 24.67691), DeliveryInfo(86260, 74132, 179.37248), DeliveryInfo(182300, 44636, 89.28055), DeliveryInfo(147959, 184607, 61.22405), DeliveryInfo(189160, 151840, 145.99667), DeliveryInfo(122790, 18453, 4.83181), DeliveryInfo(185873, 93260, 36.06763), DeliveryInfo(169139, 181695, 20.53601), DeliveryInfo(49438, 204034, 93.86347), DeliveryInfo(27409, 161337, 83.60363), DeliveryInfo(144716, 116095, 30.57524), DeliveryInfo(3851, 160399, 97.41222), DeliveryInfo(49034, 87013, 185.18018), DeliveryInfo(73148, 142959, 32.49354), DeliveryInfo(149790, 81583, 95.62476), DeliveryInfo(46735, 192208, 102.93948), DeliveryInfo(44019, 172490, 191.46980), DeliveryInfo(105967, 25660, 38.99465), DeliveryInfo(211285, 44017, 110.90310), DeliveryInfo(974, 20647, 3.60637), DeliveryInfo(209497, 195263, 83.27058), DeliveryInfo(60095, 155590, 154.57120), DeliveryInfo(54573, 164521, 106.09631), DeliveryInfo(116858, 23477, 174.84218), DeliveryInfo(202492, 65143, 6.59754), DeliveryInfo(112278, 117904, 69.95088), DeliveryInfo(132645, 84418, 199.15115), DeliveryInfo(116666, 98379, 25.79314), DeliveryInfo(54019, 88842, 60.64397), DeliveryInfo(40553, 113223, 145.27223), DeliveryInfo(36316, 5241, 35.99217), DeliveryInfo(171268, 26126, 178.05841), DeliveryInfo(114683, 200203, 57.58361), DeliveryInfo(116805, 94928, 18.24027), DeliveryInfo(148063, 46987, 76.28127), DeliveryInfo(189700, 193469, 173.36630), DeliveryInfo(105458, 158179, 164.66301), DeliveryInfo(61716, 93570, 48.10984), DeliveryInfo(114759, 36207, 109.94593), DeliveryInfo(177660, 73175, 0.12798), DeliveryInfo(131224, 118170, 177.93933), DeliveryInfo(192503, 145283, 109.47186), DeliveryInfo(50599, 212290, 116.26742), DeliveryInfo(106408, 112547, 144.80325), DeliveryInfo(111718, 67357, 197.72554), DeliveryInfo(191139, 36161, 9.46167), DeliveryInfo(39949, 39395, 124.40298), DeliveryInfo(85026, 67347, 56.24785), DeliveryInfo(57610, 35916, 139.88072), DeliveryInfo(97082, 82164, 182.33559), DeliveryInfo(160126, 190168, 120.93690), DeliveryInfo(143169, 199447, 145.69176), DeliveryInfo(135081, 120996, 87.39870), DeliveryInfo(202094, 165660, 22.98444), DeliveryInfo(59649, 183811, 81.93391), DeliveryInfo(198443, 108430, 31.87616), DeliveryInfo(46103, 159579, 94.14793), DeliveryInfo(158058, 66274, 14.53406), DeliveryInfo(42913, 157476, 173.54340), DeliveryInfo(155832, 104074, 113.41319), DeliveryInfo(55603, 113691, 168.08371), DeliveryInfo(85096, 162125, 169.28348), DeliveryInfo(205258, 137021, 82.83595), DeliveryInfo(122035, 213578, 173.82756), DeliveryInfo(139373, 183846, 30.29941), DeliveryInfo(101809, 145772, 182.77765), DeliveryInfo(54705, 153890, 60.17665), DeliveryInfo(60236, 44051, 187.01357), DeliveryInfo(97281, 101998, 131.89491), DeliveryInfo(41608, 6052, 99.34116), DeliveryInfo(57162, 109908, 96.77080), DeliveryInfo(63643, 152457, 23.36531), DeliveryInfo(123431, 194480, 67.02440), DeliveryInfo(163649, 44152, 134.58875), DeliveryInfo(133367, 115012, 133.30185), DeliveryInfo(127350, 125653, 13.76791), DeliveryInfo(166359, 140844, 191.02235), DeliveryInfo(82861, 204350, 40.45395)};
        depots = {14};
        turn_penalty = 15.000000000;
        truck_capacity = 4077.515869141;
        {
        	ECE297_TIME_CONSTRAINT(45000);
        	
        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        }

        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
        CHECK(is_legal);

        if(is_legal) {
        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
        	std::cout << "QoR extreme_new_york_usa: " << path_cost << std::endl;
        } else {
        	std::cout << "QoR extreme_new_york_usa: INVALID" << std::endl;
        }

    } //extreme_new_york_usa

    TEST(extreme_multi_new_york_usa) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        float turn_penalty;
        float truck_capacity;
        std::vector<CourierSubpath> result_path;
        bool is_legal;

        deliveries = {DeliveryInfo(113705, 54589, 24.02952), DeliveryInfo(12846, 191390, 128.83752), DeliveryInfo(36566, 163070, 116.01514), DeliveryInfo(27922, 44827, 166.51376), DeliveryInfo(76813, 59227, 186.14952), DeliveryInfo(40404, 150964, 23.32040), DeliveryInfo(205279, 96087, 98.85214), DeliveryInfo(40404, 124010, 91.61575), DeliveryInfo(129984, 43020, 183.96866), DeliveryInfo(75233, 83506, 151.52605), DeliveryInfo(186960, 265, 113.51292), DeliveryInfo(206560, 159848, 182.38109), DeliveryInfo(78847, 212772, 117.20209), DeliveryInfo(66099, 207480, 62.21970), DeliveryInfo(184851, 174251, 7.21802), DeliveryInfo(40404, 161140, 21.00040), DeliveryInfo(128238, 7361, 110.47968), DeliveryInfo(6223, 190612, 164.22585), DeliveryInfo(194042, 94828, 145.98535), DeliveryInfo(12846, 170778, 58.92366), DeliveryInfo(149352, 189939, 103.99475), DeliveryInfo(11070, 41830, 130.62224), DeliveryInfo(194042, 57081, 65.74391), DeliveryInfo(186960, 193711, 124.24331), DeliveryInfo(194042, 54589, 148.90076), DeliveryInfo(12846, 184984, 188.55615), DeliveryInfo(78847, 44462, 194.26132), DeliveryInfo(128238, 190612, 188.19695), DeliveryInfo(11070, 96087, 54.82195), DeliveryInfo(40404, 138246, 109.98607), DeliveryInfo(136294, 190612, 137.51477), DeliveryInfo(100101, 96055, 175.75049), DeliveryInfo(11070, 97609, 51.33574), DeliveryInfo(4301, 183433, 27.20531), DeliveryInfo(149705, 141805, 27.52943), DeliveryInfo(129984, 92611, 72.97310), DeliveryInfo(129984, 131978, 84.73216), DeliveryInfo(186960, 133726, 106.18583), DeliveryInfo(140611, 99893, 96.39171), DeliveryInfo(191809, 97609, 124.18550), DeliveryInfo(57217, 136416, 158.33244), DeliveryInfo(2655, 44827, 65.09235), DeliveryInfo(129984, 177919, 72.36898), DeliveryInfo(108625, 111212, 122.98188), DeliveryInfo(40404, 80205, 58.28953), DeliveryInfo(61471, 15555, 93.88316), DeliveryInfo(128238, 118385, 33.87186), DeliveryInfo(98653, 200034, 28.38225), DeliveryInfo(194042, 198829, 39.61357), DeliveryInfo(141877, 153574, 186.88531), DeliveryInfo(76216, 131172, 140.56772), DeliveryInfo(40404, 149614, 144.54257), DeliveryInfo(65379, 58869, 14.89165), DeliveryInfo(80127, 97609, 34.88544), DeliveryInfo(86939, 95832, 154.88277), DeliveryInfo(207913, 163070, 145.89897), DeliveryInfo(178889, 138485, 89.29800), DeliveryInfo(97827, 131793, 103.72406), DeliveryInfo(161988, 78580, 64.22950), DeliveryInfo(70536, 54589, 22.10223), DeliveryInfo(139417, 9685, 96.66594), DeliveryInfo(70536, 177800, 161.79268), DeliveryInfo(164505, 143634, 94.13924), DeliveryInfo(97414, 6988, 195.46440), DeliveryInfo(128238, 97622, 62.57455), DeliveryInfo(163721, 134132, 135.98068), DeliveryInfo(82288, 87022, 124.07844), DeliveryInfo(201190, 44827, 190.35825), DeliveryInfo(186960, 177179, 182.60593), DeliveryInfo(79813, 136489, 170.55211), DeliveryInfo(70536, 168559, 121.11341), DeliveryInfo(190892, 39836, 65.39146), DeliveryInfo(67886, 209282, 111.43532), DeliveryInfo(76813, 102196, 94.21487), DeliveryInfo(155554, 97609, 46.22738), DeliveryInfo(124748, 177800, 41.81517), DeliveryInfo(3571, 163070, 59.91436), DeliveryInfo(125796, 82568, 126.28914), DeliveryInfo(138949, 96087, 102.87590), DeliveryInfo(79266, 162682, 122.77299), DeliveryInfo(148100, 111212, 175.02460), DeliveryInfo(54452, 150454, 12.68412), DeliveryInfo(76813, 52864, 75.41554), DeliveryInfo(40404, 54589, 183.65543), DeliveryInfo(72026, 174251, 31.04557), DeliveryInfo(11070, 44827, 0.59063), DeliveryInfo(209328, 122203, 110.48355), DeliveryInfo(141266, 202454, 151.72780), DeliveryInfo(159758, 14609, 52.66313), DeliveryInfo(26048, 29339, 101.81618), DeliveryInfo(12846, 123859, 168.62360), DeliveryInfo(1282, 177440, 29.90578), DeliveryInfo(141289, 89796, 181.94458), DeliveryInfo(80315, 184041, 46.57819), DeliveryInfo(76813, 131172, 175.67166), DeliveryInfo(170582, 201700, 46.62588), DeliveryInfo(145942, 121199, 77.72700), DeliveryInfo(129443, 109775, 159.87247), DeliveryInfo(93915, 153349, 3.44797), DeliveryInfo(176860, 174251, 37.20285), DeliveryInfo(67779, 116365, 18.70948), DeliveryInfo(76813, 163152, 125.49466), DeliveryInfo(41979, 4913, 152.76578), DeliveryInfo(12775, 87022, 156.56242), DeliveryInfo(70536, 175786, 24.79420), DeliveryInfo(92630, 37295, 40.77335), DeliveryInfo(108625, 3908, 170.07759), DeliveryInfo(170290, 213788, 14.89180), DeliveryInfo(125553, 132339, 41.97498), DeliveryInfo(17568, 73090, 174.11722), DeliveryInfo(50143, 202110, 12.51069), DeliveryInfo(186960, 145262, 103.49479), DeliveryInfo(207847, 97622, 196.95247), DeliveryInfo(128238, 202454, 92.38354), DeliveryInfo(11070, 174251, 45.47717), DeliveryInfo(12846, 57285, 28.51158), DeliveryInfo(129984, 170496, 82.39725), DeliveryInfo(194042, 198829, 197.96646), DeliveryInfo(78847, 196594, 38.83097), DeliveryInfo(122989, 69937, 9.55794), DeliveryInfo(28850, 208098, 171.22623), DeliveryInfo(70941, 59514, 61.40087), DeliveryInfo(76813, 83311, 81.54330), DeliveryInfo(89698, 97622, 176.74506), DeliveryInfo(11070, 177800, 60.88481), DeliveryInfo(129760, 196243, 170.77982), DeliveryInfo(72782, 54589, 112.57236), DeliveryInfo(61318, 97609, 180.65919), DeliveryInfo(176518, 210906, 0.28636), DeliveryInfo(37976, 133666, 22.65839), DeliveryInfo(99371, 61814, 143.38663), DeliveryInfo(36545, 164570, 15.71275), DeliveryInfo(186960, 170778, 70.20303), DeliveryInfo(194042, 73090, 170.41853), DeliveryInfo(34126, 49143, 73.23205), DeliveryInfo(38069, 96087, 184.23135), DeliveryInfo(76813, 29339, 31.46012), DeliveryInfo(186496, 208191, 11.26297), DeliveryInfo(81428, 97622, 74.88575), DeliveryInfo(78847, 97622, 9.94568), DeliveryInfo(73201, 44859, 87.84380), DeliveryInfo(128238, 31302, 107.70588), DeliveryInfo(194042, 192593, 70.27306), DeliveryInfo(186960, 181330, 150.70450), DeliveryInfo(40404, 83311, 57.01212), DeliveryInfo(11070, 153349, 32.26524), DeliveryInfo(64263, 119759, 75.17377), DeliveryInfo(76813, 135970, 112.81884), DeliveryInfo(55011, 158340, 78.02843), DeliveryInfo(194042, 190612, 110.32596), DeliveryInfo(129984, 156342, 144.47945), DeliveryInfo(110481, 190523, 167.84109), DeliveryInfo(12846, 142847, 57.16391), DeliveryInfo(141230, 52192, 159.22272), DeliveryInfo(40404, 1327, 39.36797), DeliveryInfo(76813, 144345, 131.02383), DeliveryInfo(201657, 14595, 51.44870), DeliveryInfo(128238, 183616, 80.44250), DeliveryInfo(12846, 46357, 39.82143), DeliveryInfo(164575, 164512, 20.43353), DeliveryInfo(34857, 143634, 146.99573), DeliveryInfo(119904, 157206, 31.30645), DeliveryInfo(107300, 159848, 193.79584), DeliveryInfo(76813, 42764, 119.03182), DeliveryInfo(148957, 200138, 185.34010), DeliveryInfo(37765, 37295, 152.45525), DeliveryInfo(194042, 174251, 167.15979), DeliveryInfo(12846, 43510, 170.09586), DeliveryInfo(165207, 67063, 96.45847), DeliveryInfo(126570, 198829, 146.60443), DeliveryInfo(128238, 59865, 142.21570), DeliveryInfo(104865, 102117, 93.50856), DeliveryInfo(169800, 42106, 151.76147), DeliveryInfo(194042, 6433, 78.05167), DeliveryInfo(173645, 171373, 32.22672), DeliveryInfo(181451, 69117, 16.05515), DeliveryInfo(156340, 72103, 197.99561), DeliveryInfo(134540, 29339, 45.93628), DeliveryInfo(103483, 29339, 189.85484), DeliveryInfo(162244, 170778, 83.50754), DeliveryInfo(169844, 142383, 192.37903), DeliveryInfo(85392, 73052, 127.71318), DeliveryInfo(19597, 138485, 42.64752), DeliveryInfo(26085, 96087, 38.49220), DeliveryInfo(129984, 146101, 57.07528), DeliveryInfo(42866, 170176, 80.78585), DeliveryInfo(31995, 44827, 14.01746), DeliveryInfo(127024, 119371, 36.93928), DeliveryInfo(67283, 29339, 95.93696), DeliveryInfo(59229, 9685, 172.90303), DeliveryInfo(18631, 190612, 2.46589), DeliveryInfo(35472, 78580, 31.04699)};
        depots = {19, 72940};
        turn_penalty = 15.000000000;
        truck_capacity = 5387.044433594;
        {
        	ECE297_TIME_CONSTRAINT(45000);
        	
        	result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        }

        is_legal = courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity);
        CHECK(is_legal);

        if(is_legal) {
        	double path_cost = ece297test::compute_courier_path_travel_time(result_path, turn_penalty);
        	std::cout << "QoR extreme_multi_new_york_usa: " << path_cost << std::endl;
        } else {
        	std::cout << "QoR extreme_multi_new_york_usa: INVALID" << std::endl;
        }

    } //extreme_multi_new_york_usa

} //extreme_new_york_usa_public

