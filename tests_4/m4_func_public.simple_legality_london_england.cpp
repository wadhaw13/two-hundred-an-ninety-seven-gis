#include <random>
#include <iostream>
#include <unittest++/UnitTest++.h>

#include "StreetsDatabaseAPI.h"
#include "m1.h"
#include "m3.h"
#include "m4.h"

#include "unit_test_util.h"
#include "courier_verify.h"

using ece297test::relative_error;
using ece297test::courier_path_is_legal_with_capacity;


SUITE(simple_legality_london_england_public) {
    TEST(simple_legality_london_england) {
        std::vector<DeliveryInfo> deliveries;
        std::vector<IntersectionIndex> depots;
        std::vector<CourierSubpath> result_path;
        float turn_penalty;
        float truck_capacity;

        deliveries = {DeliveryInfo(5195, 23661, 142.49434), DeliveryInfo(22531, 154211, 144.19308), DeliveryInfo(80098, 213034, 144.16670), DeliveryInfo(216130, 115885, 70.31059), DeliveryInfo(150341, 57635, 162.37219)};
        depots = {98480, 132621, 26798};
        turn_penalty = 15.000000000;
        truck_capacity = 675.567504883;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(19923, 20888, 50.03543), DeliveryInfo(24501, 115262, 60.50912), DeliveryInfo(237568, 228229, 35.54058)};
        depots = {47033, 72604, 64991};
        turn_penalty = 15.000000000;
        truck_capacity = 13465.874023438;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(41690, 49692, 156.64452)};
        depots = {11};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(30473, 54368, 9.75238), DeliveryInfo(183148, 149305, 129.07108), DeliveryInfo(215796, 54368, 35.97311), DeliveryInfo(65712, 52061, 113.11504), DeliveryInfo(183148, 180223, 190.33194), DeliveryInfo(183148, 102549, 37.27143), DeliveryInfo(183148, 149305, 129.07108), DeliveryInfo(215796, 149305, 57.91961), DeliveryInfo(68428, 54368, 175.91534)};
        depots = {65423, 96547, 186534};
        turn_penalty = 15.000000000;
        truck_capacity = 11773.037109375;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(45306, 45906, 115.91381), DeliveryInfo(53679, 211933, 75.65656)};
        depots = {151234};
        turn_penalty = 15.000000000;
        truck_capacity = 1328.184082031;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(74001, 94299, 82.98276), DeliveryInfo(34982, 89901, 60.49590), DeliveryInfo(69791, 121175, 197.61226), DeliveryInfo(61715, 62313, 141.63181), DeliveryInfo(218542, 115866, 109.04499)};
        depots = {40226, 197967, 52961};
        turn_penalty = 15.000000000;
        truck_capacity = 3268.205078125;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(45619, 194475, 35.08236), DeliveryInfo(133222, 97053, 60.36134), DeliveryInfo(132615, 194336, 102.00575)};
        depots = {59938, 84650, 55915};
        turn_penalty = 15.000000000;
        truck_capacity = 8968.174804688;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(93003, 7342, 159.20335)};
        depots = {229435};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(97078, 119647, 28.59369), DeliveryInfo(80744, 20275, 46.00364), DeliveryInfo(85140, 179371, 41.82777)};
        depots = {3557, 109109, 101089};
        turn_penalty = 15.000000000;
        truck_capacity = 5104.119140625;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(110910, 228776, 4.68502), DeliveryInfo(145667, 208889, 150.65356)};
        depots = {40316};
        turn_penalty = 15.000000000;
        truck_capacity = 14852.536132812;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(114334, 69848, 55.20467), DeliveryInfo(224267, 193368, 99.69247), DeliveryInfo(101420, 215558, 6.81267), DeliveryInfo(89361, 215558, 119.94157), DeliveryInfo(87779, 215558, 54.26281), DeliveryInfo(97268, 120548, 196.75076), DeliveryInfo(216637, 120548, 155.84526), DeliveryInfo(146640, 105732, 184.55672)};
        depots = {94031, 101271, 3994};
        turn_penalty = 15.000000000;
        truck_capacity = 10305.068359375;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(117332, 154016, 175.53738)};
        depots = {57215, 38802};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(137461, 41168, 72.53727)};
        depots = {94067, 109633};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(119392, 43904, 24.15597)};
        depots = {128901};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(124091, 144333, 131.37521), DeliveryInfo(49739, 29377, 117.05125), DeliveryInfo(31123, 202261, 77.03854), DeliveryInfo(74974, 4928, 59.02165), DeliveryInfo(124091, 35331, 13.02049), DeliveryInfo(124091, 4612, 112.16380), DeliveryInfo(128109, 144927, 34.13472), DeliveryInfo(31123, 139063, 127.61972)};
        depots = {123306, 192871, 159304};
        turn_penalty = 15.000000000;
        truck_capacity = 13573.001953125;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(157636, 185352, 129.00230), DeliveryInfo(157400, 185352, 70.30033), DeliveryInfo(115788, 116289, 113.67527), DeliveryInfo(111354, 42993, 33.72619), DeliveryInfo(24805, 103904, 19.22806), DeliveryInfo(71208, 185352, 67.31814), DeliveryInfo(116001, 105659, 157.47266), DeliveryInfo(120441, 42993, 196.92422)};
        depots = {17000, 141436, 214833};
        turn_penalty = 15.000000000;
        truck_capacity = 12219.279296875;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(191107, 93578, 66.15946), DeliveryInfo(224822, 240637, 45.42579)};
        depots = {32704, 163121};
        turn_penalty = 15.000000000;
        truck_capacity = 11119.174804688;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(216600, 18287, 1.66533), DeliveryInfo(229170, 38740, 69.64507)};
        depots = {63148, 85567};
        turn_penalty = 15.000000000;
        truck_capacity = 2802.345214844;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(106333, 195661, 190.66869), DeliveryInfo(7549, 154000, 168.13336), DeliveryInfo(195171, 136166, 165.65623), DeliveryInfo(45697, 51827, 192.23914), DeliveryInfo(41925, 92578, 175.08046)};
        depots = {149587, 175081, 179922};
        turn_penalty = 15.000000000;
        truck_capacity = 10512.346679688;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(20277, 209744, 0.80647), DeliveryInfo(65527, 220407, 146.50169), DeliveryInfo(65527, 34196, 193.41937), DeliveryInfo(65527, 186036, 33.82141), DeliveryInfo(198930, 201833, 183.47961), DeliveryInfo(98534, 209744, 144.07825), DeliveryInfo(66738, 34196, 11.81318), DeliveryInfo(65527, 34196, 193.41937), DeliveryInfo(66738, 209744, 112.46114)};
        depots = {159711, 83689, 7040};
        turn_penalty = 15.000000000;
        truck_capacity = 9668.773437500;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(162565, 53291, 108.68627), DeliveryInfo(90161, 109931, 194.95090), DeliveryInfo(77039, 151871, 74.59904), DeliveryInfo(108942, 105256, 170.06468), DeliveryInfo(190899, 118799, 192.59464)};
        depots = {81230, 221549, 142070};
        turn_penalty = 15.000000000;
        truck_capacity = 3563.215087891;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(168822, 57160, 25.99644), DeliveryInfo(81021, 184508, 73.93540), DeliveryInfo(212986, 118709, 135.73758), DeliveryInfo(192842, 227216, 188.84705), DeliveryInfo(128770, 178336, 36.38079)};
        depots = {159855, 165833, 156460};
        turn_penalty = 15.000000000;
        truck_capacity = 8742.108398438;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(176526, 60981, 50.41379), DeliveryInfo(70700, 168654, 186.24405), DeliveryInfo(70700, 60981, 35.82661), DeliveryInfo(59333, 45231, 186.60216), DeliveryInfo(70700, 60981, 35.82661), DeliveryInfo(176526, 110586, 148.86475), DeliveryInfo(70700, 229594, 124.07938), DeliveryInfo(73081, 110586, 46.18541), DeliveryInfo(77855, 110586, 72.40468)};
        depots = {128874, 59397, 178418};
        turn_penalty = 15.000000000;
        truck_capacity = 1192.932617188;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(147442, 125381, 173.83640), DeliveryInfo(126780, 242193, 56.51704), DeliveryInfo(190802, 200290, 102.99988), DeliveryInfo(89855, 76434, 127.61862), DeliveryInfo(89855, 148213, 44.21967), DeliveryInfo(121615, 231137, 78.60152), DeliveryInfo(190802, 83028, 18.57643), DeliveryInfo(89855, 147986, 107.25372)};
        depots = {193454, 123332, 227766};
        turn_penalty = 15.000000000;
        truck_capacity = 10054.899414062;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(196728, 58004, 46.58144), DeliveryInfo(220526, 75642, 168.12914), DeliveryInfo(232655, 198973, 176.08020), DeliveryInfo(62744, 75642, 38.50279), DeliveryInfo(149070, 198973, 94.24657), DeliveryInfo(24589, 200545, 161.54309), DeliveryInfo(102045, 119879, 132.61261), DeliveryInfo(85996, 75642, 167.31036)};
        depots = {231471, 78014, 118943};
        turn_penalty = 15.000000000;
        truck_capacity = 10738.934570312;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(241411, 133774, 28.66476)};
        depots = {48808, 169115};
        turn_penalty = 15.000000000;
        truck_capacity = 5000.000000000;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(199473, 144158, 184.61324), DeliveryInfo(213887, 203297, 65.88518)};
        depots = {195870};
        turn_penalty = 15.000000000;
        truck_capacity = 10739.620117188;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(241373, 37780, 65.56896), DeliveryInfo(97333, 164173, 182.31299)};
        depots = {201993, 200475};
        turn_penalty = 15.000000000;
        truck_capacity = 2286.546142578;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(244280, 111613, 182.28844), DeliveryInfo(92345, 124086, 45.65729), DeliveryInfo(112894, 161364, 123.09861), DeliveryInfo(199733, 208864, 92.90118), DeliveryInfo(188053, 6610, 33.00730)};
        depots = {214026, 63917, 205172};
        turn_penalty = 15.000000000;
        truck_capacity = 7653.041503906;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

        deliveries = {DeliveryInfo(215780, 155735, 20.42503), DeliveryInfo(148810, 220185, 120.76764), DeliveryInfo(148810, 17060, 174.53944), DeliveryInfo(55790, 117573, 166.97858), DeliveryInfo(87650, 10495, 193.52501), DeliveryInfo(148810, 51333, 94.40701), DeliveryInfo(164350, 161786, 45.52258), DeliveryInfo(55790, 25622, 22.48336)};
        depots = {227719, 108949, 219927};
        turn_penalty = 15.000000000;
        truck_capacity = 10248.227539062;
        result_path = traveling_courier(deliveries, depots, turn_penalty, truck_capacity);
        CHECK(courier_path_is_legal_with_capacity(deliveries, depots, result_path, truck_capacity));

    } //simple_legality_london_england

} //simple_legality_london_england_public

